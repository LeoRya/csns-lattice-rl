'''
Add aperture nodes into the lattice
@author Xiaohan Lu
@email luxh@ihep.ac.cn
@time 2018-10-28
'''
import copy
import orbit_mpi
from orbit.aperture import EllipseApertureNode,CircleApertureNode,RectangleApertureNode,PhaseApertureNode
from orbit.aperture import addTeapotApertureNode

comm = orbit_mpi.mpi_comm.MPI_COMM_WORLD
rank = orbit_mpi.MPI_Comm_rank(comm)

def add_aperture_design(lattice, machine):
    '''
    Add aperture according to the design values
    '''
    if rank == 0:
        print("Using design aperture settings!")

    posDict = lattice.getNodePositionsDict()

    latticeLength = lattice.getLength()
    HarmonicNum = 2
    LonPos = 0.002
    phaseMax = 190
    phaseMin = -190
    LonPhaseNode = PhaseApertureNode(latticeLength, HarmonicNum, LonPos, phaseMax, phaseMin)
    addTeapotApertureNode(lattice, LonPos, LonPhaseNode)

    BV = lattice.getNodesForName('BV')
    for f in BV:
        #radius,pos,offsetx,offsety
        name = f.getName()
        after = CircleApertureNode(0.163/2,0,0,0)
        if name == "BV02":
            after = CircleApertureNode(0.163/2,0,0,-0.0143)
        if name == "BV03":
            after = CircleApertureNode(0.163/2,0,0.006,-0.014)
        addAperture(posDict,f,after=after)

    BH = lattice.getNodesForName('BH')
    for f in BH:
        name = f.getName()
        after = CircleApertureNode(0.163/2,0,0,0)
        if name == "BH02":
            after = CircleApertureNode(0.163/2,0,0,-0.0143)
        if name == "BH03":
            after = CircleApertureNode(0.163/2,0,0.006,-0.014)
        addAperture(posDict,f,after=after)

    #four BC position [226.32, 227.27, 0.3, 1.25]
    BC = lattice.getNodesForName('BC')
    for f in BC:
        name = f.getName()
        #xshift = 0.0385
        xshift = -0.0216
        before = EllipseApertureNode(0.213/2,0.152/2,0,xshift,-0.014)
        after = EllipseApertureNode(0.213/2,0.152/2,0,xshift,-0.014)
        mid = EllipseApertureNode(0.213/2,0.152/2,0,xshift,-0.014)
        if name == "BC01":
            before = EllipseApertureNode(0.175/2,0.152/2,0,0,-0.014)
            after = EllipseApertureNode(0.175/2,0.152/2,0,0,-0.014)
            mid = EllipseApertureNode(0.175/2,0.152/2,0,0,-0.014)
        if name == "BC03":
            before = EllipseApertureNode(0.207/2,0.152/2,0,xshift,-0.014)
            after = EllipseApertureNode(0.207/2,0.152/2,0,xshift,-0.014)
            mid = EllipseApertureNode(0.207/2,0.152/2,0,xshift,-0.014)
        addAperture(posDict,f,before,after, mid)

    #R4DH01_aperture = CircleApertureNode(0.220/2,0,0,0)
    #R1DH01_aperture = CircleApertureNode(0.190/2,0,0,0)

    #R1CAV02,R3CAV01,R3CAV02,R3CAV03,R4CAV01,R4CAV02,R4CAV03
    #R1CAV01_aperture = CircleApertureNode(0.170/2,0,0,0)
    #R2CAV_D_aperture = CircleApertureNode(0.250/2,0,0,0)#R3CAV_D,R4CAV_D

    #COLM_aperture = CircleApertureNode(0.250/2,0,0,0)
    #COLP_aperture = CircleApertureNode(0.280/2,0,0,0)
    #COLS1_aperture = CircleApertureNode(0.228/2,0,0,0)
    #COLS2_aperture = CircleApertureNode(0.228/2,0,0,0)
    #COLS3_aperture = CircleApertureNode(0.228/2,0,0,0)
    #COLS4_aperture = CircleApertureNode(0.228/2,0,0,0)

##########################################################################
#put the aperture node at the entry, middle and exit of the associated node.
#########################################################################
    QF01 = lattice.getNodesForName('QF01')
    QD02 = lattice.getNodesForName('QD02')
    QF03 = lattice.getNodesForName('QF03')
    QF04 = lattice.getNodesForName('QF04')
    QF06 = lattice.getNodesForName('QF06')
    MB = lattice.getNodesForName('bend')

    for f in MB:
        if f.getName() in ['bend', 'bend_3', 'bend_4', 'bend_5', 'bend_6',
                'bend_7', 'bend_8', 'bend_9']:
            before = EllipseApertureNode(0.218/2,0.135/2,0,0,0)
            after = EllipseApertureNode(0.218/2,0.135/2,0,0,0)
            mid = EllipseApertureNode(0.218/2, 0.135/2, 0, 0, 0)
            addAperture(posDict,f,before,after, mid)

    for f in QF01:
        if f.getName() in ['QF01', 'QF01_4', 'QF01_5', 'QF01_6']:
            before = CircleApertureNode(0.183/2,0,0,0)
            after = CircleApertureNode(0.183/2,0,0,0)
            mid = CircleApertureNode(0.183/2,0,0,0)
            addAperture(posDict,f,before,after, mid)

    for f in QD02:
        if f.getName() in ['QD02', 'QD02_3', 'QD02_4', 'QD02_5', 'QD02_6',
        'QD02_7']:
            before = CircleApertureNode(0.248/2,0,0,0)
            after = CircleApertureNode(0.248/2,0,0,0)
            mid = CircleApertureNode(0.248/2,0,0,0)
            addAperture(posDict,f,before,after, mid)

    for f in QF03:
        if f.getName() in ['QF03', 'QF03_4', 'QF03_5', 'QF03_6']:
            before = CircleApertureNode(0.183/2,0,0,0)
            after = CircleApertureNode(0.183/2,0,0,0)
            mid = CircleApertureNode(0.183/2,0,0,0)
            addAperture(posDict,f,before,after, mid)

    for f in QF04:
        if f.getName() in ['QF04', 'QF04_3', 'QF04_4', 'QF04_5']:
            before = CircleApertureNode(0.199/2,0,0,0)
            after = CircleApertureNode(0.199/2,0,0,0)
            mid = CircleApertureNode(0.199/2,0,0,0)
            addAperture(posDict,f,before,after, mid)

    for f in QF06:
        if f.getName() in ['QF06', 'QF06_4', 'QF06_5', 'QF06_6', 'QF06_7',
        'QF06_8']:
            before = CircleApertureNode(0.230/2,0,0,0)
            after = CircleApertureNode(0.230/2,0,0,0)
            mid = CircleApertureNode(0.230/2,0,0,0)
            addAperture(posDict,f,before,after, mid)

    lattice.initialize()

def add_aperture_empirical(lattice, machine):
    '''
    Add the aperture according to the real survey
    '''
    if rank == 0:
        print("Using empirical aperture settings!")

    posDict = lattice.getNodePositionsDict()

    latticeLength = lattice.getLength()
    HarmonicNum = 2
    LonPos = 0.002
    phaseMax = 190
    phaseMin = -190
    LonPhaseNode = PhaseApertureNode(latticeLength, HarmonicNum, LonPos, phaseMax, phaseMin)
    addTeapotApertureNode(lattice, LonPos, LonPhaseNode)

    BV = lattice.getNodesForName('BV')
    for f in BV:
        #radius,pos,offsetx,offsety
        name = f.getName()
        after = CircleApertureNode(0.163/2,0,0,0)
        if name == "BV02":
            after = CircleApertureNode(0.163/2,0,0,-0.0143)
        if name == "BV03":
            after = CircleApertureNode(0.150/2,0,0.006,-0.014)
        addAperture(posDict,f,after=after)

    BH = lattice.getNodesForName('BH')
    for f in BH:
        name = f.getName()
        after = CircleApertureNode(0.163/2,0,0,0)
        if name == "BH02":
            after = CircleApertureNode(0.163/2,0,0,-0.0143)
        if name == "BH03":
            after = CircleApertureNode(0.150/2,0,0.006,-0.014)
        addAperture(posDict,f,after=after)

    #four BC position [226.32, 227.27, 0.3, 1.25]
    BC = lattice.getNodesForName('BC')
    for f in BC:
        name = f.getName()
        #xshift = 0.0385
        xshift = -0.0216
        before = EllipseApertureNode(0.213/2,0.152/2,0,xshift,-0.014)
        after = EllipseApertureNode(0.213/2,0.152/2,0,xshift,-0.014)
        mid = EllipseApertureNode(0.213/2,0.152/2,0,xshift,-0.014)
        if name == "BC01":
            before = EllipseApertureNode(0.175/2,0.152/2,0,0,-0.014)
            after = EllipseApertureNode(0.175/2,0.152/2,0,0,-0.014)
            mid = EllipseApertureNode(0.175/2,0.152/2,0,0,-0.014)
        if name == "BC03":
            before = EllipseApertureNode(0.207/2,0.152/2,0,xshift,-0.014)
            after = EllipseApertureNode(0.207/2,0.152/2,0,xshift,-0.014)
            mid = EllipseApertureNode(0.207/2,0.152/2,0,xshift,-0.014)
        addAperture(posDict,f,before,after, mid)

    #R4DH01_aperture = CircleApertureNode(0.220/2,0,0,0)
    #R1DH01_aperture = CircleApertureNode(0.190/2,0,0,0)

    #R1CAV02,R3CAV01,R3CAV02,R3CAV03,R4CAV01,R4CAV02,R4CAV03
    #R1CAV01_aperture = CircleApertureNode(0.170/2,0,0,0)
    #R2CAV_D_aperture = CircleApertureNode(0.250/2,0,0,0)#R3CAV_D,R4CAV_D

    #COLM_aperture = CircleApertureNode(0.250/2,0,0,0)
    #COLP_aperture = CircleApertureNode(0.280/2,0,0,0)
    #COLS1_aperture = CircleApertureNode(0.228/2,0,0,0)
    #COLS2_aperture = CircleApertureNode(0.228/2,0,0,0)
    #COLS3_aperture = CircleApertureNode(0.228/2,0,0,0)
    #COLS4_aperture = CircleApertureNode(0.228/2,0,0,0)

##########################################################################
#put the aperture node at the entry, middle and exit of the associated node.    
#########################################################################
    QF01 = lattice.getNodesForName('QF01')
    QD02 = lattice.getNodesForName('QD02')
    QF03 = lattice.getNodesForName('QF03')
    QF04 = lattice.getNodesForName('QF04')
    QF06 = lattice.getNodesForName('QF06')
    MB = lattice.getNodesForName('bend')

    for f in MB:
        if f.getName() in ['bend', 'bend_3', 'bend_4', 'bend_5', 'bend_6',
                'bend_7', 'bend_8', 'bend_9']:
            before = EllipseApertureNode(0.218/2,0.135/2,0,0,0)
            after = EllipseApertureNode(0.218/2,0.135/2,0,0,0)
            mid = EllipseApertureNode(0.218/2, 0.135/2, 0, 0, 0)
            addAperture(posDict,f,before,after, mid)

    for f in QF01:
        if f.getName() in ['QF01', 'QF01_4', 'QF01_5', 'QF01_6']:
            before = CircleApertureNode(0.183/2,0,0,0)
            after = CircleApertureNode(0.183/2,0,0,0)
            mid = CircleApertureNode(0.183/2,0,0,0)
            addAperture(posDict,f,before,after, mid)

    QD_pipe = []
    for f in QD02:
        if f.getName() in ['QD02', 'QD02_3', 'QD02_4', 'QD02_5', 'QD02_6',
        'QD02_7']:
            before = CircleApertureNode(0.248/2,0,0,0)
            after = CircleApertureNode(0.248/2,0,0,0)
            mid = CircleApertureNode(0.248/2,0,0,0)
            addAperture(posDict,f,before,after, mid)
        elif f.getParam("subname") in ['R1QD02_8', 'R2QD11_8', 'R3QD02_8',
        'R4QD11_8']:
            QD_pipe.append(f)

    for f in QF03:
        if f.getName() in ['QF03', 'QF03_4', 'QF03_5', 'QF03_6']:
            before = CircleApertureNode(0.183/2,0,0,0)
            after = CircleApertureNode(0.183/2,0,0,0)
            mid = CircleApertureNode(0.183/2,0,0,0)
            addAperture(posDict,f,before,after, mid)

    for f in QF04:
        if f.getName() in ['QF04', 'QF04_3', 'QF04_4', 'QF04_5']:
            before = CircleApertureNode(0.199/2,0,0,0)
            after = CircleApertureNode(0.199/2,0,0,0)
            mid = CircleApertureNode(0.199/2,0,0,0)
            addAperture(posDict,f,before,after, mid)

    for f in QF06:
        if f.getName() in ['QF06', 'QF06_4', 'QF06_5', 'QF06_6', 'QF06_7',
        'QF06_8']:
            before = CircleApertureNode(0.230/2,0,0,0)
            after = CircleApertureNode(0.230/2,0,0,0)
            mid = CircleApertureNode(0.230/2,0,0,0)
            addAperture(posDict,f,before,after, mid)

    #add four special vacumm pipes in the lattice
    #only used  in phase I
    #R1QD02_pipe = CircleApertureNode(0.1, 0, 0, 0)
    #R2QD11_pipe = CircleApertureNode(0.1, 0, 0, 0)
    #R3QD02_pipe = CircleApertureNode(0.1, 0, 0, 0)
    #R4QD11_pipe = CircleApertureNode(0.1, 0, 0, 0)
    #if QD_pipe:
    #    addAperture(posDict, QD_pipe[0], mid=R1QD02_pipe)
    #    addAperture(posDict, QD_pipe[1], mid=R2QD11_pipe)
    #    addAperture(posDict, QD_pipe[2], mid=R3QD02_pipe)
    #    addAperture(posDict, QD_pipe[3], mid=R4QD11_pipe)
    #else:
    #    addTeapotApertureNode(lattice, 7.66, R1QD02_pipe)
    #    addTeapotApertureNode(lattice, 64.64, R2QD11_pipe)
    #    addTeapotApertureNode(lattice, 121.62, R3QD02_pipe)
    #    addTeapotApertureNode(lattice, 178.61, R4QD11_pipe)

    lattice.initialize()

def add_aperture_phaseII(lattice, machine):
    '''
    Add aperture according to the phaseII design
    '''
    if rank == 0:
        print("Using phaseII design aperture settings!")

    posDict = lattice.getNodePositionsDict()

    latticeLength = lattice.getLength()
    HarmonicNum = 2
    LonPos = 0.002
    phaseMax = 190
    phaseMin = -190
    LonPhaseNode = PhaseApertureNode(latticeLength, HarmonicNum, LonPos, phaseMax, phaseMin)
    addTeapotApertureNode(lattice, LonPos, LonPhaseNode)

    BV = lattice.getNodesForName('BV')
    for f in BV:
        #radius,pos,offsetx,offsety
        name = f.getName()
        after = CircleApertureNode(0.163/2,0,0,0)
        if name == "BV02":
            after = CircleApertureNode(0.163/2,0,0,0.005)
        if name == "BV03":
            after = CircleApertureNode(0.163/2,0,0.0,0.005)
        addAperture(posDict,f,after=after)

    BH = lattice.getNodesForName('BH')
    for f in BH:
        name = f.getName()
        after = RectangleApertureNode(0.241/2,0.165/2,-0.0505,0.005)
        if name == "BH01":
            after = RectangleApertureNode(0.175/2,0.165/2,0.0,0.005)
        if name == "BH04":
            after = RectangleApertureNode(0.241/2,0.165/2,-0.0445,0.005)
        addAperture(posDict,f,after=after)

    #four BCH position [225.095, 227.095, 0.475, 2.475]
    BCH = lattice.getNodesForName('BCH')
    for f in BCH:
        name = f.getName()
        #xshift = 0.0385
        xshift = -0.0505
        before = RectangleApertureNode(0.241/2,0.165/2,0,xshift,0.005)
        after = RectangleApertureNode(0.241/2,0.165/2,0,xshift,0.005)
        mid = RectangleApertureNode(0.241/2,0.165/2,0,xshift,0.005)
        if name == "BCH01":
            before = RectangleApertureNode(0.175/2,0.165/2,0,0,0.005)
            after = RectangleApertureNode(0.175/2,0.165/2,0,0,0.005)
            mid = RectangleApertureNode(0.175/2,0.165/2,0,0,0.005)
        if name == "BCH04":
            before = RectangleApertureNode(0.241/2,0.165/2,0,-0.0445,0.005)
            after = RectangleApertureNode(0.241/2,0.165/2,0,-0.0445,0.005)
            mid = RectangleApertureNode(0.241/2,0.165/2,0,-0.0445,0.005)
        addAperture(posDict,f,before,after, mid)

    #R4DH01_aperture = CircleApertureNode(0.220/2,0,0,0)
    #R1DH01_aperture = CircleApertureNode(0.190/2,0,0,0)

    #R1CAV02,R3CAV01,R3CAV02,R3CAV03,R4CAV01,R4CAV02,R4CAV03
    #R1CAV01_aperture = CircleApertureNode(0.170/2,0,0,0)
    #R2CAV_D_aperture = CircleApertureNode(0.250/2,0,0,0)#R3CAV_D,R4CAV_D

    #COLM_aperture = CircleApertureNode(0.250/2,0,0,0)
    #COLP_aperture = CircleApertureNode(0.280/2,0,0,0)
    #COLS1_aperture = CircleApertureNode(0.228/2,0,0,0)
    #COLS2_aperture = CircleApertureNode(0.228/2,0,0,0)
    #COLS3_aperture = CircleApertureNode(0.228/2,0,0,0)
    #COLS4_aperture = CircleApertureNode(0.228/2,0,0,0)

##########################################################################
#put the aperture node at the entry, middle and exit of the associated node.
#########################################################################
    QF01 = lattice.getNodesForName('QF01')
    QD02 = lattice.getNodesForName('QD02')
    QF03 = lattice.getNodesForName('QF03')
    QF04 = lattice.getNodesForName('QF04')
    QF06 = lattice.getNodesForName('QF06')
    MB = lattice.getNodesForName('bend')

    for f in MB:
        if f.getName() in ['bend', 'bend_3', 'bend_4', 'bend_5', 'bend_6',
                'bend_7', 'bend_8', 'bend_9']:
            before = EllipseApertureNode(0.218/2,0.135/2,0,0,0)
            after = EllipseApertureNode(0.218/2,0.135/2,0,0,0)
            mid = EllipseApertureNode(0.218/2, 0.135/2, 0, 0, 0)
            addAperture(posDict,f,before,after, mid)

    for f in QF01:
        if f.getName() in ['QF01', 'QF01_4', 'QF01_5', 'QF01_6']:
            before = CircleApertureNode(0.183/2,0,0,0)
            after = CircleApertureNode(0.183/2,0,0,0)
            mid = CircleApertureNode(0.183/2,0,0,0)
            addAperture(posDict,f,before,after, mid)

    for f in QD02:
        if f.getName() in ['QD02', 'QD02_3', 'QD02_4', 'QD02_5', 'QD02_6',
        'QD02_7']:
            before = CircleApertureNode(0.248/2,0,0,0)
            after = CircleApertureNode(0.248/2,0,0,0)
            mid = CircleApertureNode(0.248/2,0,0,0)
            addAperture(posDict,f,before,after, mid)

    for f in QF03:
        if f.getName() in ['QF03', 'QF03_4', 'QF03_5', 'QF03_6']:
            before = CircleApertureNode(0.183/2,0,0,0)
            after = CircleApertureNode(0.183/2,0,0,0)
            mid = CircleApertureNode(0.183/2,0,0,0)
            addAperture(posDict,f,before,after, mid)

    for f in QF04:
        if f.getName() in ['QF04', 'QF04_3', 'QF04_4', 'QF04_5']:
            before = CircleApertureNode(0.199/2,0,0,0)
            after = CircleApertureNode(0.199/2,0,0,0)
            mid = CircleApertureNode(0.199/2,0,0,0)
            addAperture(posDict,f,before,after, mid)

    for f in QF06:
        if f.getName() in ['QF06', 'QF06_4', 'QF06_5', 'QF06_6', 'QF06_7',
        'QF06_8']:
            before = CircleApertureNode(0.230/2,0,0,0)
            after = CircleApertureNode(0.230/2,0,0,0)
            mid = CircleApertureNode(0.230/2,0,0,0)
            addAperture(posDict,f,before,after, mid)

    lattice.initialize()


#add aperture to a node
def addAperture(posDict,node,before=None,after=None,mid=None):
    posArray = posDict[node]
    if before:
        before.setPosition(posArray[0])
        node.addChildNode(before, 0)
    if after:
        after.setPosition(posArray[1])
        node.addChildNode(after, 2)
    if mid:
        mid.setPosition((posArray[0]+posArray[1])/2)
        node.addChildNode(mid, 1, 1)
