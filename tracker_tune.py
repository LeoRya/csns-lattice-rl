#!/usr/bin/env pyORBIT
import time
import orbit_mpi
import argparse
from bunch import BunchTwissAnalysis
from lib.timecalc import timecalc
from lib import utils
from orbit.time_dep import time_dep_csns
from lattice_builder import csns_builder
from orbit.diagnostics import addTeapotDiagnosticsNode,TeapotTuneAnalysisNode


class Tracker:

    def __init__(self, configfile = None):
        self.builder = None
        with open(configfile, 'r') as f_in:
            self.builder = csns_builder(f_in)
        self.remain = 0
        self.tracking_time = 0
        comm = orbit_mpi.mpi_comm.MPI_COMM_WORLD
        self.rank = orbit_mpi.MPI_Comm_rank(comm)

    def begin(self):
        ##########################################
        #Begin tracking
        ##########################################
        lattice = self.builder.get_lattice()
        paramsDict = self.builder.get_bunches()
        b = paramsDict["bunch"]
        track = self.builder.get_track_setting()
        starttime = time.time()
        trackturn = track['turn']
        tuneanalysis = TeapotTuneAnalysisNode()
        addTeapotDiagnosticsNode(lattice,0.0009,tuneanalysis)
        if isinstance(lattice,time_dep_csns.TIME_DEP_Lattice):
            rank = orbit_mpi.MPI_Comm_rank(orbit_mpi.mpi_comm.MPI_COMM_WORLD)
            starttime = time.time()  
        #start
            for i in range(trackturn):
                bunchtwissanalysis = BunchTwissAnalysis()
                bunchtwissanalysis.analyzeBunch(b)
                xbeta = bunchtwissanalysis.getBeta(0)
                xalpha = bunchtwissanalysis.getAlpha(0)
                xeta = bunchtwissanalysis.getDispersion(0)
                pxeta = bunchtwissanalysis.getDispersionDerivative(0)
                ybeta = bunchtwissanalysis.getBeta(1)
                yalpha = bunchtwissanalysis.getAlpha(1)
                tuneanalysis.assignTwiss(xbeta ,xalpha ,xeta ,pxeta ,ybeta ,yalpha )
                lattice.setTimeDepStrength(b)
                lattice.trackBunch(b, paramsDict)
                bsize = b.getSizeGlobal()
                if rank == 0 :
                    now = time.time()
                    cost = now-starttime
                    remain = (cost/(i+1))*(trackturn-i-1)
                    print "Track on turn %d, cost %1.4f s,time remain %1.4f s, particles remain %d" % ((i + 1), cost, remain, bsize)
        else:
            rank = orbit_mpi.MPI_Comm_rank(orbit_mpi.mpi_comm.MPI_COMM_WORLD)
            starttime = time.time()  
            for i in xrange(trackturn):
                bunchtwissanalysis = BunchTwissAnalysis()
                bunchtwissanalysis.analyzeBunch(b)
                xbeta = bunchtwissanalysis.getBeta(0)
                xalpha = bunchtwissanalysis.getAlpha(0)
                xeta = bunchtwissanalysis.getDispersion(0)
                pxeta = bunchtwissanalysis.getDispersionDerivative(0)
                ybeta = bunchtwissanalysis.getBeta(1)
                yalpha = bunchtwissanalysis.getAlpha(1)
                tuneanalysis.assignTwiss(xbeta ,xalpha ,xeta ,pxeta ,ybeta ,yalpha )
                lattice.trackBunch(b, paramsDict)
                bsize = b.getSizeGlobal()
                if rank == 0 :
                    now = time.time()
                    cost = now-starttime
                    remain = (cost/(i+1))*(trackturn-i-1)
                    print "Track on turn %d, cost %1.4f s,time remain %1.4f s, particles remain %d" % ((i + 1), cost, remain, bsize)
        endtime = time.time()
        b.compress()
        self.remain=b.getSizeGlobal()
        cst = endtime-starttime
        self.tracking_time = '{:02}:{:02}:{:02}'.format(int(cst) / 3600,
                int(cst) / 60 % 60, cst % 60)
        #self.tracking_time = endtime-starttime
    def __del__(self):
        '''Destructor'''
        if self.builder == None:
            return
        active_diag = self.builder.get_active_diag()
        for fil in active_diag:
            fil.closeFile()
        if self.rank==0:
            print "Total tracking time %s" % (self.tracking_time)
            print "Remain particle size %d" % (self.remain)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("configfile", type=str, help="The config file!")
    args = parser.parse_args()
    tracker = Tracker(args.configfile)
    tracker.begin()
    #try:
    #    tracker.begin()
    #except Exception as e:
    #    raise e
