from matplotlib.pyplot import *
from bunch import Bunch

def plotBunchFromFile(filename):
    b = Bunch()
    b.readBunch(filename)
    plotBunch(b)

def plotBunch(b):
    [x,xp,y,yp,z,zp]=getCoor(b)
    fig,(ax1,ax2,ax3) = subplots(1,3,figsize=(15,10))
    fontsize = {'weight':'normal','size':24}
    fig.suptitle('Tracking',fontsize=20,x=0.53,y=0.95)

    ax1.plot(x,xp,'r.')
    ax2.plot(y,yp,'b.')
    ax3.plot(z,zp,'y.')
    show()

def getCoor(b):
    '''
    get the coordinates of the bunch
    '''
    size = b.getSize()
    x=[];xp=[];y=[];yp=[];z=[];zp=[]
    for i in range(size):
        x.append(b.x(i))
        xp.append(b.px(i))
        y.append(b.y(i))
        yp.append(b.py(i))
        z.append(b.z(i))
        zp.append(b.pz(i))
    return [x,xp,y,yp,z,zp]
