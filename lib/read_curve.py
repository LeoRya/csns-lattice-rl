from itertools import chain

def read_QT_curve(fileName):
    file_in = open(fileName,'r')
    input_lines = file_in.readlines()
    file_in.close()
    input_lines_split = map(lambda i:input_lines[i].split(), range(len(input_lines)))
    #remove commented lines
    for i in input_lines_split[::-1]:
        if ('!' in ''.join(i)) or ('#' in ''.join(i)):
            input_lines_split.remove(i)
    input_list = list(chain.from_iterable(input_lines_split))
    qttuple = []
    for i in xrange(17):
        t1 = [float(j) for j in input_list[i::17]]
        qttuple.append(t1)
    return qttuple

def read_RF_curve(filename, multi=False):
    file_in = open(filename,"r")
    input_lines = file_in.readlines()
    file_in.close()
    #remove commented lines
    for i in input_lines:
        if ('!' in i) or ('#' in i):
            input_lines.remove(i)
    header = input_lines.pop(0)
    harms = [int(i) for i in header.split()]
    rfharms = harms[0]
    input_lines_split = map(lambda i: input_lines[i].replace('=','').replace(';','').split(), range(len(input_lines)))
    input_list = list(chain.from_iterable(input_lines_split))
    N = 2*rfharms+2
    RFVs = []
    RFphases = []
    time = [float(j) for j in input_list[::N]]
    time = tuple(time)
    values = [float(m) for m in input_list[1::N]]
    values = tuple(values)
    #read RFVoltages and RFPhases for multi-harmonics
    for ii in range(rfharms):
        RFV = [float(i) for i in input_list[2+ii*2::N]]
        RFVs.append(tuple(RFV))
        RFphase = [float(k) for k in input_list[3+ii*2::N]]
        RFphases.append(tuple(RFphase))
    if multi:
        return time,values,RFVs,RFphases,harms
    else:
        return time, values, RFVs[0], RFphases[0], harms[1]

def read_dual_RF_curve(filename):
    fd = open(filename, "r")
    tempData = []
    time = []
    RFphase = []
    RF2phase = []
    RFV = []
    RF2V = []
    values = []
    while True:
        templine = fd.readline()
        if templine:
            tempData = templine.split("  ")
        else:
            break

        time.append(float(tempData[0])*1e-3)
        RFV.append(float(tempData[1]))
        RFphase.append(float(tempData[2]))
        RF2V.append(float(tempData[3]))
        RF2phase.append(float(tempData[4]))
        values.append(float(tempData[5]))
        tempData = []

    fd.close()
    time = tuple(time)
    RFV = tuple(RFV)
    RFphase = tuple(RFphase)
    RF2V = tuple(RF2V)
    RF2phase = tuple(RF2phase)
    values = tuple(values)
    return time,RFV,RFphase,RF2V,RF2phase,values

def read_painting_bump(fileName):
    file_in = open(fileName,'r')
    input_lines = file_in.readlines()
    file_in.close()
    input_lines_split = map(lambda i:input_lines[i].split(), range(len(input_lines)))
    #remove commented lines
    for i in input_lines_split[::-1]:
        if ('!' in ''.join(i)) or ('#' in ''.join(i)):
            input_lines_split.remove(i)
    input_list = list(chain.from_iterable(input_lines_split))
    time = [float(j) for j in input_list[::3]]
    time = tuple(time)
    paintingX = [float(j) for j in input_list[1::3]]
    bumpX = tuple(paintingX)
    paintingY = [float(j) for j in input_list[2::3]]
    bumpY = tuple(paintingY)
    return time,bumpX,bumpY

def read_magnet_curve(fileName):
    file_in = open(fileName,'r')
    input_lines = file_in.readlines()
    file_in.close()
    input_lines_split = map(lambda i:input_lines[i].split(), range(len(input_lines)))
    #remove commented lines
    for i in input_lines_split[::-1]:
        if ('!' in ''.join(i)) or ('#' in ''.join(i)):
            input_lines_split.remove(i)
    input_list = list(chain.from_iterable(input_lines_split))
    magnet_tuple = []
    for i in xrange(46):
        t1 = [float(j) for j in input_list[i::46]]
        magnet_tuple.append(t1)
    return magnet_tuple

def read_inj_curve(fileName):
    file_in = open(fileName,'r')
    input_lines = file_in.readlines()
    file_in.close()
    input_lines_split = map(lambda i:input_lines[i].split(), range(len(input_lines)))
    #remove commented lines
    for i in input_lines_split[::-1]:
        if ('!' in ''.join(i)) or ('#' in ''.join(i)):
            input_lines_split.remove(i)
    input_list = list(chain.from_iterable(input_lines_split))
    time = [float(j) for j in input_list[::2]]
    time = tuple(time)
    X = [float(j) for j in input_list[1::2]]
    tupleX = tuple(X)
    return time,tupleX

