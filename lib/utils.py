import os
import sys
import orbit_mpi

from orbit.utils import orbitFinalize

comm = orbit_mpi.mpi_comm.MPI_COMM_WORLD
rank = orbit_mpi.MPI_Comm_rank(comm)

def check_folder(folder):
    '''check if the folder exists or is empty. This is parallel version'''
    if os.path.exists(folder) and os.path.isdir(folder):
        if os.listdir(folder):
            msg = "Warning! The folder %s is not empty, process stop!" % folder
            msg = msg + " Please move out your data in this folder!"
            orbitFinalize(msg)
    else:
        if not os.path.exists(folder): #check again due to parallel calculation
            try:
                os.makedirs(folder)
            except:
                msg = "Thd folder %s already created!" % folder
                print(msg)

def check_dumpfolder(dump, turn):
    '''check if the dump folder exists or is empty. This is parallel version'''
    folder = dump['folder']
    dump_name = dump['name']
    dump_write = dump['writeBunch']
    dump_plot = dump['plotBunch']
    interval = dump['interval']
    dump_pos = dump['positions']
    dump_start = dump['start']
    dump_end = dump['end']
    if dump_end > 20000:
        dump_end =2000
    if turn < dump_end:
        dump_end = turn
    names =[]
    folder = os.path.abspath(folder)
    turns = [i for i in range(0, int(dump_end+1), int(interval)) if i>=dump_start]
    for pos in dump_pos:
        for turn in turns:
            name = str(turn) + '_'+dump_name + '_'+ str(pos)
            if dump_write:
                names.append(name+'.bunch')
            if dump_plot:
                names.append(name+'.png')

    if os.path.exists(folder) and os.path.isdir(folder):
        resu = list(set(os.listdir(folder)) & set(names))
        if resu:
            msg = str(resu) + '\n'
            msg = msg + "Warning! The folder %s is not empty, and the data is conflicted with the files you will create!" % folder
            msg = msg + " Please select a new output folder or move out the data!"
            orbitFinalize(msg)
    else:
        if not os.path.exists(folder): #check again due to parallel calculation
            try:
                os.makedirs(folder)
            except:
                msg = "Thd folder %s already created!" % folder
                print(msg)


def create_folder(folder):
    '''check if the folder exists or is empty. This is parallel version'''
    if os.path.exists(folder) and os.path.isdir(folder):
        if os.listdir(folder):
            msg = "Warning! The folder %s is not empty!" % folder
    else:
        if not os.path.exists(folder): #check again due to parallel calculation
            try:
                os.makedirs(folder)
            except:
                msg = "Thd folder %s already created!" % folder
                print(msg)
