def timecalc(costtime,partpernumber,currentturn,injectionturn,trackingturn):
    if currentturn < injectionturn:
        total = 0
        for i in xrange(currentturn):
            total = total + partpernumber*(i+1) 
        freq = costtime/total
        rem1=0
        for j in xrange(injectionturn-currentturn):
           rem1 = rem1 + partpernumber*(injectionturn+j)
        rem = rem1+partpernumber*injectionturn*(trackingturn-injectionturn)
        remaintime = freq*rem
        return remaintime
    else:
        total = 0
        for i in xrange(injectionturn):
            total = total + partpernumber*(i+1)
        total = total + partpernumber*injectionturn*(currentturn-injectionturn)
        freq = costtime/total
        rem = 0
        rem = rem + partpernumber*injectionturn*(trackingturn-currentturn)
        remaintime = freq*rem
        return remaintime
