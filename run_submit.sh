#!/bin/bash

#local submit jobs with nohup

if [ ! -n "$1" ]
  then
    echo "Usage: `basename $0` <name of the config file> <N-CPUs>"
    exit $E_BADARGS
fi

if [ ! -n "$2" ]
  then
    echo "Usage: `basename $0` <name of the config file> <N CPUs>"
    exit $E_BADARGS
fi

nohup mpirun -np $2 tracker.py $1 > run.log 2>&1 & disown
