#!/usr/bin/env pyORBIT
import os
import json
import time
import orbit_mpi
import argparse
from lib.timecalc import timecalc
from lib import utils
from orbit.time_dep import time_dep_csns
from orbit.utils import orbitFinalize


class Tracker:

    def __init__(self, configfile = None, start_bunch=None):
        self.builder = None
        comm = orbit_mpi.mpi_comm.MPI_COMM_WORLD
        self.rank = orbit_mpi.MPI_Comm_rank(comm)
        if start_bunch and not os.path.exists(start_bunch):
            if self.rank == 0:
                orbitFinalize("Can't find the given bunch file %s!" % start_bunch)
        self.start_bunch=start_bunch
        with open(configfile, 'r') as f_in:
            cfg = json.load(f_in)
            if self.start_bunch:
                cfg['track']['start_bunch'] = self.start_bunch
            if cfg['machine']['phase'] == 2:
                from lattice_builderII import csns_builder
            else:
                from lattice_builder import csns_builder
            self.builder = csns_builder(cfg)
        self.remain = 0
        self.tracking_time = 0
        if self.rank == 0:
            print("Tracking start ...")

    def begin(self):
        ##########################################
        #Begin tracking
        ##########################################
        lattice = self.builder.get_lattice()
        paramsDict = self.builder.get_bunches()
        b = paramsDict["bunch"]
        track = self.builder.get_track_setting()
        trackturn = track['turn']
        start_turn = 0
        if self.start_bunch is not None:
            start_turn = b.bunchAttrInt('Turn')
            filename = os.path.abspath(self.start_bunch)
            if self.rank == 0:
                print("Tracking restart from bunch %s, at turn %d" % (filename, start_turn))
        starttime = time.time()
        info = track['info']
        if isinstance(lattice,time_dep_csns.TIME_DEP_Lattice):
            lattice.setShowInfo(info)
            lattice.setTurns(trackturn) # Tracking for AC mode 
            lattice.trackBunchTurns(b, paramsDict)
        else:
            for i in xrange(trackturn):
                lattice.trackBunch(b, paramsDict) # Tracking for DC mode
                bsize = b.getSizeGlobal()
                if info and self.rank == 0:
                    now = time.time()
                    cost = now-starttime
                    remain = (cost/(i+1))*(trackturn-i-1)
                    curr_turn = b.bunchAttrInt('Turn')
                    print "Track on turn %d, cost %1.4f s,time remain %1.4f s, particles remain %d" % ((curr_turn), cost, remain, bsize)

        endtime = time.time()
        b.compress()
        self.remain=b.getSizeGlobal()
        cst = endtime-starttime
        self.tracking_time = '{:02}:{:02}:{:02}'.format(int(cst) / 3600,
                int(cst) / 60 % 60, cst % 60)
        #self.tracking_time = endtime-starttime
    def __del__(self):
        '''Destructor'''
        if self.builder == None:
            return
        active_diag = self.builder.get_active_diag()
        for fil in active_diag:
            fil.closeFile()
        if self.rank==0:
            print "Total tracking time %s" % (self.tracking_time)
            print "Remain particle size %d" % (self.remain)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("configfile", type=str, help="The config file!")
    parser.add_argument("start_bunch", type=str, nargs='?', default=None, help="The start bunch file!")
    args = parser.parse_args()
    tracker = Tracker(args.configfile, args.start_bunch)
    tracker.begin()
