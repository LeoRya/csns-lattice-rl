import os
import json
import time
from lib import auxils,getBucketLength

lattice_path = auxils.get_lattice_path()

cf = {}
#######################################
#some general parameters
config = dict()
config['name'] = 'design'
config['author'] = 'xiaohanlu'
config['date'] = time.time()

######################################
#machine parameters
machine = dict()
machine['phase'] = 1
machine['model'] = 'AC'
machine['sliced'] = False
machine['energy_inj'] = 0.080
machine['energy_ext'] = 1.6

#####################################
#tracking configuration
track = dict()
track['turn'] = 200
track['start_time'] = 0
track['info'] = True

######################################
# config of the diagnostic nodes
diag = dict()
dump = {}
stat = {}
tune = {}
dtune = {}
bpm = {}
mom = {}
TransAction = {}
diag['dump'] = dump
diag['stat'] = stat
diag['bpm'] = bpm
diag['tune'] = tune
diag['dtune'] = dtune
diag['transAction'] = TransAction
dump['enable'] = True
dump['folder'] = './output/dump/'
dump['name'] = 'dump'
dump['positions'] = [0.001]
dump['interval'] = 100 #every 100 turn dump the bunch
dump['start'] = 1
dump['end'] = float('inf') #no limit in default
dump['plotBunch'] = True
dump['writeBunch'] = True
stat['enable'] = True #write out the beam info every turn at specified postions
stat['folder'] = './output/stat/'
stat['name'] = 'stat.dat'
stat['positions'] = [0.0010001,]
tune['enable'] = False
tune['folder'] = './output/tune/'
tune['name'] = 'tune.dat'
tune['point'] = 20 # do the calculation at this turn
tune['thruput'] = 1000 # how many particles to be calculated
tune['sample_size'] = 10 #how many turns used to do the calculation(before point)
dtune['enable'] = False
dtune['folder'] = './output/dtune/'
mom['enable'] = False
mom['folder'] = './output/mom/'
mom['name'] = 'mom.dat'
mom['positions'] = [0.0001,]
TransAction['enable'] = True
TransAction['ratio_of_particle'] = [0.99, 0.95,]
bpm['folder'] = './output/bpm'
bpm["R1BPM01"] = False
bpm["R1BPM02"] = False
bpm["R1BPM04"] = False
bpm["R1BPM05"] = False
bpm["R1BPM08"] = False
bpm["R1BPM09"] = False
bpm["R1BPM11"] = False
bpm["R1BPM12"] = False
bpm["R2BPM01"] = False
bpm["R2BPM02"] = False
bpm["R2BPM04"] = False
bpm["R2BPM05"] = False
bpm["R2BPM08"] = False
bpm["R2BPM09"] = False
bpm["R2BPM11"] = False
bpm["R2BPM12"] = False
bpm["R3BPM01"] = False
bpm["R3BPM02"] = False
bpm["R3BPM04"] = False
bpm["R3BPM05"] = False
bpm["R3BPM08"] = False
bpm["R3BPM09"] = False
bpm["R3BPM11"] = False
bpm["R3BPM12"] = False
bpm["R4BPM01"] = False
bpm["R4BPM02"] = False
bpm["R4BPM04"] = False
bpm["R4BPM05"] = False
bpm["R4BPM08"] = False
bpm["R4BPM09"] = False
bpm["R4BPM11"] = False
bpm["R4BPM12"] = False

######################################
#the config of injection
inj = {}
beam = {}
dist = {}
foil = {}
inj['beam'] = beam
inj['distribution'] = dist
inj['foil'] = foil
beam['intensity'] = 7.8e12
beam['injectionturn'] = 200
beam['macroperturn'] = 1000 #how many particles will be injected each turn
foil['xmin'] = -0.05
foil['xmax'] = 0.05
foil['ymin'] = -0.05
foil['ymax'] = 0.05
foil['enable'] = False
foil['output'] = './output/stat/'
foil['name'] = 'foil.out'
dist['type'] = 1
dist['order'] = 12
dist['alphax'] = 0.00306
dist['betax'] = 1.6306
dist['alphay'] = -0.10154
dist['betay'] = 1.6369
dist['emitx'] = 0.636
dist['emity'] = 0.616
dist['xcenterpos'] = 0.030
dist['xcentermom'] = 0.0
#dist['xcentermom'] = os.path.join(lattice_path, 'input/angle.inj')
dist['ycenterpos'] = 0.0
dist['ycentermom'] = 0.0
dist['tailfrac'] = 0
dist['tailfac'] = 1
dist['bunchPhaseLength'] = 180 #degree 
dist['chopper_duty'] = 50
dist['emean'] = 0.0802
dist['esigma'] = 0.00015
dist['etrunc'] = 1

####################################
# the config of painting
paint = dict()
paint['enable'] = True
paint['start_time'] = track['start_time']
paint['end_time'] = 0.00039+track['start_time']
paint['start_ampx'] = 0.030 #invalid when use custom
paint['end_ampx'] = 0.00#invalid when use custom
paint['x_type'] = 1 # 1 is the curve: amp(1-sqrt((t-t0)/(t1-t0)))
paint['start_ampy'] = -0.026#invalid when use custom
paint['end_ampy'] = 0.00#invalid when use custom
paint['y_type'] = 2 # 2 is the curve: amp(sqrt(1-(t-t0)/(t1-t0)))
paint['waveform'] = 'custom' #custom, RangeRootT, RangeRootTWithFlattop, flapTop
paint['curve'] = os.path.join(lattice_path, 'input/anti-design-pc1.paint') # only valid when use custom waveform

######################################
#the value of main magnet
magnet = dict()
magnet['BC'] = 0.06316
magnet['KQF01'] = 0.977431 #gradient of quadropule
magnet['KQD02'] = -0.764576
magnet['KQF03'] = 0.876528
magnet['KQF04'] = 0.752955
magnet['KQF06'] = 0.802971
magnet['KSF01'] = 0
magnet['KSD02'] = 0
qt = {}
qt['enable'] = False
qt['R1QT02'] = 0.0
qt['R1QT05'] = 0.0
qt['R1QT08'] = 0.0
qt['R1QT11'] = 0.0
qt['R2QT11'] = 0.0
qt['R2QT08'] = 0.0
qt['R2QT05'] = 0.0
qt['R2QT02'] = 0.0
qt['R3QT02'] = 0.0
qt['R3QT05'] = 0.0
qt['R3QT08'] = 0.0
qt['R3QT11'] = 0.0
qt['R4QT11'] = 0.0
qt['R4QT08'] = 0.0
qt['R4QT05'] = 0.0
qt['R4QT02'] = 0.0
magnet['qt'] = qt

#####################################
#the value of correctors
corr = dict()
dh = {}
dv = {}
tms = {}
corr['model'] = machine['model']
corr['time']=tms
tms['start_time'] = 0
tms['end_time'] = 0.002
tms['time_tuple'] = list(range(21))
corr['dh'] = dh
corr['dv'] = dv
if corr['model'] == "AC":
    zerotuple = 21*[0] #default
else:
    zerotuple = 0
dh['R1DH01'] = zerotuple
dh['R1DH04'] = zerotuple
dh['R1DH09'] = zerotuple
dh['R1DH12'] = zerotuple
dh['R2DH01'] = zerotuple
dh['R2DH04'] = zerotuple
dh['R2DH09'] = zerotuple
dh['R2DH12'] = zerotuple
dh['R3DH01'] = zerotuple
dh['R3DH04'] = zerotuple
dh['R3DH09'] = zerotuple
dh['R3DH12'] = zerotuple
dh['R4DH01'] = zerotuple
dh['R4DH04'] = zerotuple
dh['R4DH09'] = zerotuple
dh['R4DH12'] = zerotuple

dv['R1DV02'] = zerotuple
dv['R1DV05'] = zerotuple
dv['R1DV08'] = zerotuple
dv['R1DV11'] = zerotuple
dv['R2DV02'] = zerotuple
dv['R2DV05'] = zerotuple
dv['R2DV08'] = zerotuple
dv['R2DV11'] = zerotuple
dv['R3DV02'] = zerotuple
dv['R3DV05'] = zerotuple
dv['R3DV08'] = zerotuple
dv['R3DV11'] = zerotuple
dv['R4DV02'] = zerotuple
dv['R4DV05'] = zerotuple
dv['R4DV08'] = zerotuple
dv['R4DV11'] = zerotuple

#####################################
#the config of rf cavity
rf = dict()
rf['model'] = machine['model']
rf['enable'] = True
#for ac
rf['curve'] = os.path.join(lattice_path, 'input/rfdata1.multi')
rf['multi'] = True
rf['tracker'] = 1
#for dc
rf['desync'] = 0
rf['num'] = 2
rf['voltage'] = 2.4e-6
rf['phase'] = 0
rf['tracker'] = 1


###################################
# the config of octupole
octupole = dict()
octupole['enable'] = False
octupole['kl'] = [0.0, 0.0, 0.0, 0.0]  # KL of each octupole , 0 for not using , L=0.2m


#####################################
# the config of collimation
coll = dict()
coll['enable'] = True
coll['mtypeP'] = 6 #tungsten
coll['mtypeS'] = 4 #copper
coll['shape'] = 1
coll['density_fac'] = 1.0
coll['primary'] = [-0.082, 0.082, -0.082, 0.082] #left right down up
#The direction is when the beam come to you,  negative x is left

#for all four secondary collimators, maximum position is 108.8-34.8=74mm
# minimum position is 108.8-68.8=40mm
coll['s1_a'] = 0.1088
coll['s1_shift'] = [0.0348, -0.0348, 0.0348, -0.0348]#left right down up

coll['s2_a'] = 0.1088
coll['s2_shift'] = [0.0348, -0.0348, 0.0348, -0.0348]#left right down up

coll['s3_a'] = 0.1088
coll['s3_shift'] = [0.0348, -0.0348, 0.0348, -0.0348]#left right down up

coll['s4_a'] = 0.1088
coll['s4_shift'] = [0.0348, -0.0348, 0.0348, -0.0348]#left right down up

####################################
#switch of aperture
aper = dict()
aper['enable'] = True
aper['set'] = 0 #0 for design, 1 for empirical (already deprecated), 2 for phaseII design

##########################################
#the config of spacecharge calculation
sc = dict()
sc['enable'] = True
sc['solver'] = 3 #1. SpacechargeCalc2p5D 2. SpacechargeCalc2p5Drb 3.SpacechargeForceCalc2p5D 4.TESTNode
sc['sizex'] = 128
sc['sizey'] = 128
sc['sizez'] = 128

#The boundary condition is only used in SpacechargeCalc2p5D
sc['boundary_enable'] = False
sc['boundary_points'] = 100
sc['free_mode'] = 10
sc['shape'] = 1 # 1. Circle 2. Ellipse 3. Rectangle
sc['shape_a'] = 0.11
sc['shape_b'] = 0.11

#These two parameters are only used in SpacechargeCalc2p5Drb
sc['long_avg_n'] = 3
sc['pip_radius'] = 0.11

#For longitudinal space charge node with Imped
sc['lenable'] = False
sc['b/a'] = 2.0
sc['nMacrosMin'] = 1
sc['useSpaceCharge'] = 1
sc['nBins'] = 128

#Cal bucket length for solver 4
if sc['solver'] == 4:
    sc['bucketLength'] = getBucketLength.calPhiDeltP(rf['curve'])

############################################
#the config of ac magnet
## The magnet strength variation subject to a Fourier series
## f(t) = a+b*sin(w*t+p*pi)+b1*sin(2*w*t+p1*pi)+b2*sin(3*w*t+p2*pi)......
## where a is the DC component; b,b1,b2... are the amplitudes of each order;
## w is the frequency of the machine, for CSNS/RCS which is 25 Hz;
## w is fixed for CSNS/RCS, so there is no need to prepare a dedicated variable for it
## p,p1,p2... are the phase of each order.
ac = dict()
ac['QF01'] = {}
ac['QD02'] = {}
ac['QF03'] = {}
ac['QF04'] = {}
ac['QF06'] = {}
ac['QF01']['a'] = 3.4021155
ac['QF01']['b'] = [2.4246845,]
ac['QF01']['p'] = [-0.5,]
ac['QD02']['a'] = -2.661188
ac['QD02']['b'] = [-1.896612,]
ac['QD02']['p'] = [-0.5,]
ac['QF03']['a'] = 3.0509055
ac['QF03']['b'] = [2.1743775,]
ac['QF03']['p'] = [-0.5,]
ac['QF04']['a'] = 2.6207885
ac['QF04']['b'] = [1.8678335,]
ac['QF04']['p'] = [-0.5,]
ac['QF06']['a'] = 2.7948775
ac['QF06']['b'] = [1.9919065,]
ac['QF06']['p'] = [-0.5,]
ac['BC'] = 0.238146
#ac['BC'] = os.path.join(lattice_path, 'input/angle.inj')
ac['SF'] = 0
ac['SD'] = 0
ac['qtcurve'] = os.path.join(lattice_path, 'input/qtdatazero.dat')
ac['curve'] = os.path.join(lattice_path, 'input/quadrupole_AC.slice')

# switch all the paths into abspath
dump['folder'] = os.path.abspath(dump['folder'])
stat['folder'] = os.path.abspath(stat['folder'])
tune['folder'] = os.path.abspath(tune['folder'])
dtune['folder'] = os.path.abspath(dtune['folder'])
mom['folder'] = os.path.abspath(mom['folder'])
bpm['folder'] = os.path.abspath(bpm['folder'])
paint['curve'] = os.path.abspath(paint['curve'])
rf['curve'] = os.path.abspath(rf['curve'])
ac['qtcurve'] = os.path.abspath(ac['qtcurve'])
ac['curve'] = os.path.abspath(ac['curve'])
###################################################
##ready to write out
cf['config'] = config
cf['machine'] = machine
cf['injection'] = inj
cf['magnet'] = magnet
cf['corrector'] = corr
cf['rfcavity'] = rf
cf['collimator'] = coll
cf['painting'] = paint
cf['spacecharge']=sc
cf['timedependent'] = ac
cf['diagnostic'] = diag
cf['aperture'] = aper
cf['track'] = track
cf['octupole'] = octupole

if __name__ == "__main__":
    with open('config.json', 'w') as f_out:
        json.dump(cf, f_out, indent=2, sort_keys=True)
