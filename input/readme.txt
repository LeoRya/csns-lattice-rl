This file is a description of the file rfdata*.multi / rfdat.dual /*.paint
Naming rules for files： File Name can't include “.” , all the "." used before are replaced by "p".
XXX.multi will be used to simulate the multi harmonic RFNode situation while the XXX.dual files only used to simulate the single or dual harmonic RFNode cases.
time(s) [SynchPhase or Brho] RFVoltage(GV) RFPhase(deg) RFVoltage(2) RFPhase(2) ......
rfdata.multi  is the case RFPhase synchronize with Brho[or synchEnergy], and the second column is Brho
rfdata1.multi is the case RFPhase is zero (treat as phase errors of the rf cavity), and the second column is Brho
rfdata2.multi is the case RFPhase synchronize with Brho[or synchEnergy], and the last second is SynchPhase
rfdata3.multi is the case RFPhase is zero (treat as phase errors of the rf cavity), and the second column is SynchPhase
rfdataFor300MeV.dual is the case that using dual_harmonic_cav Node. From left to right, each columm means "time(s) RFVoltage(GV) RFPhase(deg) RatioVoltage RFPhase2 Brho". Synctron Phase can be calcuated by program automatically, please keep the RFPhase being 0. You can also keep the RatioVoltage 0 to simulate the single harmonic RFNode cases.    

empir.multi contains the real RFVoltage settings of the machine for the moment
(2020-06-03), and the RFPhase is zero

For painting curve.
Time(s), X(m), Y(m)
anti-design-pc1.paint  is an anti-correlated-painting curve. From 0 to 0.39 ms, bump-x from 30mm to 0mm, bump-y from -26 mm to 0 mm.
anti-design-pc2.paint  is an anti-correlated-painting curve. From 0 to 0.39 ms, bump-x from 30mm to 0mm, bump-y from -26 mm to 0 mm. 
anti-neg0.2-pc1.paint  is similar to anti-design-pc1.paint but the time is from -0.2 ms to 0.19 ms.
anti-neg0.2-pc2.paint  is similar to anti-design-pc2.paint but the time is from -0.2 ms to 0.19 ms.
Compared with anti-design-pc1, in the vertical plane, its painting is slower at the edges, but faster at the center.
cor-design-pc1.paint  is a correlated-painting curve. The injection process is from 0 to 0.39 ms, bump-x from 30 mm to 0 mm, bump-y from 0 mm to -20 mm. The painting time should be set 0.409ms.
cor-neg0.2-pc1.paint  is similar to cor-design-pc1.paint but the time is from -0.2 ms to 0.19 ms.
cor-neg0.2-pc3-up-opi.paint  is a correlated-painting curve. The injection process is from -0.2 ms to 0.19 ms, bump-x from 24 mm to 0 mm, bump-y from 0 mm to -17.5 mm. Its rising curve is used for painting. The painting time should be set 0.409ms.
cor-neg0.2-pc3-up-opi2.paint  is a correlated-painting curve. The injection process is from -0.2 ms to 0.19 ms, bump-x from 30 mm to 0 mm, bump-y from 0 mm to -15 mm. Its rising curve is used for painting. The painting time should be set 0.409ms.
CSNSII-cor-pc3-up-test.paint  is a test correlated-painting curve for CSNS-II. The injection process is from -0.245 ms to 0.2547 ms, bump-x from 30 mm to 0 mm, bump-y from 0 mm to -15 mm. Its rising curve is used for painting. The painting time should be set 0.51137 ms.
CSNSII-cor-pc3-up-1.paint  is a test correlated-painting curve for CSNS-II. The injection process is from -0.245 ms to 0.2547 ms, bump-x from 30 mm to 0 mm, bump-y from 0 mm to -25 mm. Its rising curve is used for painting. The painting time should be set 0.51137 ms.

CSNSII-cor-pc3-d-toc-1.paint  is a preliminary optimized correlated-painting curve for CSNS-II whose falling edge is used for painting. The injection process is from -0.245 ms to 0.2547 ms, bump-x from 30 mm to 0 mm, bump-y from 25mm mm to 0. The painting time should be set 0.5 ms.
CSNSII-cor-pc3-u-toc-1.paint  is a preliminary optimized correlated-painting curve for CSNS-II whose rising edge is used for painting. The injection process is from -0.245 ms to 0.2547 ms, bump-x from 30 mm to 0 mm, bump-y from 0 to -25mm mm. The painting time should be set 0.51137 ms.

ug-pc3-d-02-m1.paint is one new correlated painting curve for CSNS-II when the preliminary injection scheme is selected. Its falling edge is used for painting. The injection process is from -0.245 ms to 0.2547 ms, bump-x from 30 mm to 0 mm, bump-y from 30 mm mm to 0. The painting time should be set 0.5 ms.
ug-pc1-d-02-m1.paint is one new anti-correlated painting curve for CSNS-II when the preliminary injection scheme is selected. Its rising edge is used for painting. The injection process is from -0.245 ms to 0.2547 ms, bump-x from 30 mm to 0 mm, bump-y from 0 mm to 30 mm. The painting time should be set 0.51137 ms.
