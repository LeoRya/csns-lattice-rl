#!/usr/bin/env pyORBIT
############################################################
#This is the time-dependent (AC mode) lattice of CSNS/RCS
#@Author: Xiaohan Lu
#@Email: luxh@ihep.ac.cn
#@Copyright (C) 2018 China Spallation Neutron Source (CSNS)
###########################################################
import os
import copy
import json
import argparse
import orbit_mpi
import numpy as np
from math import *
from lib import utils
from bunch import Bunch
from orbit.teapot import teapot
from orbit.utils import consts
from orbit.teapot import TEAPOT_Ring,TEAPOT_Lattice
from orbit.teapot import TEAPOT_MATRIX_Lattice
from orbit.rf_cavities import RFNode, RFLatticeModifications
from orbit.collimation import TeapotCollimatorNewNode, addTeapotCollimatorNode
from orbit.kickernodes import addTeapotKickerNode, TeapotXKickerNode, TeapotYKickerNode
from orbit.kickernodes import RangeRootTWaveform, RangeRootTWithFlattopWaveform
from orbit.kickernodes import flatTopWaveform, CustomWaveform

from orbit.injection import TeapotInjectionNode, addTeapotInjectionNode
from orbit.injection import JohoTransverse, GULongDist
from orbit.foils import TeapotSimpleFoilNode
from orbit.diagnostics import addTeapotDiagnosticsNode,TeapotDumpNode,TeapotTransActionNode
from orbit.diagnostics import TeapotStatLatsNode, TeapotBPMTBTNode
from orbit.diagnostics import TeapotTuneSpreadNode, TeapotTuneMachineNode
from orbit.space_charge.sc2p5d import setSC2p5DAccNodes,setSC2p5DrbAccNodes
from orbit.space_charge.directforce2p5d import setDirectForce2p5DAccNodes,setDirectForce2p5DTESTAccNodes
from orbit.space_charge.sc1d import SC1D_AccNode, addLongitudinalSpaceChargeNode
from spacecharge import SpaceChargeCalc2p5D, SpaceChargeCalc2p5Drb
from spacecharge import SpaceChargeForceCalc2p5D
from spacecharge import SpaceChargeForceCalc2p5DTEST
from spacecharge import Boundary2D
from aperture_rcs import add_aperture_design, add_aperture_empirical,add_aperture_phaseII
from orbit.time_dep import time_dep_csns
from orbit.time_dep.waveform import FourierMagnetWaveform, ConstantMagnetWaveform
from orbit.time_dep.waveform import CustomMagnetWaveform
from lib.read_curve import *
from orbit.diagnostics import TeapotCounterNode
from orbit.time_dep import time_dep_matrix_lattice

class csns_builder:
    '''build the csns rcs lattice'''

    def __init__(self, config):
        if isinstance(config, file):
            cfg = json.load(config)
        elif isinstance(config, dict):
            cfg = config
        else:
            print "Unsupported type %s" %(type(config))
            return
        comm = orbit_mpi.mpi_comm.MPI_COMM_WORLD
        self.rank = orbit_mpi.MPI_Comm_rank(comm)
        self.machine = cfg['machine']
        if self.rank == 0:
            print("The magnets are running in %s model!" % (self.machine['model']))
        magnet = cfg['magnet']
        corr = cfg['corrector']
        rf = cfg['rfcavity']
        collimator = cfg['collimator']
        octupole = cfg['octupole']
        paint = cfg['painting']
        sc = cfg['spacecharge']
        ac = cfg['timedependent']
        inj = cfg['injection']
        diag = cfg['diagnostic']
        aper = cfg['aperture']
        self.track = cfg['track']
        self.active_diag = []
        self.__init_lattice(magnet, octupole)
        self.paramsDict = self.__init_bunch() #initilize main bunch lossbunch lossfoilbunch
        self.__add_RF_cavity(rf, self.paramsDict['bunch'])
        self.__add_collimator(collimator)
        self.__add_octupole(octupole)
        self.__add_painting_magnet(paint)
        #add apertures after add painting kickers
        self.__add_aperture(aper)
        self.__apply_correctors(corr)
        self.__add_spacecharge(sc)
        self.__add_LIspacecharge(sc)
        self.__add_time_lattice(ac)
        self.__add_diagnostics(diag)
        self.__config_injection(inj)
        self.lattice.addChildren() #add bunch wrapper
        self.lattice.initialize()


    def get_lattice(self):
        return self.lattice

    def get_bunches(self):
        return self.paramsDict

    def get_track_setting(self):
        return self.track

    def get_active_diag(self):
        return self.active_diag

    def __config_injection(self, inj):
        '''config injection, this function should be called at the last of
        building the lattice'''
        beam = inj['beam']
        dist = inj['distribution']
        foil = inj['foil']
        intensity = beam['intensity'] # the total number of the particles
        injectionturn = beam['injectionturn'] # injection turn
        macroperturn = beam['macroperturn']# injected particle numbers per turn
        macrosize = intensity/injectionturn/macroperturn

        lostfoilbunch = self.paramsDict["lostbunch"]
        b = self.paramsDict['bunch']
        start_turn = 0
        if 'start_turn' in self.track.keys():
            start_turn = self.track['start_turn']
        else:
            b.macroSize(macrosize)# It's very import for space charge calculation, if not assign, the calculator will crash out
        syncPart = b.getSyncParticle()
        maxturns = injectionturn - start_turn
        if maxturns <= 0:
            return
        ############################################################
        #Add Injection Node to inject particles
        #The injection node should be added at last to
        #make sure it's at the beginning of the lattice
        ###########################################################
        xmin = foil['xmin']
        xmax = foil['xmax']
        ymin = foil['ymin']
        ymax = foil['ymax']
        foilparams = (xmin, xmax, ymin, ymax)
        foil_enable = False
        if 'enable' in foil:
            foil_enable = foil['enable']
            foil_outfile = foil['output'] + foil['name']

        order = dist['order']
        alphax = dist['alphax']
        betax = dist['betax'] #m
        alphay = dist['alphay']
        betay = dist['betay']
        emitxlim = dist['emitx'] * 2*(order + 1) * 1e-6
        emitylim = dist['emity'] * 2*(order + 1) * 1e-6
        xcenterpos = dist['xcenterpos']
        xcentermom = dist['xcentermom']
        ycenterpos = dist['ycenterpos']
        ycentermom = dist['ycentermom']
        if isinstance(xcenterpos, (str, unicode)):
            [time, posx] = read_inj_curve(xcenterpos)
            xcenterpos = CustomWaveform(syncPart, time[0], time[-1], time, posx)
        if isinstance(xcentermom, (str, unicode)):
            [time, momx] = read_inj_curve(xcentermom)
            xcentermom = CustomWaveform(syncPart, time[0], time[-1], time, momx)
        tailfrac = dist['tailfrac']
        tailfac = dist['tailfac']
        length = self.lattice.getLength()
        zlim = float(dist['bunchPhaseLength'])/2 * length/360.
        zmin = -zlim*(1-float(dist['chopper_duty'])/100)#e.g. chopper duty 50. zmin=-zlim/2
        zmax = zlim*(1-float(dist['chopper_duty'])/100)
        emean = dist['emean'] #injection energy GeV, which can differ from the energy of synch particle
        esigma = dist['esigma']
        etrunc = dist['etrunc'] # flag for truncating the distribution
        emin = emean-3*esigma
        emax = emean+3*esigma

        nparts = macroperturn
        xFunc = JohoTransverse(order, alphax, betax, emitxlim, xcenterpos, xcentermom, tailfrac, tailfac)
        yFunc = JohoTransverse(order, alphay, betay, emitylim, ycenterpos, ycentermom, tailfrac, tailfac)
        lFunc = GULongDist(zmin, zmax, syncPart, emean, esigma, etrunc, emin, emax)
        injectionNode = TeapotInjectionNode(nparts,b,lostfoilbunch,foilparams,xFunc,yFunc,lFunc,maxturns)
        #keep the injection node as the first element
        if foil_enable:
            utils.create_folder(foil['output'])
            foilNode = TeapotSimpleFoilNode(xmin, xmax, ymin, ymax, foil_outfile)
            self.lattice.getNodes()[0:1] = [injectionNode, self.lattice.getNodes()[0], foilNode]
        else:
            self.lattice.getNodes()[0:1] = [injectionNode, self.lattice.getNodes()[0]]
        #addTeapotInjectionNode(self.lattice,0,injectionNode)

    def __add_diagnostics(self, diag):
        '''add diagnostic nodes'''
        if 'enable' in diag and (not diag['enable']):
            return
        dump = diag['dump']
        stats = diag['stat']
        bpm = diag['bpm']
        tune = diag['tune']
        dtune = diag['dtune']
        TransAction = diag['transAction']
        #Create clean lattice without diagnostic nodes to avoid duplicated output
        lattice = time_dep_csns.TIME_DEP_Lattice("RCS_without_diag")
        nodes = []
        for node in self.lattice.getNodes():
            if node.getName() not in bpm:
                nodes.append(node)
        lattice.setNodes(nodes)
        lattice.initialize()


        dump_enable = dump['enable']
        if dump_enable:
            dump_folder = dump['folder']
            dump_name = dump['name']
            dump_write = dump['writeBunch']
            dump_plot = dump['plotBunch']
            interval = dump['interval']
            dump_pos = dump['positions']
            dump_start = dump['start']
            dump_end = dump['end']
            trackturn = self.track['turn']
            start_turn = 0
            if 'start_turn' in self.track.keys():
                start_turn = self.track['start_turn']
                dump['start'] = start_turn + 1
            total_turn = trackturn + start_turn
            utils.check_dumpfolder(dump, total_turn)
            for pos in dump_pos:
                name = dump_name + '_' + str(pos)
                dump = TeapotDumpNode(os.path.join(dump_folder, name),
                        interval, total_turn, dump_write, dump_plot)
                dump.setRange(dump_start, dump_end)
                addTeapotDiagnosticsNode(self.lattice, pos, dump)

        tune_enable = tune['enable']
        if tune_enable:
            b_test = Bunch()
            b_test.getSyncParticle().kinEnergy(0.08)
            matrix = TEAPOT_MATRIX_Lattice(lattice,b_test)
            if self.machine['model'] == 'AC':
                matrix = time_dep_matrix_lattice.TIME_DEP_MATRIX_Lattice(lattice, b_test)
            ring_par_dict = matrix.getRingParametersDict()
            tunex = ring_par_dict["fractional tune x"]
            tuney = ring_par_dict["fractional tune y"]
            betax = ring_par_dict["beta x [m]"]
            betay = ring_par_dict["beta y [m]"]
            alphax = ring_par_dict["alpha x"]
            alphay = ring_par_dict["alpha y"]
            etax = ring_par_dict["eta"]
            if abs(tunex) < 0.5:
                tunex = 1 - abs(tunex)
            else:
                tunex = abs(tunex)
            if abs(tuney) < 0.5:
                tuney = 1 - abs(tuney)
            else:
                tuney = abs(tuney)

            tune_folder = tune['folder']
            tune_name = tune['name']
            utils.check_folder(tune_folder)
            node = self.lattice.getNodes()[1]
            name = node.getName()
            node.setFileName(os.path.join(tune_folder, tune_name))
            point = tune['point']
            sample_size = tune['sample_size']
            thruput = tune['thruput']
            node.setSampleSize(sample_size)
            node.setCalcPoint(point)
            node.setThruput(thruput)
            node.assignTwiss(betax, alphax, etax, betay, alphay)
            node.assignTune(tunex, tuney)
            node.activate()
            self.active_diag.append(node)

        dtune_enable = dtune['enable']
        if dtune_enable and self.machine['model'] == 'AC':
            dtune_folder = dtune['folder']
            utils.create_folder(dtune_folder)
            fname = os.path.join(dtune_folder, 'dtune')
            dtune_node = TeapotTuneMachineNode(lattice, fname)
            N = len(self.lattice.getNodes())
            self.lattice.getNodes()[-1:N] = [self.lattice.getNodes()[-1],
            dtune_node]

        # Note that the data is appended to the file, so don't use an exist file
        stats_enable = stats['enable']
        if stats_enable:
            stats_folder = stats['folder']
            utils.create_folder(stats_folder)
            stats_name = stats['name']
            stats_pos = stats['positions']
            for pos in stats_pos:
                name = ('_'+str(pos)+'.').join(stats_name.split('.'))
                stats = TeapotStatLatsNode(os.path.join(stats_folder, name))
                addTeapotDiagnosticsNode(self.lattice, pos, stats)
                self.active_diag.append(stats)

        transAction_enable = TransAction['enable']
        if transAction_enable:
            RatioOfPart = TransAction['ratio_of_particle']
            ActionCal = TeapotTransActionNode(lattice, self.machine["energy_inj"], self.track["start_time"],RatioOfPartList = RatioOfPart)
            pos = 0.000009
            addTeapotDiagnosticsNode(self.lattice, pos, ActionCal)

        bpm_folder = bpm.pop('folder')
        if sum(bpm.values()):
            utils.create_folder(bpm_folder)
        keys = bpm.keys()
        for node in self.lattice.getNodes():
            name = node.getName()
            if (name in keys) and bpm[name]:
                node.activate()
                node.setFileName(os.path.join(bpm_folder, name))
                self.active_diag.append(node)

    def __init_bunch(self):
        ##########################################################
        #Main bunch parameters
        ##########################################################
        b = Bunch()
        if 'start_bunch' in self.track.keys():
            start_bunch = self.track['start_bunch']
            b.readBunch(start_bunch)
            start_turn = b.bunchAttrInt('Turn')
            self.track['start_turn'] = start_turn
        else:
            energy_inj = self.machine['energy_inj']
            syncPart = b.getSyncParticle()
            syncPart.kinEnergy(energy_inj)
            syncPart.time(self.track['start_time'])
        paramsDict={}
        lostbunch = Bunch()
        lostfoilbunch = Bunch()
        paramsDict["lostbunch"] = lostbunch
        paramsDict["bunch"] = b
        paramsDict["lostfoilbunch"] = lostfoilbunch
        lostbunch.addPartAttr("LostParticleAttributes")
        lostbunch.addPartAttr("LostParticleTime")
        return paramsDict

    def __add_RF_cavity(self, rf, b):
        ###############################################
        #Define the RF cavity as Multi_Harmonic_RFNode
        ###############################################
        if not rf["enable"]:
            return
        model = rf['model']
        ZtoPhi = 2.0*pi/self.lattice.getLength()
        length = 0.0
        if model == 'AC':
            curve = rf['curve']
            if self.rank == 0:
                print('rf is running in AC model and using curve %s ' % curve)
            if curve.split(".")[-1] == 'multi' :
                ###############################################
                #Define the RF cavity as Multi_Harmonic_RFNode
                ###############################################
                [time,values,RFV,RFphase,harms] = read_RF_curve(curve, rf['multi'])
                name = "Multi_RFNode"
                accelDict = {}
                accelDict["gammaTrans"] = 4.89
                accelDict["RFHNum"] = harms
                accelDict["RFVoltage"] = RFV
                accelDict["RFPhase"] = RFphase
                accelDict["n_tuple"] = len(time)-1
                accelDict["time"] = time
                accelDict["SyncPhase"] = values
                accelDict["BRho"] = values
                #R1CAV01 = RFNode.SyncPhaseDep_Harmonic_RFNode(ZtoPhi, accelDict,b,length,name)
                R1CAV01 = RFNode.BRhoDep_Multi_Harmonic_RFNode(ZtoPhi, accelDict,b,length,name)
                #R1CAV01 = copy.deepcopy(R1CAV01)
                #R1CAV02 = copy.deepcopy(R1CAV01)
                #R3CAV01 = copy.deepcopy(R1CAV01)
                #R3CAV02 = copy.deepcopy(R1CAV01)
                #R3CAV03 = copy.deepcopy(R1CAV01)
                #R4CAV01 = copy.deepcopy(R1CAV01)
                #R4CAV02 = copy.deepcopy(R1CAV01)
                #R4CAV03 = copy.deepcopy(R1CAV01)
            elif curve.split(".")[-1] == 'dual' :
                ###############################################
                #Define the RF cavity as Dual_Harmonic_RFNode
                ###############################################
                [time,RFV,RFphase,RF2VRatio,RF2Phase,values] = read_dual_RF_curve(curve)
                name = "Dual_RFNode"
                accelDict = {}
                accelDict["gammaTrans"] = 4.89
                accelDict["RFHNum"] = 2
                accelDict["n_tuple"] = len(time)-1
                accelDict["time"] = time
                accelDict["RFPhase2"] = RF2Phase
                accelDict["BRho"] = values
                accelDict["RatioRFHNum"] = 2
                accelDict["RFVoltage"] = RFV
                accelDict["RatioVoltage"] = RF2VRatio
                accelDict["RFPhase"] = RFphase

                R1CAV01 = RFNode.BRhoDep_Dual_Harmonic_RFNode(ZtoPhi, accelDict,b,length,name)
                #R1CAV01 = copy.deepcopy(R1CAV01)
                #R1CAV02 = copy.deepcopy(R1CAV01)
                #R3CAV01 = copy.deepcopy(R1CAV01)
                #R3CAV02 = copy.deepcopy(R1CAV01)
                #R3CAV03 = copy.deepcopy(R1CAV01)
                #R4CAV01 = copy.deepcopy(R1CAV01)
                #R4CAV02 = copy.deepcopy(R1CAV01)
                #R4CAV03 = copy.deepcopy(R1CAV01)
        else:
            if self.rank == 0:
                print('rf is runing in DC model')
            dESync = rf['desync']
            RFHNum = rf['num']
            RFVoltage = rf['voltage']
            RFPhase = rf['phase']
            name = "Harmonic_RFNode"
            R1CAV01 = RFNode.Harmonic_RFNode(ZtoPhi, dESync, RFHNum, RFVoltage,
                RFPhase, b, length, name) # no accelation

        RFLatticeModifications.addRFNode(self.lattice,10.79735,R1CAV01)
        #RFLatticeModifications.addRFNode(lattice,46.18435,R1CAV02)
        #RFLatticeModifications.addRFNode(lattice,160.14435,R3CAV01)
        #RFLatticeModifications.addRFNode(lattice,167.41735,R3CAV02)
        #RFLatticeModifications.addRFNode(lattice,170.127347,R3CAV03)
        #RFLatticeModifications.addRFNode(lattice,172.837344,R4CAV01)
        #RFLatticeModifications.addRFNode(lattice,181.73735,R4CAV02)
        #RFLatticeModifications.addRFNode(lattice,217.12435,R4CAV03)

    def __add_collimator(self, coll):
        #####################################
        #Define the collimator
        ####################################
        if not coll['enable']:
            return
        maP = coll['mtypeP']# material type, 1=carbon, 2=aluminum, 3=iron, 4=copper, 5=tantalum, 6=tungstun, 7=platinum, 8=lead, 9 = black absorber
        maS = coll['mtypeS']
        density_fac = coll['density_fac'] #A multiplier on the density of chosen material, default =1
        #shape = coll['shape'] # shape of the collimator 1=circle, 2=ellipse, 3=one sided flat, 4=two sided flat, 5=rectangular (outside is collimator), 6=rectangular (inside is collimator)
        #for CSNS/RCS the shape is known
        edgeP = coll['primary']
        s1_a = coll['s1_a']
        s1_shift = coll['s1_shift']
        s2_a = coll['s2_a']
        s2_shift = coll['s2_shift']
        s3_a = coll['s3_a']
        s3_shift = coll['s3_shift']
        s4_a = coll['s4_a']
        s4_shift = coll['s4_shift']

        collimatorP = TeapotCollimatorNewNode(0.00017, maP, density_fac, 5,
                edgeP[0], edgeP[1], edgeP[2], edgeP[3], 0., 0., 0.,0.0, "Col_prime")
        collimatorS1 = TeapotCollimatorNewNode(0.2, maS, density_fac, 7, s1_a,
                s1_a, s1_shift[0], s1_shift[1], s1_shift[2], s1_shift[3], 0.,0.0, "Col_sec1")
        collimatorS2 = TeapotCollimatorNewNode(0.2, maS, density_fac, 7, s2_a,
                s2_a, s2_shift[0], s2_shift[1], s2_shift[2], s2_shift[3], 0.0,0.0, "Col_sec2")
        collimatorS3 = TeapotCollimatorNewNode(0.2, maS, density_fac, 7, s3_a,
                s3_a, s3_shift[0], s3_shift[1], s3_shift[2], s3_shift[3], 0.0,0.0, "Col_sec3")
        collimatorS4 = TeapotCollimatorNewNode(0.2, maS, density_fac, 7, s4_a,
                s4_a, s4_shift[0], s4_shift[1], s4_shift[2], s4_shift[3], 0.0,0.0, "Col_sec4")

        addTeapotCollimatorNode(self.lattice, 52.715, collimatorP)
        addTeapotCollimatorNode(self.lattice, 53.713, collimatorS1)
        addTeapotCollimatorNode(self.lattice, 55.313, collimatorS2)
        addTeapotCollimatorNode(self.lattice, 57.313, collimatorS3)
        addTeapotCollimatorNode(self.lattice, 60.113, collimatorS4)
    def __add_octupole(self,octupole):
        ## Test octupole
        if not octupole['enable']:
            return
        octkls = octupole['kl']

        octupole1 = teapot.MultipoleTEAPOT()
        octupole1.setLength(0.2)
        octupole1.setParam("poles", [3])
        octupole1.setParam("kls", [octkls[0]])
        octupole1.setParam("skews", [0])
        octupole2 = teapot.MultipoleTEAPOT()
        octupole2.setLength(0.2)
        octupole2.setParam("poles", [3])
        octupole2.setParam("kls", [octkls[1]])
        octupole2.setParam("skews", [0])
        octupole3 = teapot.MultipoleTEAPOT()
        octupole3.setLength(0.2)
        octupole3.setParam("poles", [3])
        octupole3.setParam("kls", [octkls[2]])
        octupole3.setParam("skews", [0])
        octupole4 = teapot.MultipoleTEAPOT()
        octupole4.setLength(0.2)
        octupole4.setParam("poles", [3])
        octupole4.setParam("kls", [octkls[3]])
        octupole4.setParam("skews", [0])

        if self.machine['phase'] == 1:
            if octkls[0] !=0: addTeapotKickerNode(self.lattice, 24.04, octupole1)
            if octkls[1] !=0: addTeapotKickerNode(self.lattice, 81.02, octupole2)
            if octkls[2] !=0: addTeapotKickerNode(self.lattice, 138.00, octupole3)
            if octkls[3] !=0: addTeapotKickerNode(self.lattice, 194.98, octupole4)





    def __add_painting_magnet(self, paint):
        ######################################################
        # add kickers as painting magnet
        # kicker node should be added before spacecharge node
        ######################################################
        t1 = paint['start_time']
        t2 = paint['end_time'] #s
        startampx = paint['start_ampx']#m
        endampx = paint['end_ampx']
        x_type = paint['x_type']
        startampy = paint['start_ampy']
        endampy = paint['end_ampy']
        y_type = paint['y_type']
        b = self.paramsDict["bunch"]
        syncPart = b.getSyncParticle()

        waveform = paint['waveform']
        if waveform == 'RangeRootTWithFlattop':
            paintingWavex = RangeRootTWithFlattopWaveform(syncPart, t1, t2,
                    startampx, endampx, x_type)
            paintingWavey = RangeRootTWithFlattopWaveform(syncPart, t1, t2,
                    startampy, endampy, y_type)
        elif waveform == 'flatTop':
            paintingWavex = flatTopWaveform(startampx)
            paintingWavey = flatTopWaveform(startampy)
        elif waveform == 'custom':
            if self.rank == 0:
                print('Using painting curve %s ' % paint['curve'])
            [time, ampx, ampy] = read_painting_bump(paint['curve'])
            paintingWavex = CustomWaveform(syncPart, t1, t2, time, ampx)
            paintingWavey = CustomWaveform(syncPart, t1, t2, time, ampy)
        else:
            paintingWavex = RangeRootTWaveform(syncPart, t1, t2, startampx,
                    endampx, x_type)
            paintingWavey = RangeRootTWaveform(syncPart, t1, t2, startampy,
                    endampy, y_type)

        if not paint['enable']:
            paintingWavex = flatTopWaveform(0)
            paintingWavey = flatTopWaveform(0)


        strengthX = 1/2.2
        strengthY = 1/2.2

        BH01 = TeapotXKickerNode(b, strengthX, paintingWavex, 'BH01')
        BH02 = TeapotXKickerNode(b, -strengthX, paintingWavex, 'BH02')
        BH03 = TeapotXKickerNode(b, -strengthX, paintingWavex, 'BH03')
        BH04 = TeapotXKickerNode(b, strengthX, paintingWavex, 'BH04')
        BV01 = TeapotYKickerNode(b, strengthY, paintingWavey, 'BV01')
        BV02 = TeapotYKickerNode(b, -strengthY, paintingWavey, 'BV02')
        BV03 = TeapotYKickerNode(b, -strengthY, paintingWavey, 'BV03')
        BV04 = TeapotYKickerNode(b, strengthY, paintingWavey, 'BV04')

        addTeapotKickerNode(self.lattice, 2.155, BH03)
        addTeapotKickerNode(self.lattice, 4.355, BH04)
        addTeapotKickerNode(self.lattice, 223.565, BH01)
        addTeapotKickerNode(self.lattice, 225.765, BH02)
        addTeapotKickerNode(self.lattice, 2.705, BV03)
        addTeapotKickerNode(self.lattice, 4.905, BV04)
        addTeapotKickerNode(self.lattice, 223.015, BV01)
        addTeapotKickerNode(self.lattice, 225.215, BV02)

    def __add_qt_node(self, qt, Brhoinj):
        ######################################################
        # add trim quadrupoles by liyong
        ######################################################
        KR1QT02=qt['R1QT02'];
        KR1QT05=qt['R1QT05'];
        KR1QT08=qt['R1QT08'];
        KR1QT11=qt['R1QT11'];

        KR2QT11=qt['R1QT11'];
        KR2QT08=qt['R1QT08'];
        KR2QT05=qt['R1QT05'];
        KR2QT02=qt['R1QT02'];

        KR3QT02=qt['R1QT02'];
        KR3QT05=qt['R1QT05'];
        KR3QT08=qt['R1QT08'];
        KR3QT11=qt['R1QT11'];

        KR4QT11=qt['R1QT11'];
        KR4QT08=qt['R1QT08'];
        KR4QT05=qt['R1QT05'];
        KR4QT02=qt['R1QT02'];

        R1QT02=teapot.QuadTEAPOT("R1QT02")
        R1QT02.setLength(0.15)
        R1QT02.addParam("kq",KR1QT02/Brhoinj)
        R1QT05=teapot.QuadTEAPOT("R1QT05")
        R1QT05.setLength(0.15)
        R1QT05.addParam("kq",KR1QT05/Brhoinj)
        R1QT08=teapot.QuadTEAPOT("R1QT08")
        R1QT08.setLength(0.15)
        R1QT08.addParam("kq",KR1QT08/Brhoinj)
        R1QT11=teapot.QuadTEAPOT("R1QT11")
        R1QT11.setLength(0.15)
        R1QT11.addParam("kq",KR1QT11/Brhoinj)

        R2QT02=teapot.QuadTEAPOT("R2QT02")
        R2QT02.setLength(0.15)
        R2QT02.addParam("kq",KR2QT02/Brhoinj)
        R2QT05=teapot.QuadTEAPOT("R2QT05")
        R2QT05.setLength(0.15)
        R2QT05.addParam("kq",KR2QT05/Brhoinj)
        R2QT08=teapot.QuadTEAPOT("R2QT08")
        R2QT08.setLength(0.15)
        R2QT08.addParam("kq",KR2QT08/Brhoinj)
        R2QT11=teapot.QuadTEAPOT("R2QT11")
        R2QT11.setLength(0.15)
        R2QT11.addParam("kq",KR2QT11/Brhoinj)

        R3QT02=teapot.QuadTEAPOT("R3QT02")
        R3QT02.setLength(0.15)
        R3QT02.addParam("kq",KR3QT02/Brhoinj)
        R3QT05=teapot.QuadTEAPOT("R3QT05")
        R3QT05.setLength(0.15)
        R3QT05.addParam("kq",KR3QT05/Brhoinj)
        R3QT08=teapot.QuadTEAPOT("R3QT08")
        R3QT08.setLength(0.15)
        R3QT08.addParam("kq",KR3QT08/Brhoinj)
        R3QT11=teapot.QuadTEAPOT("R3QT11")
        R3QT11.setLength(0.15)
        R3QT11.addParam("kq",KR3QT11/Brhoinj)

        R4QT02=teapot.QuadTEAPOT("R4QT02")
        R4QT02.setLength(0.15)
        R4QT02.addParam("kq",KR4QT02/Brhoinj)
        R4QT05=teapot.QuadTEAPOT("R4QT05")
        R4QT05.setLength(0.15)
        R4QT05.addParam("kq",KR4QT05/Brhoinj)
        R4QT08=teapot.QuadTEAPOT("R4QT08")
        R4QT08.setLength(0.15)
        R4QT08.addParam("kq",KR4QT08/Brhoinj)
        R4QT11=teapot.QuadTEAPOT("R4QT11")
        R4QT11.setLength(0.15)
        R4QT11.addParam("kq",KR4QT11/Brhoinj)

        addTeapotKickerNode(self.lattice, 7.16-0.725-0.075, R1QT02)
        addTeapotKickerNode(self.lattice, 21.87-0.725-0.075, R1QT05)
        addTeapotKickerNode(self.lattice, 35.11+0.725+0.075, R1QT08)
        addTeapotKickerNode(self.lattice, 49.82+0.725+0.075, R1QT11)

        addTeapotKickerNode(self.lattice, 64.14-0.725-0.075, R2QT11)
        addTeapotKickerNode(self.lattice, 78.85-0.725-0.075, R2QT08)
        addTeapotKickerNode(self.lattice, 92.09+0.725+0.075, R2QT05)
        addTeapotKickerNode(self.lattice, 106.8+0.725+0.075, R2QT02)

        addTeapotKickerNode(self.lattice, 121.12-0.725-0.075, R3QT02)
        addTeapotKickerNode(self.lattice, 135.83-0.725-0.075, R3QT05)
        addTeapotKickerNode(self.lattice, 149.07+0.725+0.075, R3QT08)
        addTeapotKickerNode(self.lattice, 163.78+0.725+0.075, R3QT11)

        addTeapotKickerNode(self.lattice, 178.1-0.725-0.075, R4QT11)
        addTeapotKickerNode(self.lattice, 192.81-0.725-0.075, R4QT08)
        addTeapotKickerNode(self.lattice, 206.05+0.725+0.075, R4QT05)
        addTeapotKickerNode(self.lattice, 220.76+0.725+0.075, R4QT02)

    def __add_aperture(self, aper):
        if not aper['enable']:
            return
        if aper['set'] == 0:
            add_aperture_design(self.lattice, self.machine)
        elif aper['set'] == 2:
            add_aperture_phaseII(self.lattice, self.machine)
        else:
            add_aperture_empirical(self.lattice, self.machine)

    def __add_spacecharge(self, sc):
        #############################################################
        #Add 2.5D spacecharge nodes to the lattice
        #############################################################
        if not sc['enable']:
            return
        sizeX = sc['sizex'] #number of grid points in horizontal direction
        sizeY = sc['sizey'] #number of grid points in vertical direction
        sizeZ = sc['sizez']   #number of logitudinal slices in the 2.5D space charge solver
        solver = sc['solver']
        #m if the distance of two nodes less than this value, we will not add sc node in the latter one
        sc_path_length_min = 0.001
        long_avg_n = sc['long_avg_n']
        pip_radius = sc['pip_radius']

        boundary_use = sc['boundary_enable']
        if boundary_use:
            n_boundary_points = sc['boundary_points']
            n_freespace_modes = sc['free_mode']
            sc_shape = sc['shape']
            if sc_shape == 1:
                boundary_shape = "Circle"
            elif sc_shape == 2:
                boundary_shape = "Ellipse"
            elif sc_shape == 3:
                boundary_shape = "Rectangle"
            else:
                boundary_shape = "Circle"
            a_semi = sc['shape_a']
            b_semi = sc['shape_b']
            boundary = Boundary2D(n_boundary_points, n_freespace_modes, boundary_shape,
                a_semi, b_semi)
        else:
            boundary = None

        if solver == 1:
            calc = SpaceChargeCalc2p5D(sizeX, sizeY, sizeZ)
            setSC2p5DAccNodes(self.lattice, sc_path_length_min, calc, boundary)
        elif solver == 2:
            calc = SpaceChargeCalc2p5Drb(sizeX, sizeY, sizeZ)
            calc.setLongAveragingPointsN(long_avg_n)
            setSC2p5DrbAccNodes(self.lattice, sc_path_length_min, calc,
                    pip_radius)
        elif solver == 3:
            calc = SpaceChargeForceCalc2p5D(sizeX, sizeY, sizeZ)
            setDirectForce2p5DAccNodes(self.lattice, sc_path_length_min, calc)
        elif solver == 4:
            calc = SpaceChargeForceCalc2p5DTEST(sizeX, sizeY, sizeZ)
            setDirectForce2p5DTESTAccNodes(self.lattice, sc_path_length_min, calc)
            self.paramsDict["bucketLength"] = sc['bucketLength']
        else:
            calc = SpaceChargeForceCalc2p5D(sizeX, sizeY, sizeZ)
            setDirectForce2p5DAccNodes(self.lattice, sc_path_length_min, calc)

    def __add_LIspacecharge(self, sc):
        '''add longitudinal space charge node with Imped'''
        if not sc['lenable']:
            return
        b_a = sc['b/a']
        length_sc = self.lattice.getLength()
        nMacrosMin = sc['nMacrosMin']
        useSpaceCharge = sc['useSpaceCharge']
        nBins = sc['nBins']
        position = 5.1 #
        sc1Dnode = SC1D_AccNode(b_a, length_sc, nMacrosMin, useSpaceCharge, nBins)
        addLongitudinalSpaceChargeNode(self.lattice, position, sc1Dnode)

    def __apply_correctors(self, corr):
        dh_names = corr['dh'].keys()
        dv_names = corr['dv'].keys()
        model = corr['model']
        tms = corr['time']
        b = self.paramsDict["bunch"]
        syncPart = b.getSyncParticle()
        if self.rank == 0:
            print("The correctors are running in %s model!" % (model))
        for node in self.lattice.getNodes():
            name = node.getName()
            if name in dh_names:
                if model == 'AC':
                    waveformx = CustomWaveform(syncPart, tms['start_time'], tms['end_time'],
                       tms['time_tuple'], corr['dh'][name])
                    node.setParam("kx", 1)
                    node.setWaveform(waveformx)
                else:
                    node.setParam("kx", corr['dh'][name])
            elif name in dv_names:
                if model == 'AC':
                    waveformy = CustomWaveform(syncPart, tms['start_time'], tms['end_time'],
                       tms['time_tuple'], corr['dv'][name])
                    node.setParam("ky", 1)
                    node.setWaveform(waveformy)
                else:
                    node.setParam("ky", corr['dv'][name])

    def __add_time_lattice(self, ac):
        ##################################################
        #prepare to define time dependent lattice
        #################################################
        if self.machine['model'] == 'DC':
            return
        self.lattice.setLatticeOrder()
        ## The magnet strength variation subject to a Fourier series
        ## f(t) = a+b*sin(w*t+p)+b1*sin(2*w*t+p1)+b2*sin(3*w*t+p2)......
        ## where a is the DC component; b,b1,b2... are the amplitudes of each order;
        ## w is the frequency of the machine, for CSNS/RCS which is 25 Hz;
        ## p,p1,p2... are the phase of each order.

        #f01_min = 0.977431
        #f01_max = 5.8268
        QF01_a = ac['QF01']['a']
        QF01_w = 25
        #8 quadrupoles of QF01
        QF01_b = ac['QF01']['b']
        QF01_p = ac['QF01']['p']

        #f02_min = -0.764576
        #f02_max = -4.5578
        QD02_a = ac['QD02']['a']
        QD02_w = 25
        #16 quadrupoles of QD
        QD02_b = ac['QD02']['b']
        QD02_p = ac['QD02']['p']

        # 8 quadrupoles of QF03
        #f03_min = 0.876528
        #f03_max = 5.225283
        QF03_a = ac['QF03']['a']
        QF03_w = 25
        QF03_b = ac['QF03']['b']
        QF03_p = ac['QF03']['p']

        #8 quadrupoles of QF04
        #f04_min = 0.752955
        #f04_max = 4.488622
        QF04_a = ac['QF04']['a']
        QF04_w = 25
        QF04_b = ac['QF04']['b']
        QF04_p = ac['QF04']['p']

        # 8 quadrupoles of QF06
        #f06_min = 0.802971
        #f06_max = 4.786784
        QF06_a = ac['QF06']['a']
        QF06_w = 25
        QF06_b = ac['QF06']['b']
        QF06_p = ac['QF06']['p']

        bc_b = ac['BC']
        if 'qtcurve' in ac.keys():
            qtwave = read_QT_curve(ac['qtcurve'])
        if 'curve' in ac.keys() and self.machine['sliced']:
            print("loading AC slice factor!")
            mag_wave = read_magnet_curve(ac['curve'])
            t=mag_wave[0]
            QF01_slice_factor=mag_wave[1:10]
            QD02_slice_factor=mag_wave[10:19]
            QF03_slice_factor=mag_wave[19:28]
            QF04_slice_factor=mag_wave[28:35]
            QF06_slice_factor=mag_wave[35:46]
            QF01_factor_index=0
            QD02_factor_index=0
            QF03_factor_index=0
            QF04_factor_index=0
            QF06_factor_index=0

        bc_mode = 0 # 0 is costant mode, 1 is waveform mode
        if isinstance(bc_b, (str, unicode)):
            if self.rank==0:
                print('BC is running in AC model and using curve %s ' % bc_b)
            bc_mode = 1
            [time, bc_b] = read_inj_curve(bc_b)
            bc_b = np.array(bc_b)
        BCStr=[bc_b, -1*bc_b , -1*bc_b, bc_b]
        KSF01 = ac['SF'];
        KSD02 = ac['SD'];
        z=0
        qti = 1
        for node in self.lattice.getNodes():
            name =  node.getParam('TPName')
            if name.find('QF01') == 0:
                self.lattice.setTimeDepNode(name,FourierMagnetWaveform(QF01_a,QF01_b,QF01_w,QF01_p))
                if 'curve' in ac.keys() and self.machine['sliced']:
                    node.setParam('factor_wave', CustomMagnetWaveform(t,QF01_slice_factor[QF01_factor_index]))
                    QF01_factor_index+=1
                    if QF01_factor_index>=9:
                        QF01_factor_index-=9
            elif name.find('QD02') == 0:
                self.lattice.setTimeDepNode(name,FourierMagnetWaveform(QD02_a,QD02_b,QD02_w,QD02_p))
                if 'curve' in ac.keys() and self.machine['sliced']:
                    node.setParam('factor_wave', CustomMagnetWaveform(t,QD02_slice_factor[QD02_factor_index]))
                    QD02_factor_index+=1
                    if QD02_factor_index>=9:
                        QD02_factor_index-=9
            elif name.find('QF03') == 0:
                self.lattice.setTimeDepNode(name,FourierMagnetWaveform(QF03_a,QF03_b,QF03_w,QF03_p))
                if 'curve' in ac.keys() and self.machine['sliced']:
                    node.setParam('factor_wave', CustomMagnetWaveform(t,QF03_slice_factor[QF03_factor_index]))
                    QF03_factor_index+=1
                    if QF03_factor_index>=9:
                        QF03_factor_index-=9
            elif name.find('QF04') == 0:
                self.lattice.setTimeDepNode(name,FourierMagnetWaveform(QF04_a,QF04_b,QF04_w,QF04_p))
                if 'curve' in ac.keys() and self.machine['sliced']:
                    node.setParam('factor_wave', CustomMagnetWaveform(t,QF04_slice_factor[QF04_factor_index]))
                    QF04_factor_index+=1
                    if QF04_factor_index>=7:
                        QF04_factor_index-=7
            elif name.find('QF06') == 0:
                self.lattice.setTimeDepNode(name,FourierMagnetWaveform(QF06_a,QF06_b,QF06_w,QF06_p))
                if 'curve' in ac.keys() and self.machine['sliced']:
                    node.setParam('factor_wave', CustomMagnetWaveform(t,QF06_slice_factor[QF06_factor_index]))
                    QF06_factor_index+=1
                    if QF06_factor_index>=11:
                        QF06_factor_index-=11
            elif name.find('BC') == 0:
                if bc_mode == 0:
                    self.lattice.setTimeDepNode(name,ConstantMagnetWaveform(BCStr[z]))
                else:
                    self.lattice.setTimeDepNode(name,CustomMagnetWaveform(time, BCStr[z]))
                z+=1
            elif name.find('SF') == 2:
                self.lattice.setTimeDepNode(name, ConstantMagnetWaveform(KSF01))
            elif name.find('SD') == 2:
                self.lattice.setTimeDepNode(name, ConstantMagnetWaveform(KSD02))
            elif name.find('QT') == 2:
                self.lattice.setTimeDepNode(name, CustomMagnetWaveform(qtwave[0], qtwave[qti]))
                qti+=1
            else:
                pass

    def calc_twiss(self):
        ############################################################
        #calculate the tune and twiss parameters for the ac lattice
        ###########################################################

        print "lattice length: ", self.lattice.getLength()
        proton=0.93827231 #GeV
        b_test = Bunch()
        paramDict = {}
        paramDict["bunch"] = b_test
        paramDict["lostbunch"] = Bunch()
        syncP = b_test.getSyncParticle()
        energy = self.machine['energy_inj'] #GeV
        syncP.kinEnergy(energy)
        syncP.time(self.track['start_time'])
        matrix = TEAPOT_MATRIX_Lattice(self.lattice,b_test)
        if self.machine['model'] == 'AC':
            matrix = time_dep_matrix_lattice.TIME_DEP_MATRIX_Lattice(self.lattice, b_test)
            mat = matrix.getRingMatrix()
        ring_par_dict = matrix.getRingParametersDict()
        tune_x = ring_par_dict["fractional tune x"]
        tune_y = ring_par_dict["fractional tune y"]
        beta_x = ring_par_dict["beta x [m]"]
        a=ring_par_dict["transition energy [GeV]"]
        gamma_trans = ring_par_dict["gamma transition"]
        print '# Energy(GeV),Tunex,Tuney,gamma_trans,compaction_factor'
        print syncP.kinEnergy(),1+tune_x,1+tune_y, gamma_trans, (1/gamma_trans)**2

        if self.machine['model'] == 'AC':
        #set the time and related energy and rebuild, then you can get the twiss parameters in that moment
            b_test = Bunch()
            syncP = b_test.getSyncParticle()
            syncP.time(0.02)
            syncP.kinEnergy(self.machine["energy_ext"])
            matrix.rebuild(b_test)
            mat = matrix.getRingMatrix()

            ring_par_dict = matrix.getRingParametersDict()
            tune_x = ring_par_dict["fractional tune x"]
            tune_y = ring_par_dict["fractional tune y"]
            beta_x = ring_par_dict["beta x [m]"]
            a=ring_par_dict["transition energy [GeV]"]
            gamma_trans = ring_par_dict["gamma transition"]
            print syncP.kinEnergy(),1+tune_x,1+tune_y,gamma_trans,(1/gamma_trans)**2

    def __init_lattice(self, magnet, octupole):
        '''initilize the basic lattice'''

        energy_inj = self.machine['energy_inj']
        light_speed = consts.speed_of_light
        mass = consts.mass_proton
        model = self.machine['model']
        p0 = sqrt((energy_inj+mass)**2-mass**2)/light_speed
        Brhoinj = p0*1e9
        octkls = octupole['kl']

        ######################################
        #define the bend node
        #####################################
        angMB = pi/12
        MB = teapot.BendTEAPOT("bend")
        MB.setLength(2.10)
        MB.addParam("theta",angMB)
        MB.addParam("ea1",angMB/2)
        MB.addParam("ea2",angMB/2)
        MB.addParam("ratio",(0.5,0.5))
        MB.setnParts(8)

        if self.machine['sliced']:
            MB = self.load_sliced_bend()

        R1MB01 = copy.deepcopy(MB)
        R1MB02 = copy.deepcopy(MB)
        R1MB03 = copy.deepcopy(MB)
        R1MB04 = copy.deepcopy(MB)
        R1MB05 = copy.deepcopy(MB)
        R1MB06 = copy.deepcopy(MB)
        R2MB01 = copy.deepcopy(MB)
        R2MB02 = copy.deepcopy(MB)
        R2MB03 = copy.deepcopy(MB)
        R2MB04 = copy.deepcopy(MB)
        R2MB05 = copy.deepcopy(MB)
        R2MB06 = copy.deepcopy(MB)
        R3MB01 = copy.deepcopy(MB)
        R3MB02 = copy.deepcopy(MB)
        R3MB03 = copy.deepcopy(MB)
        R3MB04 = copy.deepcopy(MB)
        R3MB05 = copy.deepcopy(MB)
        R3MB06 = copy.deepcopy(MB)
        R4MB01 = copy.deepcopy(MB)
        R4MB02 = copy.deepcopy(MB)
        R4MB03 = copy.deepcopy(MB)
        R4MB04 = copy.deepcopy(MB)
        R4MB05 = copy.deepcopy(MB)
        R4MB06 = copy.deepcopy(MB)

        angBC=magnet['BC']
        BC01 = teapot.BendTEAPOT("BC01")
        BC01.setLength(0.35)
        BC01.addParam("theta",-angBC)
        BC01.addParam("ea1",0)
        BC01.addParam("ea2",-angBC)
        BC01.addParam("ratio",(0,1))

        BC02 = teapot.BendTEAPOT("BC02")
        BC02.setLength(0.35)
        BC02.addParam("theta",angBC)
        BC02.addParam("ea1",angBC)
        BC02.addParam("ea2",0)
        BC02.addParam("ratio",(1,0))

        BC03 = teapot.BendTEAPOT("BC03")
        BC03.setLength(0.35)
        BC03.addParam("theta",angBC)
        BC03.addParam("ea1",0)
        BC03.addParam("ea2",angBC)
        BC03.addParam("ratio",(0,1))

        BC04 = teapot.BendTEAPOT("BC04")
        BC04.setLength(0.35)
        BC04.addParam("theta",-angBC)
        BC04.addParam("ea1",-angBC)
        BC04.addParam("ea2",0)
        BC04.addParam("ratio",(1,0))

        L001_a = teapot.DriftTEAPOT("L001_a")
        L001_b = teapot.DriftTEAPOT("L001_b")
        #L001_c = teapot.DriftTEAPOT("L001_c")
        L001_c1 = teapot.DriftTEAPOT("L001_c1")
        L001_c2 = teapot.DriftTEAPOT("L001_c2")
        L001_c3 = teapot.DriftTEAPOT("L001_c3")
        L001_c4 = teapot.DriftTEAPOT("L001_c4")
        L001_c=[L001_c1,L001_c2,L001_c3,L001_c4];
        L002 = teapot.DriftTEAPOT("L002")
        #L003 = teapot.DriftTEAPOT("L003")
        L003_1 = teapot.DriftTEAPOT("L003_1")
        L003_2 = teapot.DriftTEAPOT("L003_2")
        L003_3= teapot.DriftTEAPOT("L003_3")
        L003_4= teapot.DriftTEAPOT("L003_4")
        L003 =[L003_1,L003_2,L003_3,L003_4];
        #L004= teapot.DriftTEAPOT("L004")
        L004_1= teapot.DriftTEAPOT("L004_1")
        L004_2= teapot.DriftTEAPOT("L004_2")
        L004 =[L004_1,L004_2];
        #L005= teapot.DriftTEAPOT("L005")
        L005_1= teapot.DriftTEAPOT("L005_1")
        L005_2= teapot.DriftTEAPOT("L005_2")
        L005 =[L005_1,L005_2];
        L006= teapot.DriftTEAPOT("L006")
        L007= teapot.DriftTEAPOT("L007")
        #L008= teapot.DriftTEAPOT("L008")
        L008_1= teapot.DriftTEAPOT("L008_1")
        L008_2= teapot.DriftTEAPOT("L008_2")
        L008_3= teapot.DriftTEAPOT("L008_3")
        L008_4= teapot.DriftTEAPOT("L008_4")
        L008_5= teapot.DriftTEAPOT("L008_5")
        L008_6= teapot.DriftTEAPOT("L008_6")
        L008_7= teapot.DriftTEAPOT("L008_7")
        L008_8= teapot.DriftTEAPOT("L008_8")
        L008 =[L008_1,L008_2,L008_3,L008_4,L008_5,L008_6,L008_7,L008_8]
        #L009= teapot.DriftTEAPOT("L009")
        L009_1= teapot.DriftTEAPOT("L009_1")
        L009_2= teapot.DriftTEAPOT("L009_2")
        L009 =[L009_1,L009_2];
        L010= teapot.DriftTEAPOT("L010")
        L011= teapot.DriftTEAPOT("L011")
        L012= teapot.DriftTEAPOT("L012")
        L013= teapot.DriftTEAPOT("L013")
        L014= teapot.DriftTEAPOT("L014")
        L015= teapot.DriftTEAPOT("L015")
        L016= teapot.DriftTEAPOT("L016")
        L017= teapot.DriftTEAPOT("L017")
        #L018= teapot.DriftTEAPOT("L018")
        L018_1= teapot.DriftTEAPOT("L018_1")
        L018_2= teapot.DriftTEAPOT("L018_2")
        L018 =[L018_1,L018_2];
        #L019= teapot.DriftTEAPOT("L019")
        L019_1= teapot.DriftTEAPOT("L019_1")
        L019_2= teapot.DriftTEAPOT("L019_2")
        L019 =[L019_1,L019_2];
        #L020= teapot.DriftTEAPOT("L020")
        L020_1= teapot.DriftTEAPOT("L020_1")
        L020_2= teapot.DriftTEAPOT("L020_2")
        L020_3= teapot.DriftTEAPOT("L020_3")
        L020_4= teapot.DriftTEAPOT("L020_4")
        L020_5= teapot.DriftTEAPOT("L020_5")
        L020_6= teapot.DriftTEAPOT("L020_6")
        L020_7= teapot.DriftTEAPOT("L020_7")
        L020 =[L020_1,L020_2,L020_3,L020_4,L020_5,L020_6,L020_7]
        #L021= teapot.DriftTEAPOT("L021")
        L021_1= teapot.DriftTEAPOT("L021_1")
        L021_2= teapot.DriftTEAPOT("L021_2")
        L021 =[L021_1,L021_2];
        #L022= teapot.DriftTEAPOT("L022")
        L022_1= teapot.DriftTEAPOT("L022_1")
        L022_2= teapot.DriftTEAPOT("L022_2")
        L022 =[L022_1,L022_2];
        L023= teapot.DriftTEAPOT("L023")
        L024= teapot.DriftTEAPOT("L024")
        L025= teapot.DriftTEAPOT("L025")
        L026= teapot.DriftTEAPOT("L026")
        L027= teapot.DriftTEAPOT("L027")
        L028= teapot.DriftTEAPOT("L028")
        L029= teapot.DriftTEAPOT("L029")
        L030= teapot.DriftTEAPOT("L030")
        #L031= teapot.DriftTEAPOT("L031")
        L031_1= teapot.DriftTEAPOT("L031_1")
        L031_2= teapot.DriftTEAPOT("L031_2")
        L031 =[L031_1,L031_2];
        #L032= teapot.DriftTEAPOT("L032")
        L032_1= teapot.DriftTEAPOT("L032_1")
        L032_2= teapot.DriftTEAPOT("L032_2")
        L032_3= teapot.DriftTEAPOT("L032_3")
        L032_4= teapot.DriftTEAPOT("L032_4")
        L032_5= teapot.DriftTEAPOT("L032_5")
        L032_6= teapot.DriftTEAPOT("L032_6")
        L032_7= teapot.DriftTEAPOT("L032_7")
        L032_8= teapot.DriftTEAPOT("L032_8")
        L032 =[L032_1,L032_2,L032_3,L032_4,L032_5,L032_6,L032_7,L032_8]
        #L033= teapot.DriftTEAPOT("L033")
        L033_1= teapot.DriftTEAPOT("L033_1")
        L033_2= teapot.DriftTEAPOT("L033_2")
        L033 =[L033_1,L033_2];
        L034= teapot.DriftTEAPOT("L034")
        L035= teapot.DriftTEAPOT("L035")
        #L036= teapot.DriftTEAPOT("L036")
        L036_1= teapot.DriftTEAPOT("L036_1")
        L036_2= teapot.DriftTEAPOT("L036_2")
        L036 =[L036_1,L036_2];
        L037= teapot.DriftTEAPOT("L037")
        L038= teapot.DriftTEAPOT("L038")
        #L039= teapot.DriftTEAPOT("L039")
        L039_1= teapot.DriftTEAPOT("L039_1")
        L039_2= teapot.DriftTEAPOT("L039_2")
        L039_3= teapot.DriftTEAPOT("L039_3")
        L039_4= teapot.DriftTEAPOT("L039_4")
        L039_5= teapot.DriftTEAPOT("L039_5")
        L039_6= teapot.DriftTEAPOT("L039_6")
        L039_7= teapot.DriftTEAPOT("L039_7")
        L039_8= teapot.DriftTEAPOT("L039_8")
        L039_9= teapot.DriftTEAPOT("L039_9")
        L039_10= teapot.DriftTEAPOT("L039_10")
        L039_11= teapot.DriftTEAPOT("L039_11")
        L039_12= teapot.DriftTEAPOT("L039_12")
        L039_13= teapot.DriftTEAPOT("L039_13")
        L039_14= teapot.DriftTEAPOT("L039_14")
        L039_15= teapot.DriftTEAPOT("L039_15")
        L039_16= teapot.DriftTEAPOT("L039_16")
        L039_17= teapot.DriftTEAPOT("L039_17")
        L039_18= teapot.DriftTEAPOT("L039_18")
        L039_19= teapot.DriftTEAPOT("L039_19")
        L039_20= teapot.DriftTEAPOT("L039_20")
        L039_21= teapot.DriftTEAPOT("L039_21")
        L039_22= teapot.DriftTEAPOT("L039_22")
        L039_23= teapot.DriftTEAPOT("L039_23")
        L039_24= teapot.DriftTEAPOT("L039_24")
        L039_25= teapot.DriftTEAPOT("L039_25")
        L039 =[L039_1,L039_2,L039_3,L039_4,L039_5,L039_6,L039_7,L039_8,\
        L039_9,L039_10,L039_11,L039_12,L039_13,L039_14,L039_15,L039_16,\
        L039_17,L039_18,L039_19,L039_20,L039_21,L039_22,L039_23,L039_24,L039_25]
        L040= teapot.DriftTEAPOT("L040")
        L041= teapot.DriftTEAPOT("L041")
        #L042= teapot.DriftTEAPOT("L042")
        L042_1= teapot.DriftTEAPOT("L042_1")
        L042_2= teapot.DriftTEAPOT("L042_2")
        L042 =[L042_1,L042_2];
        #L043= teapot.DriftTEAPOT("L043")
        L043_1= teapot.DriftTEAPOT("L043_1")
        L043_2= teapot.DriftTEAPOT("L043_2")
        L043 =[L043_1,L043_2];
        L044= teapot.DriftTEAPOT("L044")
        L045= teapot.DriftTEAPOT("L045")
        #L046= teapot.DriftTEAPOT("L046")
        L046_1= teapot.DriftTEAPOT("L046_1")
        L046_2= teapot.DriftTEAPOT("L046_2")
        L046_3= teapot.DriftTEAPOT("L046_3")
        L046_4= teapot.DriftTEAPOT("L046_4")
        L046_5= teapot.DriftTEAPOT("L046_5")
        L046_6= teapot.DriftTEAPOT("L046_6")
        L046_7= teapot.DriftTEAPOT("L046_7")
        L046_8= teapot.DriftTEAPOT("L046_8")
        L046 =[L046_1,L046_2,L046_3,L046_4,L046_5,L046_6,L046_7,L046_8]
        #L047= teapot.DriftTEAPOT("L047")
        L047_1= teapot.DriftTEAPOT("L047_1")
        L047_2= teapot.DriftTEAPOT("L047_2")
        L047_3= teapot.DriftTEAPOT("L047_3")
        L047 =[L047_1,L047_2,L047_3]
        L048= teapot.DriftTEAPOT("L048")
        L049= teapot.DriftTEAPOT("L049")
        L050= teapot.DriftTEAPOT("L050")
        L051= teapot.DriftTEAPOT("L051")
        L052= teapot.DriftTEAPOT("L052")
        L053= teapot.DriftTEAPOT("L053")
        L054= teapot.DriftTEAPOT("L054")
        L055= teapot.DriftTEAPOT("L055")
        #L056= teapot.DriftTEAPOT("L056")
        L056_1= teapot.DriftTEAPOT("L056_1")
        L056_2= teapot.DriftTEAPOT("L056_2")
        L056 =[L056_1,L056_2];
        #L057= teapot.DriftTEAPOT("L057")
        L057_1= teapot.DriftTEAPOT("L057_1")
        L057_2= teapot.DriftTEAPOT("L057_2")
        L057 =[L057_1,L057_2];
        #L058= teapot.DriftTEAPOT("L058")
        L058_1= teapot.DriftTEAPOT("L058_1")
        L058_2= teapot.DriftTEAPOT("L058_2")
        L058_3= teapot.DriftTEAPOT("L058_3")
        L058_4= teapot.DriftTEAPOT("L058_4")
        L058_5= teapot.DriftTEAPOT("L058_5")
        L058_6= teapot.DriftTEAPOT("L058_6")
        L058_7= teapot.DriftTEAPOT("L058_7")
        L058 =[L058_1,L058_2,L058_3,L058_4,L058_5,L058_6,L058_7]
        #L059.= teapot.DriftTEAPOT("L059")
        L059_1= teapot.DriftTEAPOT("L059_1")
        L059_2= teapot.DriftTEAPOT("L059_2")
        L059 =[L059_1,L059_2];
        #L060= teapot.DriftTEAPOT("L060")
        L060_1= teapot.DriftTEAPOT("L060_1")
        L060_2= teapot.DriftTEAPOT("L060_2")
        L060 =[L060_1,L060_2];
        L061= teapot.DriftTEAPOT("L061")
        L062= teapot.DriftTEAPOT("L062")
        L063= teapot.DriftTEAPOT("L063")
        L064= teapot.DriftTEAPOT("L064")
        L065= teapot.DriftTEAPOT("L065")
        L066= teapot.DriftTEAPOT("L066")
        L067= teapot.DriftTEAPOT("L067")
        L068= teapot.DriftTEAPOT("L068")
        #L069= teapot.DriftTEAPOT("L069")
        L069_1= teapot.DriftTEAPOT("L069_1")
        L069_2= teapot.DriftTEAPOT("L069_2")
        L069_3= teapot.DriftTEAPOT("L069_3")
        L069 =[L069_1,L069_2,L069_3]
        #L070= teapot.DriftTEAPOT("L070")
        L070_1= teapot.DriftTEAPOT("L070_1")
        L070_2= teapot.DriftTEAPOT("L070_2")
        L070_3= teapot.DriftTEAPOT("L070_3")
        L070_4= teapot.DriftTEAPOT("L070_4")
        L070_5= teapot.DriftTEAPOT("L070_5")
        L070 =[L070_1,L070_2,L070_3,L070_4,L070_5]
        L071= teapot.DriftTEAPOT("L071")
        #L072= teapot.DriftTEAPOT("L072")
        L072_1= teapot.DriftTEAPOT("L072_1")
        L072_2= teapot.DriftTEAPOT("L072_2")
        L072_3= teapot.DriftTEAPOT("L072_3")
        L072 =[L072_1,L072_2,L072_3]
        #L073= teapot.DriftTEAPOT("L073")
        L073_1= teapot.DriftTEAPOT("L073_1")
        L073_2= teapot.DriftTEAPOT("L073_2")
        L073 =[L073_1,L073_2];
        #L074= teapot.DriftTEAPOT("L074")
        L074_1= teapot.DriftTEAPOT("L074_1")
        L074_2= teapot.DriftTEAPOT("L074_2")
        L074 =[L074_1,L074_2];
        #L075= teapot.DriftTEAPOT("L075")
        L075_1= teapot.DriftTEAPOT("L075_1")
        L075_2= teapot.DriftTEAPOT("L075_2")
        L075_3= teapot.DriftTEAPOT("L075_3")
        L075_4= teapot.DriftTEAPOT("L075_4")
        L075_5= teapot.DriftTEAPOT("L075_5")
        L075_6= teapot.DriftTEAPOT("L075_6")
        L075_7= teapot.DriftTEAPOT("L075_7")
        L075_8= teapot.DriftTEAPOT("L075_8")
        L075 =[L075_1,L075_2,L075_3,L075_4,L075_5,L075_6,L075_7,L075_8]
        L076= teapot.DriftTEAPOT("L076")
        L077= teapot.DriftTEAPOT("L077")
        #L078.= teapot.DriftTEAPOT("L078")
        L078_1= teapot.DriftTEAPOT("L078_1")
        L078_2= teapot.DriftTEAPOT("L078_2")
        L078_3= teapot.DriftTEAPOT("L078_3")
        L078_4= teapot.DriftTEAPOT("L078_4")
        L078_5= teapot.DriftTEAPOT("L078_5")
        L078_6= teapot.DriftTEAPOT("L078_6")
        L078_7= teapot.DriftTEAPOT("L078_7")
        L078_8= teapot.DriftTEAPOT("L078_8")
        L078_9= teapot.DriftTEAPOT("L078_9")
        L078_10= teapot.DriftTEAPOT("L078_10")
        L078_11= teapot.DriftTEAPOT("L078_11")
        L078_12= teapot.DriftTEAPOT("L078_12")
        L078 =[L078_1,L078_2,L078_3,L078_4,L078_5,L078_6,L078_7,L078_8,\
        L078_9,L078_10,L078_11,L078_12]
        L079= teapot.DriftTEAPOT("L079")
        L080= teapot.DriftTEAPOT("L080")
        L081= teapot.DriftTEAPOT("L081")
        #L082= teapot.DriftTEAPOT("L082")
        L082_1= teapot.DriftTEAPOT("L082_1")
        L082_2= teapot.DriftTEAPOT("L082_2")
        L082 =[L082_1,L082_2];
        L083= teapot.DriftTEAPOT("L083")
        L084= teapot.DriftTEAPOT("L084")
        #L085.= teapot.DriftTEAPOT("L085")
        L085_1= teapot.DriftTEAPOT("L085_1")
        L085_2= teapot.DriftTEAPOT("L085_2")
        L085_3= teapot.DriftTEAPOT("L085_3")
        L085_4= teapot.DriftTEAPOT("L085_4")
        L085_5= teapot.DriftTEAPOT("L085_5")
        L085 =[L085_1,L085_2,L085_3,L085_4,L085_5]
        #L086= teapot.DriftTEAPOT("L086")
        L086_1= teapot.DriftTEAPOT("L086_1")
        L086_2= teapot.DriftTEAPOT("L086_2")
        L086_3= teapot.DriftTEAPOT("L086_3")
        L086_4= teapot.DriftTEAPOT("L086_4")
        L086_5= teapot.DriftTEAPOT("L086_5")
        L086 =[L086_1,L086_2,L086_3,L086_4,L086_5]
        #L087= teapot.DriftTEAPOT("L087")
        L087_1= teapot.DriftTEAPOT("L087_1")
        L087_2= teapot.DriftTEAPOT("L087_2")
        L087_3= teapot.DriftTEAPOT("L087_3")
        L087 =[L087_1,L087_2,L087_3]
        L088= teapot.DriftTEAPOT("L088")
        L089= teapot.DriftTEAPOT("L089")
        L090= teapot.DriftTEAPOT("L090")
        L091= teapot.DriftTEAPOT("L091")
        L092= teapot.DriftTEAPOT("L092")
        L093= teapot.DriftTEAPOT("L093")
        L094= teapot.DriftTEAPOT("L094")
        L095= teapot.DriftTEAPOT("L095")
        #L096= teapot.DriftTEAPOT("L096")
        L096_1= teapot.DriftTEAPOT("L096_1")
        L096_2= teapot.DriftTEAPOT("L096_2")
        L096 =[L096_1,L096_2];
        #L097= teapot.DriftTEAPOT("L097")
        L097_1= teapot.DriftTEAPOT("L097_1")
        L097_2= teapot.DriftTEAPOT("L097_2")
        L097 =[L097_1,L097_2];
        #L098= teapot.DriftTEAPOT("L098")
        L098_1= teapot.DriftTEAPOT("L098_1")
        L098_2= teapot.DriftTEAPOT("L098_2")
        L098_3= teapot.DriftTEAPOT("L098_3")
        L098_4= teapot.DriftTEAPOT("L098_4")
        L098_5= teapot.DriftTEAPOT("L098_5")
        L098_6= teapot.DriftTEAPOT("L098_6")
        L098_7= teapot.DriftTEAPOT("L098_7")
        L098 =[L098_1,L098_2,L098_3,L098_4,L098_5,L098_6,L098_7]
        #L099= teapot.DriftTEAPOT("L099")
        L099_1= teapot.DriftTEAPOT("L099_1")
        L099_2= teapot.DriftTEAPOT("L099_2")
        L099 =[L099_1,L099_2];
        #L100= teapot.DriftTEAPOT("L100")
        L100_1= teapot.DriftTEAPOT("L100_1")
        L100_2= teapot.DriftTEAPOT("L100_2")
        L100 =[L100_1,L100_2];
        L101= teapot.DriftTEAPOT("L101")
        L102= teapot.DriftTEAPOT("L102")
        L103= teapot.DriftTEAPOT("L103")
        L104= teapot.DriftTEAPOT("L104")
        L105= teapot.DriftTEAPOT("L105")
        L106= teapot.DriftTEAPOT("L106")
        L107= teapot.DriftTEAPOT("L107")
        L108= teapot.DriftTEAPOT("L108")
        #L109= teapot.DriftTEAPOT("L109")
        L109_1= teapot.DriftTEAPOT("L109_1")
        L109_2= teapot.DriftTEAPOT("L109_2")
        L109_3= teapot.DriftTEAPOT("L109_3")
        L109 =[L109_1,L109_2,L109_3]
        #L110= teapot.DriftTEAPOT("L110")
        L110_1= teapot.DriftTEAPOT("L110_1")
        L110_2= teapot.DriftTEAPOT("L110_2")
        L110_3= teapot.DriftTEAPOT("L110_3")
        L110_4= teapot.DriftTEAPOT("L110_4")
        L110_5= teapot.DriftTEAPOT("L110_5")
        L110_6= teapot.DriftTEAPOT("L110_6")
        L110_7= teapot.DriftTEAPOT("L110_7")
        L110_8= teapot.DriftTEAPOT("L110_8")
        L110 =[L110_1,L110_2,L110_3,L110_4,L110_5,L110_6,L110_7,L110_8]
        #L111= teapot.DriftTEAPOT("L111")
        L111_1= teapot.DriftTEAPOT("L111_1")
        L111_2= teapot.DriftTEAPOT("L111_2")
        L111 =[L111_1,L111_2];
        L112= teapot.DriftTEAPOT("L112")
        L113= teapot.DriftTEAPOT("L113")
        #L114= teapot.DriftTEAPOT("L114")
        L114_1= teapot.DriftTEAPOT("L114_1")
        L114_2= teapot.DriftTEAPOT("L114_2")
        L114 =[L114_1,L114_2];
        L115= teapot.DriftTEAPOT("L115")
        L116= teapot.DriftTEAPOT("L116")
        #L117= teapot.DriftTEAPOT("L117")
        L117_1= teapot.DriftTEAPOT("L117_1")
        L117_2= teapot.DriftTEAPOT("L117_2")
        L117_3= teapot.DriftTEAPOT("L117_3")
        L117_4= teapot.DriftTEAPOT("L117_4")
        L117_5= teapot.DriftTEAPOT("L117_5")
        L117_6= teapot.DriftTEAPOT("L117_6")
        L117_7= teapot.DriftTEAPOT("L117_7")
        L117_8= teapot.DriftTEAPOT("L117_8")
        L117_9= teapot.DriftTEAPOT("L117_9")
        L117_10= teapot.DriftTEAPOT("L117_10")
        L117_11= teapot.DriftTEAPOT("L117_11")
        L117_12= teapot.DriftTEAPOT("L117_12")
        L117_13= teapot.DriftTEAPOT("L117_13")
        L117_14= teapot.DriftTEAPOT("L117_14")
        L117_15= teapot.DriftTEAPOT("L117_15")
        L117_16= teapot.DriftTEAPOT("L117_16")
        L117_17= teapot.DriftTEAPOT("L117_17")
        L117_18= teapot.DriftTEAPOT("L117_18")
        L117_19= teapot.DriftTEAPOT("L117_19")
        L117_20= teapot.DriftTEAPOT("L117_20")
        L117_21= teapot.DriftTEAPOT("L117_21")
        L117_22= teapot.DriftTEAPOT("L117_22")
        L117_23= teapot.DriftTEAPOT("L117_23")
        L117_24= teapot.DriftTEAPOT("L117_24")
        L117_25= teapot.DriftTEAPOT("L117_25")
        L117 =[L117_1,L117_2,L117_3,L117_4,L117_5,L117_6,L117_7,L117_8,\
        L117_9,L117_10,L117_11,L117_12,L117_13,L117_14,L117_15,L117_16,\
        L117_17,L117_18,L117_19,L117_20,L117_21,L117_22,L117_23,L117_24,L117_25]
        L118= teapot.DriftTEAPOT("L118")
        L119= teapot.DriftTEAPOT("L119")
        #L120= teapot.DriftTEAPOT("L120")
        L120_1= teapot.DriftTEAPOT("L120_1")
        L120_2= teapot.DriftTEAPOT("L120_2")
        L120 =[L120_1,L120_2];
        #L121= teapot.DriftTEAPOT("L121")
        L121_1= teapot.DriftTEAPOT("L121_1")
        L121_2= teapot.DriftTEAPOT("L121_2")
        L121 =[L121_1,L121_2];
        L122= teapot.DriftTEAPOT("L122")
        L123= teapot.DriftTEAPOT("L123")
        #L124= teapot.DriftTEAPOT("L124")
        L124_1= teapot.DriftTEAPOT("L124_1")
        L124_2= teapot.DriftTEAPOT("L124_2")
        L124_3= teapot.DriftTEAPOT("L124_3")
        L124_4= teapot.DriftTEAPOT("L124_4")
        L124_5= teapot.DriftTEAPOT("L124_5")
        L124_6= teapot.DriftTEAPOT("L124_6")
        L124_7= teapot.DriftTEAPOT("L124_7")
        L124_8= teapot.DriftTEAPOT("L124_8")
        L124 =[L124_1,L124_2,L124_3,L124_4,L124_5,L124_6,L124_7,L124_8]
        #L125= teapot.DriftTEAPOT("L125")
        L125_1= teapot.DriftTEAPOT("L125_1")
        L125_2= teapot.DriftTEAPOT("L125_2")
        L125_3= teapot.DriftTEAPOT("L125_3")
        L125 =[L125_1,L125_2,L125_3]
        L126= teapot.DriftTEAPOT("L126")
        L127= teapot.DriftTEAPOT("L127")
        L128= teapot.DriftTEAPOT("L128")
        L129= teapot.DriftTEAPOT("L129")
        L130= teapot.DriftTEAPOT("L130")
        L131= teapot.DriftTEAPOT("L131")
        L132= teapot.DriftTEAPOT("L132")
        L133= teapot.DriftTEAPOT("L133")
        #L134= teapot.DriftTEAPOT("L134")
        L134_1= teapot.DriftTEAPOT("L134_1")
        L134_2= teapot.DriftTEAPOT("L134_2")
        L134 =[L134_1,L134_2];
        #L135= teapot.DriftTEAPOT("L135")
        L135_1= teapot.DriftTEAPOT("L135_1")
        L135_2= teapot.DriftTEAPOT("L135_2")
        L135 =[L135_1,L135_2];
        #L136= teapot.DriftTEAPOT("L136")
        L136_1= teapot.DriftTEAPOT("L136_1")
        L136_2= teapot.DriftTEAPOT("L136_2")
        L136_3= teapot.DriftTEAPOT("L136_3")
        L136_4= teapot.DriftTEAPOT("L136_4")
        L136_5= teapot.DriftTEAPOT("L136_5")
        L136_6= teapot.DriftTEAPOT("L136_6")
        L136_7= teapot.DriftTEAPOT("L136_7")
        L136 =[L136_1,L136_2,L136_3,L136_4,L136_5,L136_6,L136_7]
        #L137= teapot.DriftTEAPOT("L137")
        L137_1= teapot.DriftTEAPOT("L137_1")
        L137_2= teapot.DriftTEAPOT("L137_2")
        L137 =[L137_1,L137_2];
        #L138= teapot.DriftTEAPOT("L138")
        L138_1= teapot.DriftTEAPOT("L138_1")
        L138_2= teapot.DriftTEAPOT("L138_2")
        L138 =[L138_1,L138_2];
        L139= teapot.DriftTEAPOT("L139")
        L140= teapot.DriftTEAPOT("L140")
        L141= teapot.DriftTEAPOT("L141")
        L142= teapot.DriftTEAPOT("L142")
        L143= teapot.DriftTEAPOT("L143")
        L144= teapot.DriftTEAPOT("L144")
        L145= teapot.DriftTEAPOT("L145")
        L146= teapot.DriftTEAPOT("L146")
        #L147= teapot.DriftTEAPOT("L147")
        L147_1= teapot.DriftTEAPOT("L147_1")
        L147_2= teapot.DriftTEAPOT("L147_2")
        L147_3= teapot.DriftTEAPOT("L147_3")
        L147 =[L147_1,L147_2,L147_3]
        #L148= teapot.DriftTEAPOT("L148")
        L148_1= teapot.DriftTEAPOT("L148_1")
        L148_2= teapot.DriftTEAPOT("L148_2")
        L148_3= teapot.DriftTEAPOT("L148_3")
        L148_4= teapot.DriftTEAPOT("L148_4")
        L148_5= teapot.DriftTEAPOT("L148_5")
        L148_6= teapot.DriftTEAPOT("L148_6")
        L148_7= teapot.DriftTEAPOT("L148_7")
        L148_8= teapot.DriftTEAPOT("L148_8")
        L148 =[L148_1,L148_2,L148_3,L148_4,L148_5,L148_6,L148_7,L148_8]
        #L149= teapot.DriftTEAPOT("L149")
        L149_1= teapot.DriftTEAPOT("L149_1")
        L149_2= teapot.DriftTEAPOT("L149_2")
        L149 =[L149_1,L149_2];
        L150= teapot.DriftTEAPOT("L150")
        L151= teapot.DriftTEAPOT("L151")
        #L152= teapot.DriftTEAPOT("L152")
        L152_1= teapot.DriftTEAPOT("L152_1")
        L152_2= teapot.DriftTEAPOT("L152_2")
        L152 =[L152_1,L152_2];
        #L153= teapot.DriftTEAPOT("L153")
        L153_1= teapot.DriftTEAPOT("L153_1")
        L153_2= teapot.DriftTEAPOT("L153_2")
        L153_3= teapot.DriftTEAPOT("L153_3")
        L153_4= teapot.DriftTEAPOT("L153_4")
        L153_5= teapot.DriftTEAPOT("L153_5")
        L153 =[L153_1,L153_2,L153_3,L153_4,L153_5]
        L154= teapot.DriftTEAPOT("L154")
        #L155_a= teapot.DriftTEAPOT("L155_a")
        L155_a1= teapot.DriftTEAPOT("L155_a1")
        L155_a2= teapot.DriftTEAPOT("L155_a2")
        L155_a3= teapot.DriftTEAPOT("L155_a3")
        L155_a4= teapot.DriftTEAPOT("L155_a4")
        L155_a5= teapot.DriftTEAPOT("L155_a5")
        L155_a =[L155_a1,L155_a2,L155_a3,L155_a4,L155_a5]
        L155_b= teapot.DriftTEAPOT("L155_b")
        L155_c= teapot.DriftTEAPOT("L155_c")

        L001_a.setLength(0.3000)
        L001_b.setLength(0.6000)
        #L001_c.setLength(1.9700)
        L001_c1.setLength(0.4925)
        L001_c2.setLength(0.4925)
        L001_c3.setLength(0.4925)
        L001_c4.setLength(0.4925)
        L002.setLength(0.3930)
        #L003.setLength(1.5370)
        L003_1.setLength(0.38425)
        L003_2.setLength(0.38425)
        L003_3.setLength(0.38425)
        L003_4.setLength(0.38425)
        #L004.setLength(0.8000)
        L004_1.setLength(0.4000)
        L004_2.setLength(0.4000)
        #L005.setLength(0.7470)
        L005_1.setLength(0.3735)
        L005_2.setLength(0.3735)
        L006.setLength(0.0000)
        L007.setLength(0.4030)
        #L008.setLength(3.8000)
        L008_1.setLength(0.475)
        L008_2.setLength(0.475)
        L008_3.setLength(0.475)
        L008_4.setLength(0.475)
        L008_5.setLength(0.475)
        L008_6.setLength(0.475)
        L008_7.setLength(0.475)
        L008_8.setLength(0.475)
        #L009.setLength(1.2000)
        L009_1.setLength(0.6000)
        L009_2.setLength(0.6000)
        L010.setLength(0.5730)
        L011.setLength(0.0000)
        L012.setLength(0.2770)
        L013.setLength(0.2500)
        L014.setLength(0.1753)
        L015.setLength(0.2517)
        L016.setLength(0.0000)
        L017.setLength(0.6730)
        #L018.setLength(0.8000)
        L018_1.setLength(0.4000)
        L018_2.setLength(0.4000)
        #L019.setLength(0.9000)
        L019_1.setLength(0.4500)
        L019_2.setLength(0.4500)
        #L020.setLength(3.5000)
        L020_1.setLength(0.5000)
        L020_2.setLength(0.5000)
        L020_3.setLength(0.5000)
        L020_4.setLength(0.5000)
        L020_5.setLength(0.5000)
        L020_6.setLength(0.5000)
        L020_7.setLength(0.5000)
        #L021.setLength(0.9000)
        L021_1.setLength(0.4500)
        L021_2.setLength(0.4500)
        #L022.setLength(0.8000)
        L022_1.setLength(0.4000)
        L022_2.setLength(0.4000)
        L023.setLength(0.5870)
        L024.setLength(0.0000)
        L025.setLength(0.3382)
        L026.setLength(0.1748)
        L027.setLength(0.2500)
        L028.setLength(0.2770)
        L029.setLength(0.0000)
        L030.setLength(0.5730)
        #L031.setLength(1.2000)
        L031_1.setLength(0.6000)
        L031_2.setLength(0.6000)
        #L032.setLength(3.8000)
        L032_1.setLength(0.475)
        L032_2.setLength(0.475)
        L032_3.setLength(0.475)
        L032_4.setLength(0.475)
        L032_5.setLength(0.475)
        L032_6.setLength(0.475)
        L032_7.setLength(0.475)
        L032_8.setLength(0.475)
        #L033.setLength(0.7520)
        L033_1.setLength(0.3760)
        L033_2.setLength(0.3760)
        L034.setLength(0.0000)
        L035.setLength(0.3980)
        #L036.setLength(0.8000)
        L036_1.setLength(0.4000)
        L036_2.setLength(0.4000)
        L037.setLength(0.3170)
        L038.setLength(0.0000)
        #L039.setLength(10.280)
        L039_1.setLength(0.4112)
        L039_2.setLength(0.4112)
        L039_3.setLength(0.4112)
        L039_4.setLength(0.4112)
        L039_5.setLength(0.4112)
        L039_6.setLength(0.4112)
        L039_7.setLength(0.4112)
        L039_8.setLength(0.4112)
        L039_9.setLength(0.4112)
        L039_10.setLength(0.4112)
        L039_11.setLength(0.4112)
        L039_12.setLength(0.4112)
        L039_13.setLength(0.4112)
        L039_14.setLength(0.4112)
        L039_15.setLength(0.4112)
        L039_16.setLength(0.4112)
        L039_17.setLength(0.4112)
        L039_18.setLength(0.4112)
        L039_19.setLength(0.4112)
        L039_20.setLength(0.4112)
        L039_21.setLength(0.4112)
        L039_22.setLength(0.4112)
        L039_23.setLength(0.4112)
        L039_24.setLength(0.4112)
        L039_25.setLength(0.4112)
        L040.setLength(0.0000)
        L041.setLength(0.4030)
        #L042.setLength(0.8000)
        L042_1.setLength(0.4000)
        L042_2.setLength(0.4000)
        #L043.setLength(0.7475)
        L043_1.setLength(0.37375)
        L043_2.setLength(0.37375)
        L044.setLength(0.0000)
        L045.setLength(0.4025)
        #L046.setLength(3.8000)
        L046_1.setLength(0.475)
        L046_2.setLength(0.475)
        L046_3.setLength(0.475)
        L046_4.setLength(0.475)
        L046_5.setLength(0.475)
        L046_6.setLength(0.475)
        L046_7.setLength(0.475)
        L046_8.setLength(0.475)
        #L047.setLength(1.2000)
        L047_1.setLength(0.4000)
        L047_2.setLength(0.4000)
        L047_3.setLength(0.4000)
        L048.setLength(0.5730)
        L049.setLength(0.0000)
        L050.setLength(0.2770)
        L051.setLength(0.2500)
        L052.setLength(0.1748)
        L053.setLength(0.2522)
        L054.setLength(0.0000)
        L055.setLength(0.6730)
        #L056.setLength(0.8000)
        L056_1.setLength(0.4000)
        L056_2.setLength(0.4000)
        #L057.setLength(0.9000)
        L057_1.setLength(0.4500)
        L057_2.setLength(0.4500)
        #L058.setLength(3.5000)
        L058_1.setLength(0.5000)
        L058_2.setLength(0.5000)
        L058_3.setLength(0.5000)
        L058_4.setLength(0.5000)
        L058_5.setLength(0.5000)
        L058_6.setLength(0.5000)
        L058_7.setLength(0.5000)
        #L059.setLength(0.9000)
        L059_1.setLength(0.4500)
        L059_2.setLength(0.4500)
        #L060.setLength(0.8000)
        L060_1.setLength(0.4000)
        L060_2.setLength(0.4000)
        L061.setLength(0.5870)
        L062.setLength(0.0000)
        L063.setLength(0.3377)
        L064.setLength(0.1753)
        L065.setLength(0.2500)
        L066.setLength(0.2740)
        L067.setLength(0.0000)
        L068.setLength(0.5760)
        #L069.setLength(1.2000)
        L069_1.setLength(0.4000)
        L069_2.setLength(0.4000)
        L069_3.setLength(0.4000)
        #L070.setLength(2.2295)
        L070_1.setLength(0.4459)
        L070_2.setLength(0.4459)
        L070_3.setLength(0.4459)
        L070_4.setLength(0.4459)
        L070_5.setLength(0.4459)
        L071.setLength(0.0000)
        #L072.setLength(1.5705)
        L072_1.setLength(0.5235)
        L072_2.setLength(0.5235)
        L072_3.setLength(0.5235)
        #L073.setLength(1.1500)
        L073_1.setLength(0.5750)
        L073_2.setLength(0.5750)
        #L074.setLength(0.8000)
        L074_1.setLength(0.4000)
        L074_2.setLength(0.4000)
        #L075.setLength(3.9700)
        L075_1.setLength(0.49625)
        L075_2.setLength(0.49625)
        L075_3.setLength(0.49625)
        L075_4.setLength(0.49625)
        L075_5.setLength(0.49625)
        L075_6.setLength(0.49625)
        L075_7.setLength(0.49625)
        L075_8.setLength(0.49625)
        L076.setLength(0.0000)
        L077.setLength(0.3930)
        #L078.setLength(6.2770)
        L078_1.setLength(6.2770/12)
        L078_2.setLength(6.2770/12)
        L078_3.setLength(6.2770/12)
        L078_4.setLength(6.2770/12)
        L078_5.setLength(6.2770/12)
        L078_6.setLength(6.2770/12)
        L078_7.setLength(6.2770/12)
        L078_8.setLength(6.2770/12)
        L078_9.setLength(6.2770/12)
        L078_10.setLength(6.2770/12)
        L078_11.setLength(6.2770/12)
        L078_12.setLength(6.2770/12)
        L079.setLength(0.3600)
        L080.setLength(0.4650)
        L081.setLength(0.3350)
        #L082.setLength(0.7470)
        L082_1.setLength(0.3735)
        L082_2.setLength(0.3735)
        L083.setLength(0.0000)
        L084.setLength(0.4030)
        #L085.setLength(1.6825)
        L085_1.setLength(0.3365)
        L085_2.setLength(0.3365)
        L085_3.setLength(0.3365)
        L085_4.setLength(0.3365)
        L085_5.setLength(0.3365)
        #L086.setLength(2.1175)
        L086_1.setLength(0.4235)
        L086_2.setLength(0.4235)
        L086_3.setLength(0.4235)
        L086_4.setLength(0.4235)
        L086_5.setLength(0.4235)
        #L087.setLength(1.2000)
        L087_1.setLength(0.4000)
        L087_2.setLength(0.4000)
        L087_3.setLength(0.4000)
        L088.setLength(0.5730)
        L089.setLength(0.0000)
        L090.setLength(0.2770)
        L091.setLength(0.2500)
        L092.setLength(0.1753)
        L093.setLength(0.2517)
        L094.setLength(0.0000)
        L095.setLength(0.6730)
        #L096.setLength(0.8000)
        L096_1.setLength(0.4000)
        L096_2.setLength(0.4000)
        #L097.setLength(0.9000)
        L097_1.setLength(0.4500)
        L097_2.setLength(0.4500)
        #L098.setLength(3.5000)
        L098_1.setLength(0.5000)
        L098_2.setLength(0.5000)
        L098_3.setLength(0.5000)
        L098_4.setLength(0.5000)
        L098_5.setLength(0.5000)
        L098_6.setLength(0.5000)
        L098_7.setLength(0.5000)
        #L099.setLength(0.9000)
        L099_1.setLength(0.4500)
        L099_2.setLength(0.4500)
        #L100.setLength(0.8000)
        L100_1.setLength(0.4000)
        L100_2.setLength(0.4000)
        L101.setLength(0.5870)
        L102.setLength(0.0000)
        L103.setLength(0.3382)
        L104.setLength(0.1748)
        L105.setLength(0.2500)
        L106.setLength(0.2770)
        L107.setLength(0.0000)
        L108.setLength(0.5730)
        #L109.setLength(1.2000)
        L109_1.setLength(0.4000)
        L109_2.setLength(0.4000)
        L109_3.setLength(0.4000)
        #L110.setLength(3.8000)
        L110_1.setLength(0.475)
        L110_2.setLength(0.475)
        L110_3.setLength(0.475)
        L110_4.setLength(0.475)
        L110_5.setLength(0.475)
        L110_6.setLength(0.475)
        L110_7.setLength(0.475)
        L110_8.setLength(0.475)
        #L111.setLength(0.7520)
        L111_1.setLength(0.3760)
        L111_2.setLength(0.3760)
        L112.setLength(0.0000)
        L113.setLength(0.3980)
        #L114.setLength(0.8000)
        L114_1.setLength(0.4000)
        L114_2.setLength(0.4000)
        L115.setLength(0.3170)
        L116.setLength(0.0000)
        #L117.setLength(10.280)
        L117_1.setLength(0.4112)
        L117_2.setLength(0.4112)
        L117_3.setLength(0.4112)
        L117_4.setLength(0.4112)
        L117_5.setLength(0.4112)
        L117_6.setLength(0.4112)
        L117_7.setLength(0.4112)
        L117_8.setLength(0.4112)
        L117_9.setLength(0.4112)
        L117_10.setLength(0.4112)
        L117_11.setLength(0.4112)
        L117_12.setLength(0.4112)
        L117_13.setLength(0.4112)
        L117_14.setLength(0.4112)
        L117_15.setLength(0.4112)
        L117_16.setLength(0.4112)
        L117_17.setLength(0.4112)
        L117_18.setLength(0.4112)
        L117_19.setLength(0.4112)
        L117_20.setLength(0.4112)
        L117_21.setLength(0.4112)
        L117_22.setLength(0.4112)
        L117_23.setLength(0.4112)
        L117_24.setLength(0.4112)
        L117_25.setLength(0.4112)
        L118.setLength(0.0000)
        L119.setLength(0.4030)
        #L120.setLength(0.8000)
        L120_1.setLength(0.4000)
        L120_2.setLength(0.4000)
        #L121.setLength(0.7470)
        L121_1.setLength(0.3735)
        L121_2.setLength(0.3735)
        L122.setLength(0.0000)
        L123.setLength(0.4030)
        #L124.setLength(3.8000)
        L124_1.setLength(0.475)
        L124_2.setLength(0.475)
        L124_3.setLength(0.475)
        L124_4.setLength(0.475)
        L124_5.setLength(0.475)
        L124_6.setLength(0.475)
        L124_7.setLength(0.475)
        L124_8.setLength(0.475)
        #L125.setLength(1.2000)
        L125_1.setLength(0.4000)
        L125_2.setLength(0.4000)
        L125_3.setLength(0.4000)
        L126.setLength(0.5730)
        L127.setLength(0.0000)
        L128.setLength(0.2770)
        L129.setLength(0.2500)
        L130.setLength(0.1748)
        L131.setLength(0.2522)
        L132.setLength(0.0000)
        L133.setLength(0.6730)
        #L134.setLength(0.8000)
        L134_1.setLength(0.4000)
        L134_2.setLength(0.4000)
        #L135.setLength(0.9000)
        L135_1.setLength(0.4500)
        L135_2.setLength(0.4500)
        #L136.setLength(3.5000)
        L136_1.setLength(0.5000)
        L136_2.setLength(0.5000)
        L136_3.setLength(0.5000)
        L136_4.setLength(0.5000)
        L136_5.setLength(0.5000)
        L136_6.setLength(0.5000)
        L136_7.setLength(0.5000)
        #L137.setLength(0.9000)
        L137_1.setLength(0.4500)
        L137_2.setLength(0.4500)
        #L138.setLength(0.8000)
        L138_1.setLength(0.4000)
        L138_2.setLength(0.4000)
        L139.setLength(0.5870)
        L140.setLength(0.0000)
        L141.setLength(0.3377)
        L142.setLength(0.1753)
        L143.setLength(0.2500)
        L144.setLength(0.2770)
        L145.setLength(0.0000)
        L146.setLength(0.5730)
        #L147.setLength(1.2000)
        L147_1.setLength(0.4000)
        L147_2.setLength(0.4000)
        L147_3.setLength(0.4000)
        #L148.setLength(3.8000)
        L148_1.setLength(0.475)
        L148_2.setLength(0.475)
        L148_3.setLength(0.475)
        L148_4.setLength(0.475)
        L148_5.setLength(0.475)
        L148_6.setLength(0.475)
        L148_7.setLength(0.475)
        L148_8.setLength(0.475)
        #L149.setLength(0.7520)
        L149_1.setLength(0.3760)
        L149_2.setLength(0.3760)
        L150.setLength(0.0000)
        L151.setLength(0.3980)
        #L152.setLength(0.8000)
        L152_1.setLength(0.4000)
        L152_2.setLength(0.4000)
        #L153.setLength(1.8670)
        L153_1.setLength(0.3734)
        L153_2.setLength(0.3734)
        L153_3.setLength(0.3734)
        L153_4.setLength(0.3734)
        L153_5.setLength(0.3734)
        L154.setLength(0.0000)
        #L155_a.setLength(2.0330)
        L155_a1.setLength(0.4066)
        L155_a2.setLength(0.4066)
        L155_a3.setLength(0.4066)
        L155_a4.setLength(0.4066)
        L155_a5.setLength(0.4066)
        L155_b.setLength(0.6000)
        L155_c.setLength(0.3000)

        if self.machine['sliced']:
              L001_a.setLength(0.3000)
              L001_b.setLength(0.6000)
              #L001_c.setLength(1.9700)
              L001_c1.setLength(0.4925)
              L001_c2.setLength(0.4925)
              L001_c3.setLength(0.4925)
              L001_c4.setLength(0.4925)
              L002.setLength(0.3930)
              #L003.setLength(1.5370-0.3)
              L003_1.setLength(0.30925)
              L003_2.setLength(0.30925)
              L003_3.setLength(0.30925)
              L003_4.setLength(0.30925)
              #L004.setLength(0.8000-0.6)
              L004_1.setLength(0.1500)
              L004_2.setLength(0.1500)
              #L005.setLength(0.7470-0.3)
              L005_1.setLength(0.2735)
              L005_2.setLength(0.2735)
              L006.setLength(0.0000)
              L007.setLength(0.4030-0.3)
              #L008.setLength(3.8000-0.7)
              L008_1.setLength(0.3875)
              L008_2.setLength(0.3875)
              L008_3.setLength(0.3875)
              L008_4.setLength(0.3875)
              L008_5.setLength(0.3875)
              L008_6.setLength(0.3875)
              L008_7.setLength(0.3875)
              L008_8.setLength(0.3875)
              #L009.setLength(1.2000-0.8)
              L009_1.setLength(0.2000)
              L009_2.setLength(0.2000)
              L010.setLength(0.5730-0.4)
              L011.setLength(0.0000)
              L012.setLength(0.2770)
              L013.setLength(0.2500-0.3+0.126)
              L014.setLength(0.1753-0.3+0.126)
              L015.setLength(0.2517)
              L016.setLength(0.0000)
              L017.setLength(0.6730-0.3+0.1)
              #L018.setLength(0.8000-0.6)
              L018_1.setLength(0.1500)
              L018_2.setLength(0.1500)
              #L019.setLength(0.9000-0.7)
              L019_1.setLength(0.100)
              L019_2.setLength(0.100)
              #L020.setLength(3.5000-0.8)
              L020_1.setLength(2.7/7)
              L020_2.setLength(2.7/7)
              L020_3.setLength(2.7/7)
              L020_4.setLength(2.7/7)
              L020_5.setLength(2.7/7)
              L020_6.setLength(2.7/7)
              L020_7.setLength(2.7/7)
              #L021.setLength(0.9000-0.7)
              L021_1.setLength(0.100)
              L021_2.setLength(0.100)
              #L022.setLength(0.8000-0.6)
              L022_1.setLength(0.1500)
              L022_2.setLength(0.1500)
              L023.setLength(0.5870-0.3+0.1)
              L024.setLength(0.0000)
              L025.setLength(0.3382)
              L026.setLength(0.1748-0.3+0.126)
              L027.setLength(0.2500-0.3+0.126)
              L028.setLength(0.2770)
              L029.setLength(0.0000)
              L030.setLength(0.5730-0.4)
              #L031.setLength(1.2000)
              L031_1.setLength(0.2000)
              L031_2.setLength(0.2000)
              #L032.setLength(3.8000-0.7)
              L032_1.setLength(0.3875)
              L032_2.setLength(0.3875)
              L032_3.setLength(0.3875)
              L032_4.setLength(0.3875)
              L032_5.setLength(0.3875)
              L032_6.setLength(0.3875)
              L032_7.setLength(0.3875)
              L032_8.setLength(0.3875)
              #L033.setLength(0.7520)
              L033_1.setLength(0.2260)
              L033_2.setLength(0.2260)
              L034.setLength(0.0000)
              L035.setLength(0.3980-0.3+0.1)
              #L036.setLength(0.8000)
              L036_1.setLength(0.1500)
              L036_2.setLength(0.1500)
              L037.setLength(0.3170-0.3)
              L038.setLength(0.0000)
              #L039.setLength(10.280)
              L039_1.setLength(0.4112)
              L039_2.setLength(0.4112)
              L039_3.setLength(0.4112)
              L039_4.setLength(0.4112)
              L039_5.setLength(0.4112)
              L039_6.setLength(0.4112)
              L039_7.setLength(0.4112)
              L039_8.setLength(0.4112)
              L039_9.setLength(0.4112)
              L039_10.setLength(0.4112)
              L039_11.setLength(0.4112)
              L039_12.setLength(0.4112)
              L039_13.setLength(0.4112)
              L039_14.setLength(0.4112)
              L039_15.setLength(0.4112)
              L039_16.setLength(0.4112)
              L039_17.setLength(0.4112)
              L039_18.setLength(0.4112)
              L039_19.setLength(0.4112)
              L039_20.setLength(0.4112)
              L039_21.setLength(0.4112)
              L039_22.setLength(0.4112)
              L039_23.setLength(0.4112)
              L039_24.setLength(0.4112)
              L039_25.setLength(0.4112)
              L040.setLength(0.0000)
              L041.setLength(0.4030-0.3)
              #L042.setLength(0.8000)
              L042_1.setLength(0.1500)
              L042_2.setLength(0.1500)
              #L043.setLength(0.7475)
              L043_1.setLength(0.27375)
              L043_2.setLength(0.27375)
              L044.setLength(0.0000)
              L045.setLength(0.4025-0.3)
              #L046.setLength(3.8000)
              L046_1.setLength(0.3875)
              L046_2.setLength(0.3875)
              L046_3.setLength(0.3875)
              L046_4.setLength(0.3875)
              L046_5.setLength(0.3875)
              L046_6.setLength(0.3875)
              L046_7.setLength(0.3875)
              L046_8.setLength(0.3875)
              #L047.setLength(1.2000)
              L047_1.setLength(0.4000/3)
              L047_2.setLength(0.4000/3)
              L047_3.setLength(0.4000/3)
              L048.setLength(0.5730-0.4)
              L049.setLength(0.0000)
              L050.setLength(0.2770)
              L051.setLength(0.2500-0.3+0.126)
              L052.setLength(0.1748-0.3+0.126)
              L053.setLength(0.2522)
              L054.setLength(0.0000)
              L055.setLength(0.6730-0.3+0.1)
              #L056.setLength(0.8000)
              L056_1.setLength(0.1500)
              L056_2.setLength(0.1500)
              #L057.setLength(0.9000)
              L057_1.setLength(0.100)
              L057_2.setLength(0.100)
              #L058.setLength(3.5000-0.8)
              L058_1.setLength(2.7/7)
              L058_2.setLength(2.7/7)
              L058_3.setLength(2.7/7)
              L058_4.setLength(2.7/7)
              L058_5.setLength(2.7/7)
              L058_6.setLength(2.7/7)
              L058_7.setLength(2.7/7)
              #L059.setLength(0.9000-0.7)
              L059_1.setLength(0.100)
              L059_2.setLength(0.100)
              #L060.setLength(0.8000-0.6)
              L060_1.setLength(0.1500)
              L060_2.setLength(0.1500)
              L061.setLength(0.5870-0.3+0.1)
              L062.setLength(0.0000)
              L063.setLength(0.3377)
              L064.setLength(0.1753-0.3+0.126)
              L065.setLength(0.2500-0.3+0.126)
              L066.setLength(0.2740)
              L067.setLength(0.0000)
              L068.setLength(0.5760-0.4)
              #L069.setLength(1.2000)
              L069_1.setLength(0.4000/3)
              L069_2.setLength(0.4000/3)
              L069_3.setLength(0.4000/3)
              #L070.setLength(2.2295)
              L070_1.setLength(0.3659)
              L070_2.setLength(0.3659)
              L070_3.setLength(0.3659)
              L070_4.setLength(0.3659)
              L070_5.setLength(0.3659)
              L071.setLength(0.0000)
              #L072.setLength(1.5705)
              L072_1.setLength(0.4235)
              L072_2.setLength(0.4235)
              L072_3.setLength(0.4235)
              #L073.setLength(1.1500)
              L073_1.setLength(0.3250)
              L073_2.setLength(0.3250)
              #L074.setLength(0.8000)
              L074_1.setLength(0.1500)
              L074_2.setLength(0.1500)
              #L075.setLength(3.9700)
              L075_1.setLength(0.45875)
              L075_2.setLength(0.45875)
              L075_3.setLength(0.45875)
              L075_4.setLength(0.45875)
              L075_5.setLength(0.45875)
              L075_6.setLength(0.45875)
              L075_7.setLength(0.45875)
              L075_8.setLength(0.45875)
              L076.setLength(0.0000)
              L077.setLength(0.3930)
              #L078.setLength(6.2770)
              L078_1.setLength(6.2770/12)
              L078_2.setLength(6.2770/12)
              L078_3.setLength(6.2770/12)
              L078_4.setLength(6.2770/12)
              L078_5.setLength(6.2770/12)
              L078_6.setLength(6.2770/12)
              L078_7.setLength(6.2770/12)
              L078_8.setLength(6.2770/12)
              L078_9.setLength(6.2770/12)
              L078_10.setLength(6.2770/12)
              L078_11.setLength(6.2770/12)
              L078_12.setLength(6.2770/12)
              L079.setLength(0.3600-0.3)
              L080.setLength(0.4650-0.3)
              L081.setLength(0.3350-0.3+0.1)
              #L082.setLength(0.7470)
              L082_1.setLength(0.2735)
              L082_2.setLength(0.2735)
              L083.setLength(0.0000)
              L084.setLength(0.4030-0.3)
              #L085.setLength(1.6825)
              L085_1.setLength(0.2765)
              L085_2.setLength(0.2765)
              L085_3.setLength(0.2765)
              L085_4.setLength(0.2765)
              L085_5.setLength(0.2765)
              #L086.setLength(2.1175)
              L086_1.setLength(0.3435)
              L086_2.setLength(0.3435)
              L086_3.setLength(0.3435)
              L086_4.setLength(0.3435)
              L086_5.setLength(0.3435)
              #L087.setLength(1.2000)
              L087_1.setLength(0.4000/3)
              L087_2.setLength(0.4000/3)
              L087_3.setLength(0.4000/3)
              L088.setLength(0.5730-0.4)
              L089.setLength(0.0000)
              L090.setLength(0.2770)
              L091.setLength(0.2500-0.3+0.126)
              L092.setLength(0.1753-0.3+0.126)
              L093.setLength(0.2517)
              L094.setLength(0.0000)
              L095.setLength(0.6730-0.3+0.1)
              #L096.setLength(0.8000)
              L096_1.setLength(0.1500)
              L096_2.setLength(0.1500)
              #L097.setLength(0.9000)
              L097_1.setLength(0.100)
              L097_2.setLength(0.100)
              #L098.setLength(3.5000)
              L098_1.setLength(2.7/7)
              L098_2.setLength(2.7/7)
              L098_3.setLength(2.7/7)
              L098_4.setLength(2.7/7)
              L098_5.setLength(2.7/7)
              L098_6.setLength(2.7/7)
              L098_7.setLength(2.7/7)
              #L099.setLength(0.9000)
              L099_1.setLength(0.100)
              L099_2.setLength(0.100)
              #L100.setLength(0.8000)
              L100_1.setLength(0.1500)
              L100_2.setLength(0.1500)
              L101.setLength(0.5870-0.3+0.1)
              L102.setLength(0.0000)
              L103.setLength(0.3382)
              L104.setLength(0.1748-0.3+0.126)
              L105.setLength(0.2500-0.3+0.126)
              L106.setLength(0.2770)
              L107.setLength(0.0000)
              L108.setLength(0.5730-0.4)
              #L109.setLength(1.2000)
              L109_1.setLength(0.4000/3)
              L109_2.setLength(0.4000/3)
              L109_3.setLength(0.4000/3)
              #L110.setLength(3.8000)
              L110_1.setLength(0.3875)
              L110_2.setLength(0.3875)
              L110_3.setLength(0.3875)
              L110_4.setLength(0.3875)
              L110_5.setLength(0.3875)
              L110_6.setLength(0.3875)
              L110_7.setLength(0.3875)
              L110_8.setLength(0.3875)
              #L111.setLength(0.7520)
              L111_1.setLength(0.2260)
              L111_2.setLength(0.2260)
              L112.setLength(0.0000)
              L113.setLength(0.3980-0.3+0.1)
              #L114.setLength(0.8000)
              L114_1.setLength(0.1500)
              L114_2.setLength(0.1500)
              L115.setLength(0.3170-0.3)
              L116.setLength(0.0000)
              #L117.setLength(10.280)
              L117_1.setLength(0.4112)
              L117_2.setLength(0.4112)
              L117_3.setLength(0.4112)
              L117_4.setLength(0.4112)
              L117_5.setLength(0.4112)
              L117_6.setLength(0.4112)
              L117_7.setLength(0.4112)
              L117_8.setLength(0.4112)
              L117_9.setLength(0.4112)
              L117_10.setLength(0.4112)
              L117_11.setLength(0.4112)
              L117_12.setLength(0.4112)
              L117_13.setLength(0.4112)
              L117_14.setLength(0.4112)
              L117_15.setLength(0.4112)
              L117_16.setLength(0.4112)
              L117_17.setLength(0.4112)
              L117_18.setLength(0.4112)
              L117_19.setLength(0.4112)
              L117_20.setLength(0.4112)
              L117_21.setLength(0.4112)
              L117_22.setLength(0.4112)
              L117_23.setLength(0.4112)
              L117_24.setLength(0.4112)
              L117_25.setLength(0.4112)
              L118.setLength(0.0000)
              L119.setLength(0.4030-0.3)
              #L120.setLength(0.8000)
              L120_1.setLength(0.1500)
              L120_2.setLength(0.1500)
              #L121.setLength(0.7470)
              L121_1.setLength(0.2735)
              L121_2.setLength(0.2735)
              L122.setLength(0.0000)
              L123.setLength(0.4030-0.3)
              #L124.setLength(3.8000)
              L124_1.setLength(0.3875)
              L124_2.setLength(0.3875)
              L124_3.setLength(0.3875)
              L124_4.setLength(0.3875)
              L124_5.setLength(0.3875)
              L124_6.setLength(0.3875)
              L124_7.setLength(0.3875)
              L124_8.setLength(0.3875)
              #L125.setLength(1.2000)
              L125_1.setLength(0.4000/3)
              L125_2.setLength(0.4000/3)
              L125_3.setLength(0.4000/3)
              L126.setLength(0.5730-0.4)
              L127.setLength(0.0000)
              L128.setLength(0.2770)
              L129.setLength(0.2500-0.3+0.126)
              L130.setLength(0.1748-0.3+0.126)
              L131.setLength(0.2522)
              L132.setLength(0.0000)
              L133.setLength(0.6730-0.3+0.1)
              #L134.setLength(0.8000)
              L134_1.setLength(0.1500)
              L134_2.setLength(0.1500)
              #L135.setLength(0.9000)
              L135_1.setLength(0.100)
              L135_2.setLength(0.100)
              #L136.setLength(3.5000)
              L136_1.setLength(2.7/7)
              L136_2.setLength(2.7/7)
              L136_3.setLength(2.7/7)
              L136_4.setLength(2.7/7)
              L136_5.setLength(2.7/7)
              L136_6.setLength(2.7/7)
              L136_7.setLength(2.7/7)
              #L137.setLength(0.9000)
              L137_1.setLength(0.100)
              L137_2.setLength(0.100)
              #L138.setLength(0.8000)
              L138_1.setLength(0.1500)
              L138_2.setLength(0.1500)
              L139.setLength(0.5870-0.3+0.1)
              L140.setLength(0.0000)
              L141.setLength(0.3377)
              L142.setLength(0.1753-0.3+0.126)
              L143.setLength(0.2500-0.3+0.126)
              L144.setLength(0.2770)
              L145.setLength(0.0000)
              L146.setLength(0.5730-0.4)
              #L147.setLength(1.2000)
              L147_1.setLength(0.4000/3)
              L147_2.setLength(0.4000/3)
              L147_3.setLength(0.4000/3)
              #L148.setLength(3.8000)
              L148_1.setLength(0.3875)
              L148_2.setLength(0.3875)
              L148_3.setLength(0.3875)
              L148_4.setLength(0.3875)
              L148_5.setLength(0.3875)
              L148_6.setLength(0.3875)
              L148_7.setLength(0.3875)
              L148_8.setLength(0.3875)
              #L149.setLength(0.7520)
              L149_1.setLength(0.2260)
              L149_2.setLength(0.2260)
              L150.setLength(0.0000)
              L151.setLength(0.3980-0.3+0.1)
              #L152.setLength(0.8000)
              L152_1.setLength(0.1500)
              L152_2.setLength(0.1500)
              #L153.setLength(1.8670)
              L153_1.setLength(0.3134)
              L153_2.setLength(0.3134)
              L153_3.setLength(0.3134)
              L153_4.setLength(0.3134)
              L153_5.setLength(0.3134)
              L154.setLength(0.0000)
              #L155_a.setLength(2.0330)
              L155_a1.setLength(0.4066)
              L155_a2.setLength(0.4066)
              L155_a3.setLength(0.4066)
              L155_a4.setLength(0.4066)
              L155_a5.setLength(0.4066)
              L155_b.setLength(0.6000)
              L155_c.setLength(0.3000)

        ##########################################
        # Define quadrupoles
        #########################################
        KQF01 = magnet['KQF01']
        KQD02 = magnet['KQD02']
        KQF03 = magnet['KQF03']
        KQF04 = magnet['KQF04']
        KQF06 = magnet['KQF06']

        #The main quadrupoles in the ring
        QF01 = teapot.QuadTEAPOT("QF01")
        QF01.setLength(0.41)
        QF01.addParam("kq",KQF01/Brhoinj)
        #QF01.addParam("poles",[2,3])
        #QF01.addParam("kls",[0.8e-3,0.8e-3])
        #QF01.addParam("skews",[0,0])

        QD02 = teapot.QuadTEAPOT("QD02")
        QD02.setLength(0.9)
        QD02.addParam("kq",KQD02/Brhoinj)
        #QD02.addParam("poles",[2,3])
        #QD02.addParam("kls",[0.8e-3,0.8e-3])
        #QD02.addParam("skews",[0,0])

        QF03 = teapot.QuadTEAPOT("QF03")
        QF03.setLength(0.41)
        QF03.addParam("kq",KQF03/Brhoinj)
        #QF03.addParam("poles",[2,3])
        #QF03.addParam("kls",[0.8e-3,0.8e-3])
        #QF03.addParam("skews",[0,0])

        QF04 = teapot.QuadTEAPOT("QF04")
        QF04.setLength(0.45)
        QF04.addParam("kq",KQF04/Brhoinj)
        #QF04.addParam("poles",[2,3])
        #QF04.addParam("kls",[0.8e-3,0.8e-3])
        #QF04.addParam("skews",[0,0])

        QF06 = teapot.QuadTEAPOT("QF06")
        QF06.setLength(0.62)
        QF06.addParam("kq",KQF06/Brhoinj)
        #QF06.addParam("poles",[2,3])
        #QF06.addParam("kls",[0.8e-3,0.8e-3])
        #QF06.addParam("skews",[0,0])
        if self.machine['sliced']:
            [QF01, QD02, QF03, QF04, QF06] = self.load_sliced_quad(KQF01/Brhoinj,
                    KQD02/Brhoinj, KQF03/Brhoinj, KQF04/Brhoinj, KQF06/Brhoinj)

        R1QF01 = copy.deepcopy(QF01)
        R1QD02 = copy.deepcopy(QD02)
        R1QF03 = copy.deepcopy(QF03)
        R1QF04 = copy.deepcopy(QF04)
        R1QD05 = copy.deepcopy(QD02)
        R1QF06 = copy.deepcopy(QF06)
        R1QF07 = copy.deepcopy(QF06)
        R1QD08 = copy.deepcopy(QD02)
        R1QF09 = copy.deepcopy(QF04)
        R1QF10 = copy.deepcopy(QF03)
        R1QD11 = copy.deepcopy(QD02)
        R1QF12 = copy.deepcopy(QF01)
        R2QF01 = copy.deepcopy(QF01)
        R2QD02 = copy.deepcopy(QD02)
        R2QF03 = copy.deepcopy(QF03)
        R2QF04 = copy.deepcopy(QF04)
        R2QD05 = copy.deepcopy(QD02)
        R2QF06 = copy.deepcopy(QF06)
        R2QF07 = copy.deepcopy(QF06)
        R2QD08 = copy.deepcopy(QD02)
        R2QF09 = copy.deepcopy(QF04)
        R2QF10 = copy.deepcopy(QF03)
        R2QD11 = copy.deepcopy(QD02)
        R2QF12 = copy.deepcopy(QF01)
        R3QF01 = copy.deepcopy(QF01)
        R3QD02 = copy.deepcopy(QD02)
        R3QF03 = copy.deepcopy(QF03)
        R3QF04 = copy.deepcopy(QF04)
        R3QD05 = copy.deepcopy(QD02)
        R3QF06 = copy.deepcopy(QF06)
        R3QF07 = copy.deepcopy(QF06)
        R3QD08 = copy.deepcopy(QD02)
        R3QF09 = copy.deepcopy(QF04)
        R3QF10 = copy.deepcopy(QF03)
        R3QD11 = copy.deepcopy(QD02)
        R3QF12 = copy.deepcopy(QF01)
        R4QF01 = copy.deepcopy(QF01)
        R4QD02 = copy.deepcopy(QD02)
        R4QF03 = copy.deepcopy(QF03)
        R4QF04 = copy.deepcopy(QF04)
        R4QD05 = copy.deepcopy(QD02)
        R4QF06 = copy.deepcopy(QF06)
        R4QF07 = copy.deepcopy(QF06)
        R4QD08 = copy.deepcopy(QD02)
        R4QF09 = copy.deepcopy(QF04)
        R4QF10 = copy.deepcopy(QF03)
        R4QD11 = copy.deepcopy(QD02)
        R4QF12 = copy.deepcopy(QF01)
        

        if not self.machine['sliced']:
            R1QF01.setParam('subname','R1QF01')
            R1QD02.setParam('subname','R1QD02')
            R1QF03.setParam('subname','R1QF03')
            R1QF04.setParam('subname','R1QF04')
            R1QD05.setParam('subname','R1QD05')
            R1QF06.setParam('subname','R1QF06')
            R1QF07.setParam('subname','R1QF07')
            R1QD08.setParam('subname','R1QD08')
            R1QF09.setParam('subname','R1QF09')
            R1QF10.setParam('subname','R1QF10')
            R1QD11.setParam('subname','R1QD11')
            R1QF12.setParam('subname','R1QF12')
            R2QF01.setParam('subname','R2QF01')
            R2QD02.setParam('subname','R2QD02')
            R2QF03.setParam('subname','R2QF03')
            R2QF04.setParam('subname','R2QF04')
            R2QD05.setParam('subname','R2QD05')
            R2QF06.setParam('subname','R2QF06')
            R2QF07.setParam('subname','R2QF07')
            R2QD08.setParam('subname','R2QD08')
            R2QF09.setParam('subname','R2QF09')
            R2QF10.setParam('subname','R2QF10')
            R2QD11.setParam('subname','R2QD11')
            R2QF12.setParam('subname','R2QF12')
            R3QF01.setParam('subname','R3QF01')
            R3QD02.setParam('subname','R3QD02')
            R3QF03.setParam('subname','R3QF03')
            R3QF04.setParam('subname','R3QF04')
            R3QD05.setParam('subname','R3QD05')
            R3QF06.setParam('subname','R3QF06')
            R3QF07.setParam('subname','R3QF07')
            R3QD08.setParam('subname','R3QD08')
            R3QF09.setParam('subname','R3QF09')
            R3QF10.setParam('subname','R3QF10')
            R3QD11.setParam('subname','R3QD11')
            R3QF12.setParam('subname','R3QF12')
            R4QF01.setParam('subname','R4QF01')
            R4QD02.setParam('subname','R4QD02')
            R4QF03.setParam('subname','R4QF03')
            R4QF04.setParam('subname','R4QF04')
            R4QD05.setParam('subname','R4QD05')
            R4QF06.setParam('subname','R4QF06')
            R4QF07.setParam('subname','R4QF07')
            R4QD08.setParam('subname','R4QD08')
            R4QF09.setParam('subname','R4QF09')
            R4QF10.setParam('subname','R4QF10')
            R4QD11.setParam('subname','R4QD11')
            R4QF12.setParam('subname','R4QF12')
        else:
            if self.rank == 0:
                print('sliced')
            for i in xrange(len(R1QF01)): R1QF01[i].setParam('subname','R1QF01_%d' % i)
            for i in xrange(len(R1QD02)): R1QD02[i].setParam('subname','R1QD02_%d' % i)
            for i in xrange(len(R1QF03)): R1QF03[i].setParam('subname','R1QF03_%d' % i)
            for i in xrange(len(R1QF04)): R1QF04[i].setParam('subname','R1QF04_%d' % i)
            for i in xrange(len(R1QD05)): R1QD05[i].setParam('subname','R1QD05_%d' % i)
            for i in xrange(len(R1QF06)): R1QF06[i].setParam('subname','R1QF06_%d' % i)
            for i in xrange(len(R1QF07)): R1QF07[i].setParam('subname','R1QF07_%d' % i)
            for i in xrange(len(R1QD08)): R1QD08[i].setParam('subname','R1QD08_%d' % i)
            for i in xrange(len(R1QF09)): R1QF09[i].setParam('subname','R1QF09_%d' % i)
            for i in xrange(len(R1QF10)): R1QF10[i].setParam('subname','R1QF10_%d' % i)
            for i in xrange(len(R1QD11)): R1QD11[i].setParam('subname','R1QD11_%d' % i)
            for i in xrange(len(R1QF12)): R1QF12[i].setParam('subname','R1QF12_%d' % i)
            for i in xrange(len(R2QF01)): R2QF01[i].setParam('subname','R2QF01_%d' % i)
            for i in xrange(len(R2QD02)): R2QD02[i].setParam('subname','R2QD02_%d' % i)
            for i in xrange(len(R2QF03)): R2QF03[i].setParam('subname','R2QF03_%d' % i)
            for i in xrange(len(R2QF04)): R2QF04[i].setParam('subname','R2QF04_%d' % i)
            for i in xrange(len(R2QD05)): R2QD05[i].setParam('subname','R2QD05_%d' % i)
            for i in xrange(len(R2QF06)): R2QF06[i].setParam('subname','R2QF06_%d' % i)
            for i in xrange(len(R2QF07)): R2QF07[i].setParam('subname','R2QF07_%d' % i)
            for i in xrange(len(R2QD08)): R2QD08[i].setParam('subname','R2QD08_%d' % i)
            for i in xrange(len(R2QF09)): R2QF09[i].setParam('subname','R2QF09_%d' % i)
            for i in xrange(len(R2QF10)): R2QF10[i].setParam('subname','R2QF10_%d' % i)
            for i in xrange(len(R2QD11)): R2QD11[i].setParam('subname','R2QD11_%d' % i)
            for i in xrange(len(R2QF12)): R2QF12[i].setParam('subname','R2QF12_%d' % i)
            for i in xrange(len(R3QF01)): R3QF01[i].setParam('subname','R3QF01_%d' % i)
            for i in xrange(len(R3QD02)): R3QD02[i].setParam('subname','R3QD02_%d' % i)
            for i in xrange(len(R3QF03)): R3QF03[i].setParam('subname','R3QF03_%d' % i)
            for i in xrange(len(R3QF04)): R3QF04[i].setParam('subname','R3QF04_%d' % i)
            for i in xrange(len(R3QD05)): R3QD05[i].setParam('subname','R3QD05_%d' % i)
            for i in xrange(len(R3QF06)): R3QF06[i].setParam('subname','R3QF06_%d' % i)
            for i in xrange(len(R3QF07)): R3QF07[i].setParam('subname','R3QF07_%d' % i)
            for i in xrange(len(R3QD08)): R3QD08[i].setParam('subname','R3QD08_%d' % i)
            for i in xrange(len(R3QF09)): R3QF09[i].setParam('subname','R3QF09_%d' % i)
            for i in xrange(len(R3QF10)): R3QF10[i].setParam('subname','R3QF10_%d' % i)
            for i in xrange(len(R3QD11)): R3QD11[i].setParam('subname','R3QD11_%d' % i)
            for i in xrange(len(R3QF12)): R3QF12[i].setParam('subname','R3QF12_%d' % i)
            for i in xrange(len(R4QF01)): R4QF01[i].setParam('subname','R4QF01_%d' % i)
            for i in xrange(len(R4QD02)): R4QD02[i].setParam('subname','R4QD02_%d' % i)
            for i in xrange(len(R4QF03)): R4QF03[i].setParam('subname','R4QF03_%d' % i)
            for i in xrange(len(R4QF04)): R4QF04[i].setParam('subname','R4QF04_%d' % i)
            for i in xrange(len(R4QD05)): R4QD05[i].setParam('subname','R4QD05_%d' % i)
            for i in xrange(len(R4QF06)): R4QF06[i].setParam('subname','R4QF06_%d' % i)
            for i in xrange(len(R4QF07)): R4QF07[i].setParam('subname','R4QF07_%d' % i)
            for i in xrange(len(R4QD08)): R4QD08[i].setParam('subname','R4QD08_%d' % i)
            for i in xrange(len(R4QF09)): R4QF09[i].setParam('subname','R4QF09_%d' % i)
            for i in xrange(len(R4QF10)): R4QF10[i].setParam('subname','R4QF10_%d' % i)
            for i in xrange(len(R4QD11)): R4QD11[i].setParam('subname','R4QD11_%d' % i)
            for i in xrange(len(R4QF12)): R4QF12[i].setParam('subname','R4QF12_%d' % i)


        ###################################
        #The sextupoles
        ###################################
        KSF01 = magnet['KSF01'];
        KSD02 = magnet['KSD02'];
        SF = teapot.MultipoleTEAPOT("SF")
        SF.setLength(0.2)
        SF.getParam("poles").append(2)
        SF.getParam("kls").append(KSF01/Brhoinj)
        SF.getParam("skews").append(0)

        SD = teapot.MultipoleTEAPOT("SD")
        SD.setLength(0.2)
        SD.getParam("poles").append(2)
        SD.getParam("kls").append(KSD02/Brhoinj)
        SD.getParam("skews").append(0)

        R1SF01 = copy.deepcopy(SF)
        R1SD02 = copy.deepcopy(SD)
        R1SD03 = copy.deepcopy(SD)
        R1SF04 = copy.deepcopy(SF)
        R2SF01 = copy.deepcopy(SF)
        R2SD02 = copy.deepcopy(SD)
        R2SD03 = copy.deepcopy(SD)
        R2SF04 = copy.deepcopy(SF)
        R3SF01 = copy.deepcopy(SF)
        R3SD02 = copy.deepcopy(SD)
        R3SD03 = copy.deepcopy(SD)
        R3SF04 = copy.deepcopy(SF)
        R4SF01 = copy.deepcopy(SF)
        R4SD02 = copy.deepcopy(SD)
        R4SD03 = copy.deepcopy(SD)
        R4SF04 = copy.deepcopy(SF)

        R1SF01.setName('R1SF01')
        R1SD02.setName('R1SD02')
        R1SD03.setName('R1SD03')
        R1SF04.setName('R1SF04')
        R2SF01.setName('R2SF01')
        R2SD02.setName('R2SD02')
        R2SD03.setName('R2SD03')
        R2SF04.setName('R2SF04')
        R3SF01.setName('R3SF01')
        R3SD02.setName('R3SD02')
        R3SD03.setName('R3SD03')
        R3SF04.setName('R3SF04')
        R4SF01.setName('R4SF01')
        R4SD02.setName('R4SD02')
        R4SD03.setName('R4SD03')
        R4SF04.setName('R4SF04')

        #BPM list

        R1BPM01 = TeapotBPMTBTNode("R1BPM01")
        R1BPM02 = TeapotBPMTBTNode("R1BPM02")
        R1BPM04 = TeapotBPMTBTNode("R1BPM04")
        R1BPM05 = TeapotBPMTBTNode("R1BPM05")
        R1BPM08 = TeapotBPMTBTNode("R1BPM08")
        R1BPM09 = TeapotBPMTBTNode("R1BPM09")
        R1BPM11 = TeapotBPMTBTNode("R1BPM11")
        R1BPM12 = TeapotBPMTBTNode("R1BPM12")

        R2BPM01 = TeapotBPMTBTNode("R2BPM01")
        R2BPM02 = TeapotBPMTBTNode("R2BPM02")
        R2BPM04 = TeapotBPMTBTNode("R2BPM04")
        R2BPM05 = TeapotBPMTBTNode("R2BPM05")
        R2BPM08 = TeapotBPMTBTNode("R2BPM08")
        R2BPM09 = TeapotBPMTBTNode("R2BPM09")
        R2BPM11 = TeapotBPMTBTNode("R2BPM11")
        R2BPM12 = TeapotBPMTBTNode("R2BPM12")

        R3BPM01 = TeapotBPMTBTNode("R3BPM01")
        R3BPM02 = TeapotBPMTBTNode("R3BPM02")
        R3BPM04 = TeapotBPMTBTNode("R3BPM04")
        R3BPM05 = TeapotBPMTBTNode("R3BPM05")
        R3BPM08 = TeapotBPMTBTNode("R3BPM08")
        R3BPM09 = TeapotBPMTBTNode("R3BPM09")
        R3BPM11 = TeapotBPMTBTNode("R3BPM11")
        R3BPM12 = TeapotBPMTBTNode("R3BPM12")

        R4BPM01 = TeapotBPMTBTNode("R4BPM01")
        R4BPM02 = TeapotBPMTBTNode("R4BPM02")
        R4BPM04 = TeapotBPMTBTNode("R4BPM04")
        R4BPM05 = TeapotBPMTBTNode("R4BPM05")
        R4BPM08 = TeapotBPMTBTNode("R4BPM08")
        R4BPM09 = TeapotBPMTBTNode("R4BPM09")
        R4BPM11 = TeapotBPMTBTNode("R4BPM11")
        R4BPM12 = TeapotBPMTBTNode("R4BPM12")

        #The correctors
        R1DH01 = teapot.KickTEAPOT("R1DH01")
        R1DV02 = teapot.KickTEAPOT("R1DV02")
        R1DH04 = teapot.KickTEAPOT("R1DH04")
        R1DV05 = teapot.KickTEAPOT("R1DV05")
        R1DV08 = teapot.KickTEAPOT("R1DV08")
        R1DH09 = teapot.KickTEAPOT("R1DH09")
        R1DV11 = teapot.KickTEAPOT("R1DV11")
        R1DH12 = teapot.KickTEAPOT("R1DH12")
        R2DH01 = teapot.KickTEAPOT("R2DH01")
        R2DV02 = teapot.KickTEAPOT("R2DV02")
        R2DH04 = teapot.KickTEAPOT("R2DH04")
        R2DV05 = teapot.KickTEAPOT("R2DV05")
        R2DV08 = teapot.KickTEAPOT("R2DV08")
        R2DH09 = teapot.KickTEAPOT("R2DH09")
        R2DV11 = teapot.KickTEAPOT("R2DV11")
        R2DH12 = teapot.KickTEAPOT("R2DH12")
        #R3DH01 = teapot.KickTEAPOT("R3DH01")
        R3DH01 = teapot.DriftTEAPOT("R3DH01_mark")
        R3DV02 = teapot.KickTEAPOT("R3DV02")
        R3DH04 = teapot.KickTEAPOT("R3DH04")
        R3DV05 = teapot.KickTEAPOT("R3DV05")
        R3DV08 = teapot.KickTEAPOT("R3DV08")
        R3DH09 = teapot.KickTEAPOT("R3DH09")
        R3DV11 = teapot.KickTEAPOT("R3DV11")
        R3DH12 = teapot.KickTEAPOT("R3DH12")
        R4DH01 = teapot.KickTEAPOT("R4DH01")
        R4DV02 = teapot.KickTEAPOT("R4DV02")
        R4DH04 = teapot.KickTEAPOT("R4DH04")
        R4DV05 = teapot.KickTEAPOT("R4DV05")
        R4DV08 = teapot.KickTEAPOT("R4DV08")
        R4DH09 = teapot.KickTEAPOT("R4DH09")
        R4DV11 = teapot.KickTEAPOT("R4DV11")
        R4DH12 = teapot.KickTEAPOT("R4DH12")
        R3LMTDH = teapot.KickTEAPOT("R3LMTDH")
        R3LMTDV = teapot.KickTEAPOT("R3LMTDV")

        counter = TeapotCounterNode("counter") # record the tracking turn, only need one
        tune_BPM = TeapotTuneSpreadNode("TUNE_BPM") #calculate the tune, only need one

        RCS_temp=[counter, tune_BPM, L001_a,BC03,L001_b,BC04,L001_c,R1BPM01,L002,R1DH01,L003,R1QF01,L004,R1QD02,L005,R1BPM02,L006,R1DV02,\
        L007,R1QF03,L008,R1MB01,L009,R1MB02,L010,R1BPM04,L011,R1DH04,L012,R1SF01,L013,\
        R1QF04,L014,R1SD02,L015,R1BPM05,L016,R1DV05,L017,R1QD05,L018,R1QF06,L019,R1MB03,L020,R1MB04,L021,R1QF07,L022,R1QD08,L023,R1BPM08,L024,R1DV08,\
        L025,R1SD03,L026,R1QF09,L027,R1SF04,L028,R1BPM09,L029,R1DH09,L030,R1MB05,L031,R1MB06,L032,R1QF10,L033,R1BPM11,L034,R1DV11,L035,\
        R1QD11,L036,R1QF12,L037,R1BPM12,L038,R1DH12,L039,R2BPM12,L040,R2DH12,L041,R2QF12,L042,R2QD11,L043,R2BPM11,L044,R2DV11,L045,\
        R2QF10,L046,R2MB06,L047,R2MB05,L048,R2BPM09,L049,R2DH09,L050,R2SF04,L051,R2QF09,L052,R2SD03,L053,R2BPM08,L054,R2DV08,L055,R2QD08,\
        L056,R2QF07,L057,R2MB04,L058,R2MB03,L059,R2QF06,L060,R2QD05,L061,R2BPM05,L062,R2DV05,L063,R2SD02,L064,R2QF04,L065,R2SF01,L066,\
        R2BPM04,L067,R2DH04,L068,R2MB02,L069,R2MB01,L070,R2BPM02,L071,R2DV02,L072,R2QF03,L073,R2QD02,L074,R2QF01,L075,R2BPM01,L076,R2DH01,L077,R3LMTDV,L078,R3BPM01,L079,\
        R3QF01,L080,R3DH01,L081,R3QD02,L082,R3BPM02,L083,R3DV02,L084,R3QF03,L085,R3LMTDH,L086,R3MB01,L087,R3MB02,L088,R3BPM04,L089,R3DH04,L090,R3SF01,L091,R3QF04,\
        L092,R3SD02,L093,R3BPM05,L094,R3DV05,L095,R3QD05,L096,R3QF06,L097,R3MB03,L098,R3MB04,L099,R3QF07,L100,R3QD08,L101,R3BPM08,L102,R3DV08,L103,R3SD03,L104,\
        R3QF09,L105,R3SF04,L106,R3BPM09,L107,R3DH09,L108,R3MB05,L109,R3MB06,L110,R3QF10,L111,R3BPM11,L112,R3DV11,L113,R3QD11,L114,R3QF12,L115,R3BPM12,L116,R3DH12,L117,\
        R4BPM12,L118,R4DH12,L119,R4QF12,L120,R4QD11,L121,R4BPM11,L122,R4DV11,L123,R4QF10,L124,R4MB06,L125,R4MB05,L126,R4BPM09,L127,R4DH09,L128,R4SF04,L129,R4QF09,\
        L130,R4SD03,L131,R4BPM08,L132,R4DV08,L133,R4QD08,L134,R4QF07,L135,R4MB04,L136,R4MB03,L137,R4QF06,L138,R4QD05,L139,R4BPM05,L140,R4DV05,L141,R4SD02,L142,\
        R4QF04,L143,R4SF01,L144,R4BPM04,L145,R4DH04,L146,R4MB02,L147,R4MB01,L148,R4QF03,L149,R4BPM02,L150,R4DV02,L151,R4QD02,L152,R4QF01,L153,R4BPM01,L154,R4DH01,\
        L155_a,BC01,L155_b,BC02,L155_c];

        #RCS = list(chain.from_iterable(RCS))
        def flat(con, a):
            if isinstance(a, list):
                for b in a:
                    flat(con, b)
            else:
                con.append(a)

        RCS = []
        flat(RCS, RCS_temp)


        if model=='AC':
            lattice = time_dep_csns.TIME_DEP_Lattice("RCS")
        else:
            lattice = TEAPOT_Ring("RCS")
        lattice.setNodes(RCS)
        lattice.initialize()
        self.lattice = lattice
        if 'qt' in magnet.keys() and magnet['qt']['enable']:
            self.__add_qt_node(magnet['qt'], Brhoinj)

    def load_sliced_bend(self):
        ANG = pi/12
        EE = ANG/2
        b01 = teapot.BendTEAPOT("bend_1")
        b01.setLength(0.2)
        b01.addParam("theta",0.00421*ANG)
        b01.addParam("ea1",EE)
        b01.addParam("ea2",-0.99158*EE)
        b01.addParam("ratio",(0.5,0.5))
        b02 = teapot.BendTEAPOT("bend_2")
        b02.setLength(0.2)
        b02.addParam("theta",0.02265*ANG)
        b02.addParam("ea1",0.99158*EE)
        b02.addParam("ea2",-0.94628*EE)
        b02.addParam("ratio",(0.5,0.5))
        b03 = teapot.BendTEAPOT("bend_3")
        b03.setLength(0.2)
        b03.addParam("theta",0.06688*ANG)
        b03.addParam("ea1",0.94628*EE)
        b03.addParam("ea2",-0.81252*EE)
        b03.addParam("ratio",(0.5,0.5))
        b04 = teapot.BendTEAPOT("bend_4")
        b04.setLength(0.2)
        b04.addParam("theta",0.09525*ANG)
        b04.addParam("ea1",0.81252*EE)
        b04.addParam("ea2",-0.62202*EE)
        b04.addParam("ratio",(0.5,0.5))
        b05 = teapot.BendTEAPOT("bend_5")
        b05.setLength(0.2)
        b05.addParam("theta",0.09569*ANG)
        b05.addParam("ea1",0.62202*EE)
        b05.addParam("ea2",-0.43064*EE)
        b05.addParam("ratio",(0.5,0.5))
        b06 = teapot.BendTEAPOT("bend_6")
        b06.setLength(0.9)
        b06.addParam("theta",0.43064*ANG)
        b06.addParam("ea1",0.43064*EE)
        b06.addParam("ea2",0.43064*EE)
        b06.addParam("ratio",(0.5,0.5))
        b07 = teapot.BendTEAPOT("bend_7")
        b07.setLength(0.2)
        b07.addParam("theta",0.09569*ANG)
        b07.addParam("ea1",-0.43064*EE)
        b07.addParam("ea2",0.62202*EE)
        b07.addParam("ratio",(0.5,0.5))
        b08 = teapot.BendTEAPOT("bend_8")
        b08.setLength(0.2)
        b08.addParam("theta",0.09525*ANG)
        b08.addParam("ea1",-0.62202*EE)
        b08.addParam("ea2",0.81252*EE)
        b08.addParam("ratio",(0.5,0.5))
        b09 = teapot.BendTEAPOT("bend_9")
        b09.setLength(0.2)
        b09.addParam("theta",0.06688*ANG)
        b09.addParam("ea1",-0.81252*EE)
        b09.addParam("ea2",0.94628*EE)
        b09.addParam("ratio",(0.5,0.5))
        b10 = teapot.BendTEAPOT("bend_10")
        b10.setLength(0.2)
        b10.addParam("theta",0.02265*ANG)
        b10.addParam("ea1",-0.94628*EE)
        b10.addParam("ea2",0.99158*EE)
        b10.addParam("ratio",(0.5,0.5))
        b11 = teapot.BendTEAPOT("bend_11")
        b11.setLength(0.2)
        b11.addParam("theta",0.00421*ANG)
        b11.addParam("ea1",-0.99158*EE)
        b11.addParam("ea2",EE)
        b11.addParam("ratio",(0.5,0.5))
        MB = [b01,b02,b03,b04,b05,b06,b07,b08,b09,b10,b11]
        return MB

    def load_sliced_quad(self, KR1Q01, KR1Q02, KR1Q03, KR1Q04, KR1Q06):
        r101q01 = teapot.QuadTEAPOT("QF01_1")
        r101q01.setLength(0.1)
        r101q01.addParam("kq",KR1Q01)
        r101q01.addParam("fudge",0.004971*0.41/0.1)
        r101q02 = teapot.QuadTEAPOT("QF01_2")
        r101q02.setLength(0.1)
        r101q02.addParam("kq",KR1Q01 )
        r101q02.addParam("fudge",0.015883*0.41/0.1)
        r101q03 = teapot.QuadTEAPOT("QF01_3")
        r101q03.setLength(0.1)
        r101q03.addParam("kq",KR1Q01)
        r101q03.addParam("fudge",0.054644*0.41/0.1)
        r101q04 = teapot.QuadTEAPOT("QF01_4")
        r101q04.setLength(0.1)
        r101q04.addParam("kq",KR1Q01)
        r101q04.addParam("fudge",0.172257*0.41/0.1)
        r101q05 = teapot.QuadTEAPOT("QF01_5")
        r101q05.setLength(0.21)
        r101q05.addParam("kq",KR1Q01)
        r101q05.addParam("fudge",0.504490*0.41/0.21)
        r101q06 = teapot.QuadTEAPOT("QF01_6")
        r101q06.setLength(0.1)
        r101q06.addParam("kq",KR1Q01)
        r101q06.addParam("fudge",0.172257*0.41/0.1)
        r101q07 = teapot.QuadTEAPOT("QF01_7")
        r101q07.setLength(0.1)
        r101q07.addParam("kq",KR1Q01)
        r101q07.addParam("fudge",0.054644*0.41/0.1)
        r101q08 = teapot.QuadTEAPOT("QF01_8")
        r101q08.setLength(0.1)
        r101q08.addParam("kq",KR1Q01)
        r101q08.addParam("fudge",0.015883*0.41/0.1)
        r101q09 = teapot.QuadTEAPOT("QF01_9")
        r101q09.setLength(0.1)
        r101q09.addParam("kq",KR1Q01)
        r101q09.addParam("fudge",0.004971*0.41/0.1)
        QF01 = [r101q01, r101q02, r101q03, r101q04, r101q05, r101q06,
                r101q07, r101q08, r101q09]

        r102q01 = teapot.QuadTEAPOT("QD02_1")
        r102q01.setLength(0.1)
        r102q01.addParam("kq",KR1Q02)
        r102q01.addParam("fudge",0.021979*0.9/0.1)
        r102q02 = teapot.QuadTEAPOT("QD02_2")
        r102q02.setLength(0.1)
        r102q02.addParam("kq",KR1Q02)
        r102q02.addParam("fudge",0.027192*0.9/0.1)
        r102q03 = teapot.QuadTEAPOT("QD02_3")
        r102q03.setLength(0.1)
        r102q03.addParam("kq",KR1Q02)
        r102q03.addParam("fudge",0.076485*0.9/0.1)
        r102q04 = teapot.QuadTEAPOT("QD02_4")
        r102q04.setLength(0.1)
        r102q04.addParam("kq",KR1Q02)
        r102q04.addParam("fudge",0.104508*0.9/0.1)
        r102q05 = teapot.QuadTEAPOT("QD02_5")
        r102q05.setLength(0.5)
        r102q05.addParam("kq",KR1Q02)
        r102q05.addParam("fudge",0.539670*0.9/0.5)
        r102q06 = teapot.QuadTEAPOT("QD02_6")
        r102q06.setLength(0.1)
        r102q06.addParam("kq",KR1Q02)
        r102q06.addParam("fudge",0.104508*0.9/0.1)
        r102q07 = teapot.QuadTEAPOT("QD02_7")
        r102q07.setLength(0.1)
        r102q07.addParam("kq",KR1Q02)
        r102q07.addParam("fudge",0.076485*0.9/0.1)
        r102q08 = teapot.QuadTEAPOT("QD02_8")
        r102q08.setLength(0.1)
        r102q08.addParam("kq",KR1Q02)
        r102q08.addParam("fudge",0.027192*0.9/0.1)
        r102q09 = teapot.QuadTEAPOT("QD02_9")
        r102q09.setLength(0.1)
        r102q09.addParam("kq",KR1Q02)
        r102q09.addParam("fudge",0.021979*0.9/0.1)
        QD02 = [r102q01, r102q02, r102q03, r102q04, r102q05, r102q06, r102q07,
                r102q08, r102q09]

        r103q01 = teapot.QuadTEAPOT("QF03_1")
        r103q01.setLength(0.1)
        r103q01.addParam("kq",KR1Q03)
        r103q01.addParam("fudge",0.004971*0.41/0.1)
        r103q02 = teapot.QuadTEAPOT("QF03_2")
        r103q02.setLength(0.1)
        r103q02.addParam("kq",KR1Q03)
        r103q02.addParam("fudge",0.015883*0.41/0.1)
        r103q03 = teapot.QuadTEAPOT("QF03_3")
        r103q03.setLength(0.1)
        r103q03.addParam("kq",KR1Q03)
        r103q03.addParam("fudge",0.054644*0.41/0.1)
        r103q04 = teapot.QuadTEAPOT("QF03_4")
        r103q04.setLength(0.1)
        r103q04.addParam("kq",KR1Q03)
        r103q04.addParam("fudge",0.172257*0.41/0.1)
        r103q05 = teapot.QuadTEAPOT("QF03_5")
        r103q05.setLength(0.21)
        r103q05.addParam("kq",KR1Q03)
        r103q05.addParam("fudge",0.504490*0.41/0.21)
        r103q06 = teapot.QuadTEAPOT("QF03_6")
        r103q06.setLength(0.1)
        r103q06.addParam("kq",KR1Q03)
        r103q06.addParam("fudge",0.172257*0.41/0.1)
        r103q07 = teapot.QuadTEAPOT("QF03_7")
        r103q07.setLength(0.1)
        r103q07.addParam("kq",KR1Q03)
        r103q07.addParam("fudge",0.054644*0.41/0.1)
        r103q08 = teapot.QuadTEAPOT("QF03_8")
        r103q08.setLength(0.1)
        r103q08.addParam("kq",KR1Q03)
        r103q08.addParam("fudge",0.015883*0.41/0.1)
        r103q09 = teapot.QuadTEAPOT("QF03_9")
        r103q09.setLength(0.1)
        r103q09.addParam("kq",KR1Q03)
        r103q09.addParam("fudge",0.004971*0.41/0.1)

        QF03 = [r103q01, r103q02, r103q03, r103q04, r103q05, r103q06, r103q07,
                r103q08, r103q09]
        r104q01 = teapot.QuadTEAPOT("QF04_1")
        r104q01.setLength(0.074)
        r104q01.addParam("kq",KR1Q04)
        r104q01.addParam("fudge",0.022501*0.45/0.074)
        r104q02 = teapot.QuadTEAPOT("QF04_2")
        r104q02.setLength(0.1)
        r104q02.addParam("kq",KR1Q04)
        r104q02.addParam("fudge",0.056312*0.45/0.1)
        r104q03 = teapot.QuadTEAPOT("QF04_3")
        r104q03.setLength(0.1)
        r104q03.addParam("kq",KR1Q04)
        r104q03.addParam("fudge",0.156580*0.45/0.1)
        r104q04 = teapot.QuadTEAPOT("QF04_4")
        r104q04.setLength(0.25)
        r104q04.addParam("kq",KR1Q04)
        r104q04.addParam("fudge",0.529214*0.45/0.25)
        r104q05 = teapot.QuadTEAPOT("QF04_5")
        r104q05.setLength(0.1)
        r104q05.addParam("kq",KR1Q04)
        r104q05.addParam("fudge",0.156580*0.45/0.1)
        r104q06 = teapot.QuadTEAPOT("QF04_6")
        r104q06.setLength(0.1)
        r104q06.addParam("kq",KR1Q04)
        r104q06.addParam("fudge",0.056312*0.45/0.1)
        r104q07 = teapot.QuadTEAPOT("QF04_7")
        r104q07.setLength(0.074)
        r104q07.addParam("kq",KR1Q04)
        r104q07.addParam("fudge",0.022501*0.45/0.074)

        QF04 = [r104q01, r104q02, r104q03, r104q04, r104q05, r104q06, r104q07]
        r106q01 = teapot.QuadTEAPOT("QF06_1")
        r106q01.setLength(0.1)
        r106q01.addParam("kq",KR1Q06)
        r106q01.addParam("fudge",0.00507716*0.62/0.1)
        r106q02 = teapot.QuadTEAPOT("QF06_2")
        r106q02.setLength(0.1)
        r106q02.addParam("kq",KR1Q06)
        r106q02.addParam("fudge",0.014081869*0.62/0.1)
        r106q03 = teapot.QuadTEAPOT("QF06_3")
        r106q03.setLength(0.1)
        r106q03.addParam("kq",KR1Q06)
        r106q03.addParam("fudge",0.040726884*0.62/0.1)
        r106q04 = teapot.QuadTEAPOT("QF06_4")
        r106q04.setLength(0.1)
        r106q04.addParam("kq",KR1Q06)
        r106q04.addParam("fudge",0.106496121*0.62/0.1)
        r106q05 = teapot.QuadTEAPOT("QF06_5")
        r106q05.setLength(0.1)
        r106q05.addParam("kq",KR1Q06)
        r106q05.addParam("fudge",0.155892976*0.62/0.1)
        r106q06 = teapot.QuadTEAPOT("QF06_6")
        r106q06.setLength(0.22)
        r106q06.addParam("kq",KR1Q06)
        r106q06.addParam("fudge",0.355449982*0.62/0.22)
        r106q07 = teapot.QuadTEAPOT("QF06_7")
        r106q07.setLength(0.1)
        r106q07.addParam("kq",KR1Q06)
        r106q07.addParam("fudge",0.155892976*0.62/0.1)
        r106q08 = teapot.QuadTEAPOT("QF06_8")
        r106q08.setLength(0.1)
        r106q08.addParam("kq",KR1Q06)
        r106q08.addParam("fudge",0.106496121*0.62/0.1)
        r106q09 = teapot.QuadTEAPOT("QF06_9")
        r106q09.setLength(0.1)
        r106q09.addParam("kq",KR1Q06)
        r106q09.addParam("fudge",0.040726884*0.62/0.1)
        r106q10 = teapot.QuadTEAPOT("QF06_10")
        r106q10.setLength(0.1)
        r106q10.addParam("kq",KR1Q06)
        r106q10.addParam("fudge",0.014081869*0.62/0.1)
        r106q11 = teapot.QuadTEAPOT("QF06_11")
        r106q11.setLength(0.1)
        r106q11.addParam("kq",KR1Q06)
        r106q11.addParam("fudge",0.00507716*0.62/0.1)
        QF06 = [r106q01, r106q02, r106q03, r106q04, r106q05, r106q06, r106q07,
                r106q08, r106q09, r106q10, r106q11]
        return QF01, QD02, QF03, QF04, QF06

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("configfile", type=str, help="The config file!")
    args = parser.parse_args()
    with open(args.configfile) as f_in:
        cfg = json.load(f_in)
        cfg['diagnostic']['enable'] = False
        if 'enable' in cfg['injection']['foil']:
            cfg['injection']['foil']['enable'] = False
        builder = csns_builder(cfg)
    builder.calc_twiss()
